---
layout: default
title: "16f-6501: Computer Security"
weight: 0
---



<p><b>If SIS does not allow you to enroll:</b> I am happy to course-action you into this course.  Fill out the form, scan it, email it, and I will sign it.  Otherwise, you can try this maneuver on the first day of class. I can't change the rules that SIS imposes on registration.</p>

<p>This course meets in Olsson 011  on Tue Thu at 11--12.15.</p>

<p>My office hours are Tue,Thu 1.45-3p in my office which is the 5th floor of Rice, 512.  You can also write me to make a different appt if you cannot make that time. 
</p>


Prerequisite: The class is open to advanced undergraduates and graduate students with systems experience.

Description: Big topics:

1. Classical security topics: control flow, race conditions, privilege escalations, data validation
2. Basics of applied cryptography
3. Peril of passwords, two-factor authentication
4. Secure protocols, TLS, SSH, and recent flaws in TLS
5. How to distribute trust, e.g., Bitcoin, outsourcing, secure computation, Anonize
6. Database privacy, how humans are ``sparse” data points, and much can be inferred from a few seemingly innocent pieces of information, e.g. how the NYC taxi dataset leaks Jessica Alba’s taxi tips.
7. Online tracking, ad-tracking, ad-blocking, online privacy

Differences from ``Defense against the dark arts”:  The DADA course focuses almost exclusively on studying low-level mechanisms for hijacking the control-flow of a program.  Taking over control flow is the first step of a virus or malware.  Defenses and counter-offenses against this attack have become more and more complex, and thus it takes an entire course in order to become familiar with modern techniques.  In contrast, this security course will briefly cover control flow, but spend most of the time on other types of attacks and security defenses.

The course will involve a 3-4 homework assignments and a major class project.  I am interested in forming projects that tackle interesting, substantial problems related to computer security.  