---
layout: default
title: "16f-4800: Algorithms"
weight: 0
---


This course meets in EV024 on TueFri at 9.50--11.30.


## Textbook

You do not need a textbook for this course, but if you would like to purchase one for review, I recommend the following two books:

1. CLRS: "<a href="http://www.amazon.com/Introduction-Algorithms-Thomas-H-Cormen/dp/0262033844">Introduction to Algorithms</a>"
2. Kleinberg+Tardos, "<a href="http://www.amazon.com/Algorithm-Design-Jon-Kleinberg/dp/0321295358">Algorithm Design</a>"

In addition, I recommend the following Coursera links for you to review between and after my lectures:

1. Tim Roughgarden's  open course <a href="https://class.coursera.org/algo-004/lecture/preview">Alg Part I</a>
2. Wayne+Sedgewick's course <a href="https://class.coursera.org/algs4partI-003/class/index">Alg Design Part I</a>

## LaTeX

You should prepare your homework solutions using LaTeX, and submit PDFs to the course submission site. LaTeX is a free, open-source scientific document preparation system; most CS technical publications are prepared using this tool.  Every one of my previous advisees highly recommends it for preparing your senior thesis.


<a href="http://pages.uoregon.edu/koch/texshop/" title="Texshop">TexShop</a> is a great tex editor for the Mac platform.  For windows, I have heard good feedback about <a href="http://www.texniccenter.org/">TexNiCenter</a>.

The <a href="http://tobi.oetiker.ch/lshort/lshort.pdf">Not so short intro</a> to latex is a good reference.  <a href="http://www.tug.org/twg/mactex/tutorials/ltxprimer-1.0.pdf">This</a> is another tutorial for latex from a user group.


You may also consider using <a href="https://www.sharelatex.com">ShareLatex</a> which is a web-based latex system.  Using it avoids having to install your own latex system.

## Expectations

This is a demanding course that assigns several homeworks, programming exercises, a midterm, and a final.  I have run the course several times.  Many people loved it, but some people complained about the pace, the workload, and the latency in receiving graded homeworks.  TAs grade the homeworks as fast as they can, but this course only has 1 or 2 for 50 students.  I encourage group/collaborative work; most people enjoy that aspect.  I make myself available during evenings for Google Hangouts office hours, and I also record my lectures so that you can review them offline. That said, this course will ask you to pay attention during class, take notes, review the recordings to catch parts that you did not understand during class.  
