---
layout: post
status: publish
published: true
title: PKI Going Down (SSL Pinning)
author: AbiusX
author_login: abbas
author_email: an2wm@virginia.edu
author_url: https://abiusx.com
excerpt: "Before reading this, you should have a deep understanding of <a href=\"https://en.wikipedia.org/wiki/Secure_Sockets_Layer\">SSL</a>,
  <a href=\"https://en.wikipedia.org/wiki/Https\">HTTPS</a>, <a href=\"https://en.wikipedia.org/wiki/X509\">X.509</a> and
  <a href=\"https://en.wikipedia.org/wiki/Public_key_infrastructure\">PKI</a>.\r\n\r\nThe
  security of Internet as we know it, relies on PKI, but recent trends <a href=\"http://nakedsecurity.sophos.com/2013/12/09/serious-security-google-finds-fake-but-trusted-ssl-certificates-for-its-domains-made-in-france/\">[1]</a>
  <a href=\"http://www.comodo.com/Comodo-Fraud-Incident-2011-03-23.html\">[2]</a>
  <a href=\"http://perspectives-project.org/\">[3]</a> <a href=\"http://googleonlinesecurity.blogspot.com/2011/04/improving-ssl-certificate-security.html\">[4]</a> show
  that as cyber-security becomes more potent, and as state-driven <a href=\"https://en.wikipedia.org/wiki/Advanced_persistent_threat\">APTs</a> gain
  momentum, PKI is getting more and more abused. In [4], my government hacked into
  Comodo to acquire valid SSL certificates for Google and Yahoo, because Iranians
  use those email provides and IM using those, then they sat back and without anything
  suspicious happening, inspected all the communications of their people.\r\n\r\nSame
  thing went on with recent NSA trends as well as French government, and one can simply
  assume that all governments are doing this. Now the way they do this, if carefully
  planned, makes it almost impossible for tools like Perspective or Google Crawler
  to notice you.\r\n\r\n"
wordpress_id: 54
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=54
date: '2014-01-28 23:37:04 -0500'
date_gmt: '2014-01-28 23:37:04 -0500'
tags: []
categories: 14s-pet
comments:
- id: 2
  author: jack
  author_email: jhd3pa@virginia.edu
  author_url: ''
  date: '2014-01-29 23:04:06 -0500'
  date_gmt: '2014-01-29 23:04:06 -0500'
  content: "Interesting. As a side effect, your solution should also go a long way
    toward protecting against Moxie Marlinspike's Magic  Certificates (http://www.youtube.com/watch?v=ibF36Yyeehw),
    and any similar certificate content attacks that might be discovered (allegedly
    most common SSL implementations have patched his vulnerability, but the attack
    is only three years old - I can think of plenty of people/organizations who use
    operating systems and web browsers older than that).\r\n\r\nIncidentally, the
    same Moxie Marlinspike talk I linked to in the last paragraph has some information
    about certificate revocation via OCSP which may be relevant.\r\n\r\nThere is one
    thing that stood out to me as I was reading this, which is that I can't think
    of a way it could address the possibility of a government (or ISP, or other entity
    with control of the network infrastructure) performing MITM attacks on all accesses
    to a particular domain, for all people, all of the time (presumably using just
    one falsified certificate). Such an attack would require lots of resources if
    it were performed on a high traffic target, certainly, but population-wide monitoring
    and filtering has existed for quite a while, so I don't think it's a stretch to
    imagine that such a system might be in use. Of course, it would be easily detectable
    by carrying a computer across network boundaries, to a place where the connection
    is actually secure, but I suspect many people for whom such a tool might be useful
    don't have that option.\r\n\r\nRegardless, I would certainly use your tool. I
    have discovered at least one verifiable MITM attack on my home internet connection
    here in Charlottesville, but only because it was poorly executed. I would certainly
    be more comfortable if I could detect well-executed attacks too."
- id: 3
  author: jack
  author_email: jhd3pa@virginia.edu
  author_url: ''
  date: '2014-01-30 02:11:06 -0500'
  date_gmt: '2014-01-30 02:11:06 -0500'
  content: FYI, the bit in the previous post that says "Moxie Marlinspike's Magic
    Certificates" originally said "Moxie Marlinspike's Magic Null Character Certificates",
    where null character was actually backslash zero, as it would be in C. Apparently
    wordpress didn't like it.
- id: 5
  author: abbas
  author_email: an2wm@virginia.edu
  author_url: https://abiusx.com
  date: '2014-01-31 15:29:41 -0500'
  date_gmt: '2014-01-31 15:29:41 -0500'
  content: "Nice link, I'm gonna study that further.\r\n\r\nThe problem with you assumption
    is, that that scheme has to eventually end, and you probably travel to California
    and discover that the certificate changed there, and realize something's been
    up.\r\n\r\nIf they wanted to mount such a high-scale attack, don't you think it
    would be much easier to go passive (steal, or force an entity to release their
    private key, and decrypt all transactions on the fly) and simply read everything,
    instead of actively manipulating all Internet traffic?\r\n\r\nActually that's
    a very interesting attack, and I think we need to think about that as well :D"
- id: 9
  author: jack
  author_email: jhd3pa@virginia.edu
  author_url: ''
  date: '2014-02-01 04:49:16 -0500'
  date_gmt: '2014-02-01 04:49:16 -0500'
  content: "I'm not entirely sure that the scheme does have to eventually end. After
    all, one of the most alarming characteristics of all the recently revealed NSA
    snooping is that it appears to be perpetual (though for the most part passive).
    Moreover, how valuable is a tool that can only tell you when you've been compromised
    after the attacker has got everything they need and left?\r\n\r\nAs for the stealing
    of private keys, that's certainly still a threat, but I think a lot of privacy-centric
    organizations would rather shut down than yield to the compulsion to compromise
    their users (see also: lavabit). Your own post on single-copy data seems extremely
    relevant here.\r\n\r\nAnyway, the case I'm imagining is similar to the way chinese
    anti-government websites often worked (or so I have read) in the early days of
    the great firewall. The site is operated by a foreigner from outside the country,
    while the users access it from inside, often through some secure means. Due to
    politics (or other factors), neither the operator nor the users can easily cross
    network boundaries, meaning it would be very difficult for either party to detect
    a large scale MITM attack (if the government can silently intercept, they can
    certainly prevent the users from successfully comparing certs over the network
    itself). Of course, such a site would also be a very high value target for the
    government - which would likely have much more to gain by snooping silently than
    by blocking the site altogether. Now, I recognize that the situation is outlined
    is extreme (and I suspect less common than it might once have been), but aren't
    the people in extreme circumstances exactly those who might find such a tool the
    most valuable?\r\n\r\nOf course, I've now spent quite a few words poking very
    tiny, specific holes without providing any potential plugs for them (and indeed,
    a plug for the particular hole I've been poking seems a difficult problem to me).
    The certificate fingerprinting system is nonetheless extremely valuable. As a
    thought, if you implemented it in such a way that it could be run on a gateway,
    you could protect an entire network (the gateway could perhaps send emails or
    text messages when it detects a suspicious cert), and gain the advantage of a
    larger sample size."
- id: 43
  author: SSLEye draft from SSLDump | 14s Privacy Enhancing Technology
  author_email: ''
  author_url: http://crypto.cs.virginia.edu/courses/14s-pet/ssleye-draft-from-ssldump/
  date: '2014-03-20 17:47:17 -0400'
  date_gmt: '2014-03-20 17:47:17 -0400'
  content: "[&#8230;] initial idea was to create something that discovers legit MITM
    attacks (original post). Instead of creating a proxy (which is what everyone else
    has done) I decided to go with a passive [&#8230;]"
- id: 44
  author: fariopetjki
  author_email: ''
  author_url: http://www.fariopetjki.com/
  date: '2014-10-13 06:27:04 -0400'
  date_gmt: '2014-10-13 06:27:04 -0400'
  content: |-
    <strong>Check this out</strong>

    [...] that is the end of this article. Here you’ll find some sites that we think you’ll appreciate, just click the links over[...]…
---
<p>Before reading this, you should have a deep understanding of <a href="https://en.wikipedia.org/wiki/Secure_Sockets_Layer">SSL</a>, <a href="https://en.wikipedia.org/wiki/Https">HTTPS</a>, <a href="https://en.wikipedia.org/wiki/X509">X.509</a> and <a href="https://en.wikipedia.org/wiki/Public_key_infrastructure">PKI</a>.</p>
<p>The security of Internet as we know it, relies on PKI, but recent trends <a href="http://nakedsecurity.sophos.com/2013/12/09/serious-security-google-finds-fake-but-trusted-ssl-certificates-for-its-domains-made-in-france/">[1]</a> <a href="http://www.comodo.com/Comodo-Fraud-Incident-2011-03-23.html">[2]</a> <a href="http://perspectives-project.org/">[3]</a> <a href="http://googleonlinesecurity.blogspot.com/2011/04/improving-ssl-certificate-security.html">[4]</a> show that as cyber-security becomes more potent, and as state-driven <a href="https://en.wikipedia.org/wiki/Advanced_persistent_threat">APTs</a> gain momentum, PKI is getting more and more abused. In [4], my government hacked into Comodo to acquire valid SSL certificates for Google and Yahoo, because Iranians use those email provides and IM using those, then they sat back and without anything suspicious happening, inspected all the communications of their people.</p>
<p>Same thing went on with recent NSA trends as well as French government, and one can simply assume that all governments are doing this. Now the way they do this, if carefully planned, makes it almost impossible for tools like Perspective or Google Crawler to notice you.</p>
<p><a id="more"></a><a id="more-54"></a></p>
<p>The proposed solution for now has been Certificate Pinning, which I personally think is useless (because of the history with <a href="http://en.wikipedia.org/wiki/DNS_rebinding">DNS pinning</a> and <a href="http://abiusx.com/archive/document/DNS%20Hijacking%20via%20DNS%20Rebinding.pdf">my old paper</a> on it). Another approach, which I'm hopefully going to implement is a personal X.509 manager.</p>
<p>The way it works is, it sits on some Network layer on your system (ideally the lowest level, most practically browser integration), inspects your SSL connections, and stores them in itself. Every time you hit an X.509 certificate, it records the time and the fingerprint of that certificate. Now if it's for the same website that you already had a different X.509 for (say gmail.com), you know that something has happened:</p>
<ul>
<li>Their X.509 was expired and a new one is in place: this can be easily checked by checking the validity date of the previous one and the new one</li>
<li>It has been revoked: this can also be checked by enumerating the revocation lists, but why would it be revoked? (Suspicious)</li>
<li>You're under a legit <a href="https://en.wikipedia.org/wiki/Man-in-the-middle_attack">MITM</a>: someone is inspecting your SSL traffic while smiling that you won't notice.</li>
<li>You were under a legit MITM: the previous time (the first time this certificate was stored in your app), you were under a MITM attack, and someone smiled, but now you're not.</li>
</ul>
<p>Either way you know that something is (has been) up. One might argue that if you're MITMd, they might always use the same certificate on you (even the first time) and you will never know. That is very unlikely to happen, because they have to intercept all your traffic (no matter where you go) and really care about you that much, but you'd probably have bodyguards at that point.</p>
<p>Now ideally this would be a passive IDS on your system, inspecting your network traffic and alerting you with a minimal delay (possibly a couple seconds) when you're MITMd, so that you can keep your cool, and fake everything (you really don't need to, because they can read your password with that). It could also have a intercepting proxy mode, which adds a lot of instability and delay on your system's network, but keep you pretty safe. If that's not feasible, one could at list do this as a HTTP(S) proxy over all HTTPS connections in your system (instead of checking all SSL connections, like for your IM software).</p>
<p>I, as a security guy with many years of experience, have experienced legit MITMs at least a dozen times, and their numbers are increasing, so if you really want a safer future for yourself and the rest of the Internet folk, step in!</p>
