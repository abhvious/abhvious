---
layout: post
status: publish
published: true
title: "(non-project) The most embarrassing line of code."
author: jack
author_login: jack
author_email: jhd3pa@virginia.edu
wordpress_id: 199
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=199
date: '2014-02-25 04:22:10 -0500'
date_gmt: '2014-02-25 04:22:10 -0500'
tags: []
categories: 14s-pet
comments: []
---
<p><a href="http://nakedsecurity.sophos.com/2014/02/24/anatomy-of-a-goto-fail-apples-ssl-bug-explained-plus-an-unofficial-patch/">http://nakedsecurity.sophos.com/2014/02/24/anatomy-of-a-goto-fail-apples-ssl-bug-explained-plus-an-unofficial-patch/</a></p>
<p>Due to a bug in the SecureTransport library, Apple software currently accepts any SSL certificate as valid so long as the connection uses TLS1.1 + forward security. This bug consists of a single, incredibly appropriate line of code: 'goto fail;'</p>
