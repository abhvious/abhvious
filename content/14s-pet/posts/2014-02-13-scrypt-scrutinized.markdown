---
layout: post
status: publish
published: true
title: scrypt scrutinized
author: Amir
author_login: ameer
author_email: am8zv@virginia.edu
excerpt: "I was quite intrigued by the scrypt key derivation algorithm that was discussed
  in class so I decided to look further into how the process works. I have not found
  a good secondary resource on scrypt (not any that I liked anyway), so I went through
  Colin Percival's <a href=\"https://www.tarsnap.com/scrypt/scrypt.pdf\" target=\"_blank\">original
  paper</a> to see how the algorithm works and derived a high-level (not-quite-abstract)
  overview of the different components used in the derivation process.\r\n\r\n<a href=\"http://www.cs.virginia.edu/~am8zv/resources/scrypt.pdf\"
  target=\"_blank\">Here is a link to my take</a> on how to view the operation of
  the algorithm.\r\n\r\nIn this post, I'll describe in more detail the parameters
  used and the process through which a key is derived from a passphrase.\r\n\r\n"
wordpress_id: 135
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=135
date: '2014-02-13 20:01:25 -0500'
date_gmt: '2014-02-13 20:01:25 -0500'
tags: []
categories: 14s-pet
comments:
- id: 34
  author: jack
  author_email: jhd3pa@virginia.edu
  author_url: ''
  date: '2014-02-15 06:44:53 -0500'
  date_gmt: '2014-02-15 06:44:53 -0500'
  content: "I don't quite understand how BlockMix mitigates memory latency issues,
    nor what causes it to be more efficient to precompute the N pseudorandom values,
    rather than computing them only when needed. I didn't read the original paper,
    though (perhaps I will - those are very elementary points so I'm sure it explains
    them).\r\n\r\nOut of curiosity, I understand how such a system prevents the acceleration
    of a single execution of the algorithm, but it's my understanding that systems
    like litecoin (and perhaps also many attacks) benefit more from total throughput
    than from increased execution speed for any one scrypt run. So my question is,
    is the limiting factor memory capacity or memory throughput at present? If the
    limitation is capacity, couldn't we trade execution speed for total throughput
    by using slower but more capacious memory? For instance, fusion-io makes 5tb drives
    capable of 32 gbit/s sustained write speeds, and faster reads. Four of them in
    tandem would yield 20tb of memory at about half the speed of a single channel
    of DDR3 (about 200gbit/s, according to some quick math I just did)."
- id: 35
  author: Amir
  author_email: am8zv@virginia.edu
  author_url: ''
  date: '2014-02-16 03:42:31 -0500'
  date_gmt: '2014-02-16 03:42:31 -0500'
  content: "Good questions. I rushed quite a bit when explaining SMix. The paper does
    not go too much into detail but it alludes to the fact that BlockMix requires
    memory latency to be a good deal less than execution time. Otherwise, we would
    use the time waiting for the next block to be accessed to perform more computations
    (Salsa is still somewhat parallelizable). This is countered by raising the r parameter
    to make computation time comparable to memory latency.\r\n\r\nNow, as for rapidly
    executing BlockMix, I suggest you refer to the code of BlockMix in the paper.
    If we did not shuffle the blocks at the end of BlockMix, we can cascade the computation
    of several precomputed values of blocks to quickly find the answer of the next
    hash value. Ideally, what we want is for H(H(X)) to be computable only after fully
    computing H(X) first. Again, no formal proof is given in the paper. \r\n\r\nAccording
    to the author, the main idea of ROMix (which is basically a sequential memory-hard
    function) is to increase the size (and hence) cost of ASICs by requiring large
    amounts of memory on board to perform the required computations."
- id: 37
  author: jack
  author_email: jhd3pa@virginia.edu
  author_url: ''
  date: '2014-02-18 18:16:20 -0500'
  date_gmt: '2014-02-18 18:16:20 -0500'
  content: "Oh, I see. So the implementers change the parameters to balance computation
    time and latency as appropriate for any given environment. This makes sense.\r\n\r\nI'll
    definitely read the paper when I get a moment, but I understand now how the shuffling
    plays in, and it makes sense intuitively (though the lack of formal proof is a
    bit worrisome).\r\n\r\nFrom what you said in the first part of your response,
    I gather that the CPU is well utilized (which is something I didn't realize),
    so using a technique like the one I've suggested probably wouldn't work."
---
<p>I was quite intrigued by the scrypt key derivation algorithm that was discussed in class so I decided to look further into how the process works. I have not found a good secondary resource on scrypt (not any that I liked anyway), so I went through Colin Percival's <a href="https://www.tarsnap.com/scrypt/scrypt.pdf" target="_blank">original paper</a> to see how the algorithm works and derived a high-level (not-quite-abstract) overview of the different components used in the derivation process.</p>
<p><a href="http://www.cs.virginia.edu/~am8zv/resources/scrypt.pdf" target="_blank">Here is a link to my take</a> on how to view the operation of the algorithm.</p>
<p>In this post, I'll describe in more detail the parameters used and the process through which a key is derived from a passphrase.</p>
<p><a id="more"></a><a id="more-135"></a></p>
<p>The scrypt algorithm is parametrized with the following variables:<em><br />
</em></p>
<ul>
<li><em>P</em>: Passphrase<em></em></li>
<li><em>S</em>: Salt<em></em></li>
<li><em>r</em>: Salsa block size divider parameter</li>
<li><em>N</em>: CPU work/memory accesses metric</li>
<li><em>p</em>: Parallelism parameter</li>
<li><em>dkLen</em>: The desired length of the derived key</li>
</ul>
<p>The process starts with applying a single iteration of PBKDF2 on <em>P</em> with salt <em>S</em> to generate an internal key of size (<em>p.k</em>) where <em>k</em> is the length (in bits) of a block that shall be given to a ROMix routine. After that, we divide the resulting <em>p.k</em> key into <em>p</em> <em>k-</em>bit blocks and process each block in parallel. To simplify discussion, I will assume that  <em>p</em> = 1, so there is only one block to process.<em></em><br />
<em></em></p>
<p>The ROMix routine takes as input a <em>k</em>-bit block <em>B</em> and <em>N</em>, which specifies the number of memory locations to access. The main goal of ROMix is to implement a sequential memory-hard function that uses as much memory as possible for any given running time while thwarting parallelized attacks that attempt to bypass this memory requirement. ROMix is divided into two phases (denoted by the two yellow circles in the figure). In the first phase, we write <em>N</em> random values into <em>N</em> memory locations <em>V</em><sub>0</sub>,...,<em>V</em><sub><em>N</em>-1</sub> using BlockMix, which is used as a <a href="http://en.wikipedia.org/wiki/Random_oracle" target="_blank">Random Oracle</a> that generates these pseudorandom values. Basically, <em>V<sub>i</sub> </em>= H<em><sup>i</sup></em>(<em>B</em>) for <em>i</em> = 0,...,<em>N</em>-1. In the second phase, we access <em>N</em> memory locations pseudo-randomly by using BlockMix as a means of generating the indices of these memory locations. The intuition behind this method is to use as much memory as we can to increase the cost (circuit size) of mounting an attack.</p>
<p>The point of BlockMix is to avoid any memory-latency issues that may arise in the future. In particular, the random oracle that is BlockMix is instantiated as a routine that invokes a hash function that generates large outputs with uniform distribution. The hash function used here is Salsa20/8 since, according to the author, it has good throughput and low internal parallelism. Furthermore, the block generated at the end of the Salsa hash chain is shuffled to avoid the possibility of using precomputed values of BlockMix as means of rapidly iterating through BlockMix.</p>
<p>The output of ROMix is a "well-mixed" block. If <em>p</em> &gt; 1, the blocks are combined and fed into the last stage, which is another single-round PBKDF2 that generates the desired derived key. Currently, litecoin uses scrypt as its proof-of-work with parameters <em>N</em> = 1024, <em>r</em> = 1, and <em>p </em> = 1. However, as memory latency and CPU parallelism rise, <em>r</em> and <em>p</em> should be increased, respectively.</p>
