---
layout: post
status: publish
published: true
title: 'Satoshi''s Scripting System '
author: nick
author_login: nick
author_email: nskelsey@gmail.com
wordpress_id: 118
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=118
date: '2014-02-09 17:37:18 -0500'
date_gmt: '2014-02-09 17:37:18 -0500'
tags: []
categories: 14s-pet
comments:
- id: 21
  author: jack
  author_email: jhd3pa@virginia.edu
  author_url: ''
  date: '2014-02-11 16:37:53 -0500'
  date_gmt: '2014-02-11 16:37:53 -0500'
  content: Something occurred to me this morning. One of the interesting things you
    proposed originally was a method for verifying that particular piece of data existed
    at a given date and time. The objection I raised was that a bitcoin-type system
    can only effectively verify the order of data, rather than precise times/dates
    (of course, I said that with a very minimal understanding of bitcoin, but it is,
    to my knowledge, a valid point). However, perhaps you could implement something
    akin to time-lock encryption in order to verify that it has been at least a certain
    amount of time since the last transaction? Since each transaction depends on the
    previous ones, such a system would also verify that older transactions occurred
    at least a certain amount of time in the past. It would be fuzzy of course, but
    it might be good enough for certain use cases. Of course, I have no idea how this
    would be implemented, and I know that *coins already have proof of work mechanisms
    builtin. Maybe someone else with a bit more knowledge than I could say whether
    such a thing would actually be possible (or already exists within the system?).
- id: 23
  author: abhi
  author_email: abhi@virginia.edu
  author_url: ''
  date: '2014-02-11 18:09:56 -0500'
  date_gmt: '2014-02-11 18:09:56 -0500'
  content: Can you try scanning blockchain.info for examples of any non-standard transactions
    in Bitcoin or any other crypto-currency?
- id: 30
  author: nick
  author_email: nskelsey@gmail.com
  author_url: ''
  date: '2014-02-11 18:32:19 -0500'
  date_gmt: '2014-02-11 18:32:19 -0500'
  content: "Try to figure out what is going on here. The ScriptPubKey is the challenge.\r\n\r\nhttp://blockexplorer.com/tx/a4bfa8ab6435ae5f25dae9d89e4eb67dfa94283ca751f393c1ddc5a837bbc31b"
---
<p>So after doing a fair amount of reading last night and on Friday I feel like I have a good handle on what this project is going to entail. As it turns out, many people have already considered and implemented applications that use Bitcoin's distributed database to store what I am generically going to call application data. Fortunately for me no-one has built precisely what I intend to make so at least I am not wasting my time. :)</p>
<p>After reading about manually creating bitcoin transactions, I decided to take a deeper look at the scripting language built into Bitcoin. These scripts are executed as a transaction is being verified and are typically used to do one of two things. Either demonstrate that the transaction has been signed by a single party or that it is by signed by n of m parties.</p>
<p>The canonical use of a script is to verify that the signature + public key of the party sending the coins is able to spend those coins. Since every transaction relies on the unspent output of older transactions, the script is really broken into two parts. The actual commands to be executed live in the unspent output of an old transaction in the ScriptPubKey field, and the required signature + public key live in ScriptSig field of the input to a new transaction. <a title="more" href="https://en.bitcoin.it/wiki/Script">(source)</a></p>
<p>This is the typical setup:<br />
<code>
<pre>scriptPubKey: OP_DUP OP_HASH160 &lt;pubKeyHash&gt; OP_EQUALVERIFY OP_CHECKSIG
scriptSig: &lt;sig&gt; &lt;pubKey&gt;</pre>
<p></code><br />
Because the script must return true before it becomes part of a new valid transaction, the ScriptSig can specify any number of conditions that the values in ScriptPubKey must evaluate to when they are passed to ScriptSig as parameters. So again, the typical use is to verify that the person signing the transaction is the one who owns the coins. But this is not necessary the language can do much more than that.</p>
<p>The scripting language itself is really not a full programming language, but instead a Context Free Language without any loops. This ensures that every script that gets executed actually halts in a reasonable amount of time. It however does not guarantee that the script will be efficient, which has resulted in all sorts of <a href="https://en.bitcoin.it/wiki/Weaknesses#Denial_of_Service_.28DoS.29_attacks">denial of service attacks</a> on Bitcoin itself. <a href="https://bitcointalk.org/index.php?topic=140078.0">Example</a></p>
<p>Its worth noting that in the scripting language you can very easily push arbitrary data onto the stack (used for program state). This command in particular seems like a winner (and as a result lets you encode arbitrary data into the block chain):</p>
<table>
<tbody>
<tr>
<td>OP_PUSHDATA4</td>
<td>78</td>
<td>0x4e</td>
<td>(special)</td>
<td>data</td>
<td>The next four bytes contain the number of bytes to be pushed onto the stack.</td>
</tr>
</tbody>
</table>
<p>The problem with scripts is that the Bitcoin network has stopped relaying all non-standard transactions around the network. This effectively means that Bitcoin no longer support these custom scripts.</p>
<p>However, after looking at a fair number of other currencies including litecoin, dogecoin, fedoracoin, namecoin and mastercoin. I happened upon <a href="https://github.com/libcoin/libcoin">libcoin</a>. Libcoin is just a library that implements the protocol exactly as it was described in Satoshi's paper. Its goal is to make it easy to develop currencies with different parameters and they show this with a great example (<a href="https://github.com/libcoin/libcoin/blob/master/examples/ponzicoin/ponzicoin.cpp">ponzicoin</a>).</p>
<p>So I think the way forward is to figure how to create a new currency with libcoin, understanding which parameters allow for the addition of arbitrary data into the block chain and then launching a new currency. Sounds fun.</p>
<p>There are a fair number of side notes I just want to mention.</p>
<ul>
<li>Namecoin is essentially a proof of concept for alternative uses of the block chain. It was a key-value store whose purpose was to provide decentralized DNS. Due to fundamental errors in the implementation documented <a href="https://bitcointalk.org/index.php?topic=310954.0">here</a>, the project all but died. Aaron Schwartz is sometimes credited with envisioning <a href="http://www.aaronsw.com/weblog/squarezooko">this</a> use of bitcoin in 2012.</li>
<li>Mastercoin essentially implemented my original idea, which was to encode information ontop of bitcoin and takes it even further by describing ways to derive other currencies using that information. It is all very meta, hard to understand and ultimately doomed for failure, as this nice man <a href="http://themisescircle.org/blog/2013/12/20/mastercoin-is-a-nightmare-of-insanity/">documents</a>.</li>
<li>Satoshi's original <a href="http://www.mail-archive.com/cryptography@metzdowd.com/msg10001.html">release</a> of his paper had some interesting discussion attached to it. <a href="http://www.mail-archive.com/cryptography@metzdowd.com/msg10001.html"><br />
</a></li>
<li>These <a title="guys" href="http://www.ethereum.org/">guys</a> want to make the bitcoin scripting language Turing complete. That seems like a bad idea.</li>
</ul>
<p>&nbsp;</p>
