---
layout: post
status: publish
published: true
title: SSLEye draft from SSLDump
author: AbiusX
author_login: abbas
author_email: an2wm@virginia.edu
author_url: https://abiusx.com
wordpress_id: 252
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=252
date: '2014-03-20 17:47:14 -0400'
date_gmt: '2014-03-20 17:47:14 -0400'
tags: []
categories: 14s-pet
comments: []
---
<p>Its been a while since I've posted, but I'm here with updates on what I'm doing.</p>
<p>The initial idea was to create something that discovers legit MITM attacks (<a title="PKI Going Down (SSL Pinning)" href="http://crypto.cs.virginia.edu/courses/14s-pet/pki-going-down-ssl-pinning/">original post</a>). Instead of creating a proxy (which is what everyone else has done) I decided to go with a passive network sniffer. The proxy would not work on non-trivial applications that do not respect system proxy configurations (such as some IM clients and games), and the passive mode is enough in most cases to prevent leakage of data, as when you bring up a secured login dialog which is MITMed, you get a notice from the application and don't provide your credentials.</p>
<p>I found <a href="http://www.rtfm.com/ssldump/">SSLDump</a>, which is an open source tool by the same author of the book on SSL/TLS. I was able to master SSLDumps code, and modify it in a way that extracts X.509 certificates from live network traffic, or a captured traffic file. The next step, was to add GUI elements and refurbish the code for new software. Unfortunately, this was not possible, as SSLDump is a very old C code, based on K&amp;R C standard (an old standard made before ANSI C), and is not compatible with C++ and many new C libraries and toolkits.</p>
<p>Now I'm rewriting the SSLDump code from scratch, using nice ANSI C code. I'm more than 50% in the progress, and when finished, will add GUI elements and backend SQLite database. The two major problems with the rewrite are, that first the initial code model uses Observer Pattern to analyze network traffic, which is very hard to track, debug and maintain. It uses that pattern because it wants to allow multiple analyzers to work cooperatively.</p>
<p>The second problem is that SSL is a stateful protocol, but PCAP (which the network sniffing tools rely on) is not. It receives each packet separately, and its the duty of the developer to establish and track state for analysis. Fortunately I don't need to build the entire state and track it because I only need X.509 certificates.</p>
<p>I'm planning on having a free release, as well as a nicer commercial release.</p>
