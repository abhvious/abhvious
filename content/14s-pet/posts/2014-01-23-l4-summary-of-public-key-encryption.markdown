---
layout: post
status: publish
published: true
title: L4 - Summary of public key encryption
author: abhi
author_login: abhi
author_email: abhi@virginia.edu
wordpress_id: 35
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=35
date: '2014-01-23 19:58:32 -0500'
date_gmt: '2014-01-23 19:58:32 -0500'
tags: []
categories: 14s-pet
comments: []
---
<p>Video review materials:</p>
<ul>
<li><a href="https://class.coursera.org/crypto-preview/lecture/55">PKE definitions</a> (16m)
<li><a href="https://class.coursera.org/crypto-preview/lecture/57">RSA</a> (18m)
<li><a href="https://class.coursera.org/crypto-preview/lecture/58">PKCS padding</a> (15m)
<li><a href="https://class.coursera.org/crypto-preview/lecture/60">RSA in practice</a> (15m)
<li><a href="https://class.coursera.org/crypto-preview/lecture/41">Password-based Key derivations</a> (14m)
</ul>
