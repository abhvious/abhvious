---
layout: post
status: publish
published: true
title: Ciphertext-Policy Attribute-Based Encryption (CPABE)
author: tommy
author_login: tommy
author_email: tjt7a@virginia.edu
wordpress_id: 272
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=272
date: '2014-03-31 04:56:52 -0400'
date_gmt: '2014-03-31 04:56:52 -0400'
tags: []
categories: 14s-pet
comments: []
---
<p>John Bethencourt worked with Amit Sahai (advisor) and Brent Waters (advisor) on the <a title="http://hms.isi.jhu.edu/acsc/cpabe/" href="http://hms.isi.jhu.edu/acsc/cpabe/">CPABE toolkit</a>, an implementation of the ciphertext-policy attribute-based encryption scheme presented in their <a title="https://www.cs.utexas.edu/~bwaters/publications/papers/cp-abe.pdf" href="https://www.cs.utexas.edu/~bwaters/publications/papers/cp-abe.pdf">paper</a>. This toolkit is built on the <a title="http://crypto.stanford.edu/pbc/" href="http://crypto.stanford.edu/pbc/">PBC</a> algebra library, and is split into a crypt library called <a title="http://hms.isi.jhu.edu/acsc/cpabe/libbswabe-0.9.tar.gz" href="http://hms.isi.jhu.edu/acsc/cpabe/libbswabe-0.9.tar.gz">libbswabe</a> and the user interface functions library <a title="http://hms.isi.jhu.edu/acsc/cpabe/cpabe-0.11.tar.gz" href="http://hms.isi.jhu.edu/acsc/cpabe/cpabe-0.11.tar.gz">cpabe</a>.</p>
<p>In the ciphertext-policy attribute-based encryption scheme, each user's private key (decryption key) is tied to a set of attributes representing that user's permissions. When a ciphertext is encrypted, a set of attributes is designated for the encryption, and only users tied to the relevant attributes are able to decrypt the ciphertext.</p>
<p>The example presented on the website presents a ciphertext encrypted such that only employees with the attributes "Human Resources" UNION "Executive" are able to decrypt it. HR employees have the "Human Resources" attribute tied to their private keys, and Executive employees have the "Executive" attribute tied to their private keys. Both groups, therefore, are able to decrypt the encrypted message.</p>
<p>Unlike other Role-Based Access Control (RBAC) systems, CPABE does not require a trusted authority, or any form of storage. The encryption itself serves as the RBAC mechanism.</p>
<p>To Install:</p>
<p>     On a Mac: sudo port install cpabe</p>
<p>     On linux: download the tarball, untar, configure, make, make install</p>
<p>     On windows: bad idea</p>
<p>To Setup:</p>
<p>     cpabe-setup: This program generates the public key and master keys.</p>
<p>In addition to the compressed binary representation, the public key also contains legible parameters:</p>
<pre>----------

type a

q 8780710799663312522437781984754049815806883199414208211028653399266475630880222957078625179422662221423155858769582317459277713367317481324925129998224791

h 12016012264891146079388821366740534204802954401251311822919615131047207289359704531102844802183906537786776

r 730750818665451621361119245571504901405976559617

exp2 159

exp1 107

sign1 1

sign0 1

----------</pre>
<p>cpabe-keygen: This program allows the user to produce private keys associated with a set of attributes. The example presented on the website was creating two new private keys for new employees Sara and Kevin; it should be noted that the Master Key is required to generate these keys! It is critical that the user keep this key private:</p>
<pre>----------
$ cpabe-keygen -o sara_priv_key pub_key master_key \
    sysadmin it_department 'office = 1431' 'hire_date = '`date +%s`
$ cpabe-keygen -o kevin_priv_key pub_key master_key \
    business_staff strategy_team 'executive_level = 7' \
    'office = 2362' 'hire_date = '`date +%s`
----------</pre>
<p>    It's clear from the code above that Sara is a system administrator in the IT department, has office room 1431, and was hired today. Kevin is a business staff member on the Strategy Team, has executive level 7 permissions, works in room 2362, and was hired today as well.</p>
<p>These private keys belong to Sara and Kevin and would serve as their decryption keys for messages sent from the user that generated their private keys.</p>
<p>To Use:</p>
<p>In order for the generating user to send an encrypted message, that user would use cpabe-enc</p>
<p>cpabe-enc:  This program encrypts a message using a public key and a set of attributes.</p>
<pre>----------
$ cpabe-enc pub_key security_report.pdf
    (sysadmin and (hire_date &lt; 946702800 or security_team)) or
    (business_staff and 2 of (executive_level &gt;= 5, audit_group, strategy_team))
----------</pre>
<p>The code above shows a security report encrypted with the user's public key and a set of attributes. Only (system administrators AND (hired before a certain date or on the security team)) OR (business staff AND 2 OF THE FOLLOWING (with an executive level 5 or greater, in the audit group, or on the strategy team) can decrypt the message. Looking back at Kevin and Sara, only one of the two has the necessary attributes. Kevin can decrypt this message with his private key; Sara cannot.</p>
<p>In order for Kevin to decrypt, he must use his private key, the encrypter's public key, and the cpabe-dec function:</p>
<p>cpabe-dec: This program decrypts an encrypted message using the encrypting user's public key, and the decrypting user's private key. The decrypted file will share the name with the encrypted file minus the .cpabe.</p>
<p>Kevin could decrypt the message using the following syntax:</p>
<pre>----------
$ cpabe-dec pub_key kevin_priv_key security_report.pdf.cpabe
----------</pre>
<p>Sarah would receive an error if she attempted to decrypt with her private key.</p>
