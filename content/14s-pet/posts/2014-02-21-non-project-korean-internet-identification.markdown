---
layout: post
status: publish
published: true
title: "(non-project) Korean Internet Identification"
author: jack
author_login: jack
author_email: jhd3pa@virginia.edu
wordpress_id: 180
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=180
date: '2014-02-21 05:10:21 -0500'
date_gmt: '2014-02-21 05:10:21 -0500'
tags: []
categories: 14s-pet
comments: []
---
<p>See <a href="http://en.wikipedia.org/wiki/Resident_registration_number" target="_blank">information on Wikipedia</a></p>
<p>Apparently the system is enforced by websites themselves, not by ISPs, and actually predates the internet. It looks like it's currently being replaced by some sort of government-issued digital certificate, but that section of the wikipedia article isn't very well written.</p>
