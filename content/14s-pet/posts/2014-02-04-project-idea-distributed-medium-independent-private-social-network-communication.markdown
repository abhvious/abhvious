---
layout: post
status: publish
published: true
title: 'Project Idea: Distributed, Medium-Independent, Private Social Network Communication'
author: tommy
author_login: tommy
author_email: tjt7a@virginia.edu
wordpress_id: 93
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=93
date: '2014-02-04 18:49:34 -0500'
date_gmt: '2014-02-04 18:49:34 -0500'
tags: []
categories: 14s-pet
comments:
- id: 28
  author: abhi
  author_email: abhi@virginia.edu
  author_url: ''
  date: '2014-02-11 18:18:16 -0500'
  date_gmt: '2014-02-11 18:18:16 -0500'
  content: "Can it be organized as a layer on top of all other social mechanisms?
    \ Ie, i can still post to whatever network I want. But the content is encrypted,
    and a special plugin or system that anyone can install can be used to read/view
    it ?\r\n\r\nThis layer has all the key management issues implemented."
---
<p>By: Tom Tracy II, Nathan Brunelle</p>
<p>Our project idea is to create a distributed, medium-independent cryptographic system which enables secure communication in a social network. The core of the system will use <a title="https://gnunet.org/sites/default/files/CCS%2706%20-%20Attributed-based%20encryption%20for%20fine-grained%20access%20control%20of%20encrypted%20data.pdf" href="https://gnunet.org/sites/default/files/CCS%2706%20-%20Attributed-based%20encryption%20for%20fine-grained%20access%20control%20of%20encrypted%20data.pdf">attribute-based encryption</a>.  For a particular user, different members of the social network are assigned different friendship attributes. Each of these friends is then issued a decryption key by this user valid for these attributes. The user can encrypt a message and choose which friend attributes are necessary for decrypting the message. The user can use set union, intersection, and flat negation to create combinations of attributes when encrypting the message. This cipher text can then only be decrypted by friends with a decryption key with proper attributes. In the context of a social network, these attributes can be tied to relationships like family member, co-worker, or neighbor. In effect, this would allow a user to encrypt a message such that only family members and neighbors but not co-workers can read the message.</p>
<p><span style="line-height: 1.5em">Our goal is to create a web of relationships between users and enable private communication among subsets of users in this social network. </span></p>
<p>Some components of our proposed system include:</p>
<p>1. A method for friend discovery. How does Nathan find Tommy and ask to be his friend?</p>
<p>2. A de-centralized method for key management. Where are the keys stored?</p>
<p>3. How to "defriend" or <span style="line-height: 1.5em">modify attribute assignment. How are the keys updated? How do I change the members of my 'co-workers' group when I get fired?</span></p>
<p><span style="line-height: 1.5em">4. How to make sure your system is not machine-restricted. Can I run this on my phone?</span></p>
<p>&nbsp;</p>
<p>Related Work: <a title="https://www.usenix.org/conference/usenixsecurity12/technical-sessions/presentation/feldman" href="https://www.usenix.org/conference/usenixsecurity12/technical-sessions/presentation/feldman">Friendtegrity Usenix Presentation</a></p>
<p>&nbsp;</p>
