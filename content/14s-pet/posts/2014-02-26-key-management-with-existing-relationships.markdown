---
layout: post
status: publish
published: true
title: Key Management with Existing Relationships
author: tommy
author_login: tommy
author_email: tjt7a@virginia.edu
wordpress_id: 208
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=208
date: '2014-02-26 04:44:04 -0500'
date_gmt: '2014-02-26 04:44:04 -0500'
tags: []
categories: 14s-pet
comments:
- id: 40
  author: jack
  author_email: jhd3pa@virginia.edu
  author_url: ''
  date: '2014-02-27 02:34:11 -0500'
  date_gmt: '2014-02-27 02:34:11 -0500'
  content: "In answer to your questions:\r\n\r\n1. You can do what gmail does and
    keep fingerprints of the machines that are requesting keys from the key storage
    server (or messages/data if you also control the message transport mechanism,
    but my understanding was that you don't). Of course, this only works if the attacker
    actually has to request keys, and so doesn't protect against MitM attacks or key
    theft from Alice's local machine. It's also only effective if someone (presumably
    Alice) is reading the logs.\r\n\r\n 2. Assuming you can't find a workable blacklist
    mechanism (it seems a difficult task to me), you could always force Alice to renegotiate
    keys with *all* of her friends, regardless of their aspects. This will at worst
    inconvenience all of her friends (presumably a reasonable number, not more than
    a few thousand, but probably much less) by a constant amount."
---
<p>By: Nathan Brunelle, Tommy Tracy</p>
<p>By our Friend Discovery Protocol shared last week, it has become apparent that the integrity of the whole social network is dependent on the security of our symmetric keys. These symmetric keys serve as the means of private communication with friends and are used for encryption/decryption key management purposes. To be clear, symmetric keys are used for key management. Aspect-based encryption and decryption keys are used for secure messaging. These are two, separate secure channels. Symmetric keys may also be used as individual's unique identifier (we want to avoid the necessity of a centralized ID authority). In the following paragraph, we present a few use cases which demonstrate the importance of keeping symmetric keys private. In all cases we take on Alice's perspective:</p>
<p>Case 1: Alice and Bob are friends. Eve has somehow obtained Bob's decryption key for Alice, meaning that Eve can now read any of Alice's messages readable by Bob. If either Bob or Alice are aware that Bob's decryption key has become compromised, Alice can use Bob's symmetric key to issue a new decryption key to Bob. Alice will then void the aspects that Bob had on his decryption key, and re-negotiate encryption/decryption keys with everyone that shares aspects with Bob. Note that this does not protect the privacy of all previous messages. Those are now readable by Eve.</p>
<p>Case 2: Alice and Bob are friends. Eve has somehow learned the Alice-Bob symmetric key. Eve can now use this key to request a new decryption key from Bob, and is now able to read all of Bob's messages. Should Alice become aware of this, Alice may request Carole notify Bob of a private key vulnerability. This message will include a public key so that Bob may securely send a new symmetric key to Alice; at this point, the two will re-negotiate symmetric keys, and subsequently encryption/decryption keys. Eve will lose her snooping capabilities.</p>
<p>Case 3: Eve somehow obtains the symmetric keys for all of Alice's friends. In this situation Eve now has the power to hijack Alice's network. This is because Eve can use the stolen symmetric keys to obtain a whole new key set (symmetric, decryption, aspects) for all of her friends. Now Alice is unable to send/receive messages from any of her friends, and is unable to notify them of a vulnerability through the network. For Alice to recover from such an attack she must notify her Primary friends that her network has been hijacked. These friends immediately drop all of Alice's old keys, and notify their friends of the hijack. Those friends drop their keys as well. Finally, Alice re-generates her new network by following the Friend Discovery Protocol.</p>
<p>From the above cases it should be evident that Alice MUST protect her symmetric keys most tightly. To accomplish this, we propose using the CryptDB protocol as our key storage <a title="CryptDB" href="http://people.csail.mit.edu/nickolai/papers/raluca-cryptdb.pdf">CRYPTDB</a>. This allows for efficient queries over an encrypted database. Also, it allows for assigning permissions, similar to a normal database management system. What these permission allow for is that should some user be compromised, only those data items available to the hacked user are readable by the adversary. This property allows us to make a tradeoff between portability and security. Alice will store all her keys in a database on some server. Each device/application which Alice uses to access her network will be viewed as a different "account" on this database. It should be the case that only one particularly secure device/application will have permission to access the symmetric keys, all other devices may access the other keys. This provides that, for example, should Alice's iPhone become compromised, and Eve has access to all decryption and aspect keys, Alice may from this safe machine refresh the keys in her network.</p>
<p>Questions to be answered:</p>
<p>Is there a good way to discover when keys have been leaked?</p>
<p>It may be the case that to assign a new key to Bob this would require new keys for everyone who shares an aspects with Bob. We would like to be able to somehow blacklist Bob's old key in such a way where the old key is no longer able to decrypt new messages, and all other users' keys are still valid. Without such power, there is an attack by which Bob (with collusion) can learn his aspects. This is done by requesting a new key, and then asking all mutual friends he has with Alice whether they received a new key from Alice.</p>
