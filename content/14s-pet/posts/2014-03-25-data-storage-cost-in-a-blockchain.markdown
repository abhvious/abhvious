---
layout: post
status: publish
published: true
title: Pre-Nineties Prices for Blockchain Storage
author: nick
author_login: nick
author_email: nskelsey@gmail.com
wordpress_id: 255
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=255
date: '2014-03-25 03:35:01 -0400'
date_gmt: '2014-03-25 03:35:01 -0400'
tags: []
categories: 14s-pet
comments: []
---
<p>In the process of generating valid bitcoin transaction with arbitrary data I thought it might be a good idea to figure out a reasonable and cost efficient method to store that data. In the end, I made a spreadsheet and learned some interesting things. <strong>TL;DR it costs 1/2 a penny to have the litecoin network store your tweet forever and ever</strong><strong><br />
</strong></p>
<p>There are a-couple different methods to storing data in the block chain, they all involve specially formatting transactions or pairs of transactions. You can either generate a transaction normally and use the address as your message (denoted Regular). You can use the new fangled OP_RETURN method which was added specifically to do this. You can use the crappy (as I found out) method I constructed (denoted multisig 1-3 redeem). Or you can use another method that is soon to be disallowed known as OP_CHECKMULTISIG.</p>
<p>I generated a spreadsheet that looks at this cost in detail by assuming that we will always pay the minimum fee required by nodes running bitcoind. I used the minimum transaction fee because miners only care right about the reward from block generation than from fees collected from transactions.</p>
<table dir="ltr" border="1" cellspacing="0" cellpadding="0">
<col width="120" />
<col width="120" />
<col width="120" />
<col width="130" />
<col width="120" />
<tbody>
<tr>
<td><strong>Storage Method</strong></td>
<td>Regular</td>
<td>OP_RETURN</td>
<td>multisig 1-3 redeem</td>
<td>OP_CHECKMULTI</td>
</tr>
<tr>
<td><strong>Message Size (bytes)</strong></td>
<td><strong>Current Cost ($)</strong></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td>70</td>
<td>0.20314</td>
<td>0.05804</td>
<td>0.35024</td>
<td>0.05804</td>
</tr>
<tr>
<td>140</td>
<td>0.40628</td>
<td>0.05804</td>
<td>0.70048</td>
<td>0.08377</td>
</tr>
<tr>
<td>1000</td>
<td>2.90200</td>
<td>0.05804</td>
<td>5.00345</td>
<td>0.59835</td>
</tr>
<tr>
<td>5000</td>
<td>15.00334</td>
<td>0.37001</td>
<td>26.52328</td>
<td>3.32085</td>
</tr>
<tr>
<td>10000</td>
<td>30.00668</td>
<td>0.74001</td>
<td>53.04656</td>
<td>6.64169</td>
</tr>
<tr>
<td>100000</td>
<td>300.06680</td>
<td>7.40010</td>
<td>530.46559</td>
<td>66.41691</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>The full spreadsheet lives <a href="https://docs.google.com/spreadsheet/pub?key=0AqeLZN-5NKsPdFp0VjVLWEJvOTRrQWNFay1VTW5FdFE&amp;output=html">here</a>.</p>
<p>These costs were computed using the current BTC/USD price of $580.05. Interestingly the new OP_RETURN accomplishes its intended goal only for people that want to store large transactions. The whole reason the developers made the script standard was to encourage people to store data in what they call provably unspendable outputs. This way, nodes no longer have to pay attention to or care about those transactions and can thus reduce the amount of data they have to store. However, for a message of say 40 bytes or less the cost of just storing a message regularly ends up only being 2x the cost of using this new fangled method.</p>
<p>Now things get interesting if we drop the coin to dollar conversion down to the litecoin market price of $16.14. We see much lower costs:</p>
<table dir="ltr" border="1" cellspacing="0" cellpadding="0">
<col width="120" />
<col width="120" />
<col width="120" />
<col width="130" />
<col width="120" />
<tbody>
<tr>
<td><strong>Storage Method</strong></td>
<td>Regular</td>
<td>OP_RETURN</td>
<td>multisig 1-3 redeem</td>
<td>OP_CHECKMULTI</td>
</tr>
<tr>
<td><strong>Message Size (bytes)</strong></td>
<td><strong>Current Cost ($)</strong></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td>70</td>
<td>0.00561</td>
<td>0.00160</td>
<td>0.00968</td>
<td>0.00160</td>
</tr>
<tr>
<td>140</td>
<td>0.01123</td>
<td>0.00160</td>
<td>0.01936</td>
<td>0.00232</td>
</tr>
<tr>
<td>1000</td>
<td>0.08020</td>
<td>0.00160</td>
<td>0.13828</td>
<td>0.01654</td>
</tr>
<tr>
<td>5000</td>
<td>0.41463</td>
<td>0.01023</td>
<td>0.73300</td>
<td>0.09178</td>
</tr>
<tr>
<td>10000</td>
<td>0.82927</td>
<td>0.02045</td>
<td>1.46600</td>
<td>0.18355</td>
</tr>
<tr>
<td>100000</td>
<td>8.29268</td>
<td>0.20451</td>
<td>14.66001</td>
<td>1.83551</td>
</tr>
</tbody>
</table>
<p>These charts lead me to two interesting conclusions. The first is that it would cost 1/2 a penny to store a tweet in the Litecoin blockchain (Can't beat a replication factor &gt; 1000). And second, as long as the fee structure stays the way it does someone like me is totally incentivized to store data for close to nothing while the rest of the network has to bear the transmission and storage cost forever. This could be construed as a tragedy of the commons or as a scaling problem depending on what your background is in.</p>
