---
layout: post
status: publish
published: true
title: Attacking Microsoft's Picture-Gesture Passwords
author: peter
author_login: peter
author_email: pts2td@virginia.edu
wordpress_id: 173
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=173
date: '2014-02-20 07:41:31 -0500'
date_gmt: '2014-02-20 07:41:31 -0500'
tags: []
categories: 14s-pet
comments: []
---
<p>In discussing passwords last Tuesday, we talked about Android Swipe gestures, which got me thinking about <a href="http://windows.microsoft.com/en-us/windows-8/personalize-pc-tutorial" target="_blank">Windows 8's new Picture Gesture passwords</a> and how attacks against them might be possible. The users basically take a picture of their choosing and add three "gestures" to it (any combination of circles, lines, and points).</p>
<p>Unsurprisingly, many people also saw the opportunity for attacks against this system. Users often choose very similar points of interest on their pictures to perform the same gestures. <a href="http://0b4af6cdc2f0c5998459-c0245c5c937c5dedcca3f1764ecc9b2f.r43.cf2.rackcdn.com/12316-sec13-paper_zhao.pdf" target="_blank">In this paper from ASU</a>, the researcher took data from a group of undergraduate students who had to use this system to get into a class website for a whole semester, and told some people Mechanical Turk that they would use a password to access a bank account.</p>
<p>As you may have guessed, most users chose to use pictures of people (almost half), and of those, a strong majority of the gestures are focused on parts of the face. About 86% of pictures of people have at least one gesture involving someone's eyes. It's basically apparent that all passwords involve specific, identifiable features on the picture. Two users used Van Gogh's <em>Starry Night</em> and used the stars as three points.</p>
<p>Analyzing the picture allowed them to generate a dictionary of passwords for each picture. This included color and shape identification, and facial recognition of the nose, mouth and eyes.</p>
<p>Given the dictionaries generated for each Dataset, they use the BestCover Algorithm to reduce the number of passwords and prioritize the points of interest. With these datasets, they were able to guess up to 48% of passwords with 2^19 (about 550,000) guesses. The rate on the second dataset was lower (24%) because those were generated for a bank account and only used once. Pictures with fewer points of interest like planes in the sky could be cracked at 40%  with less than 6000 tries, portaits of people could be had at 29% with 2^19 tries, and pictures of artificial objects and landscapes proved the most difficult, due to a greater number of more obscure points of interest.</p>
<p>Windows only allows 5 guesses on a login screen before requiring a text password, and the percentages on those proved rather low, but I thought this was a very interesting study basically on how the ease of use ( the pictures especially) gives so much information to the adversary that allows for a specifically tailored dictionary attack. If anyone has found anything similar that hasn't been extensively studied I would love to hear about it and talk further action on it.</p>
