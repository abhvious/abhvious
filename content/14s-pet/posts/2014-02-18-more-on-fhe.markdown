---
layout: post
status: publish
published: true
title: More on FHE
author: divy
author_login: divy
author_email: ds5nu@virginia.edu
wordpress_id: 171
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=171
date: '2014-02-18 20:22:33 -0500'
date_gmt: '2014-02-18 20:22:33 -0500'
tags: []
categories: 14s-pet
comments: []
---
<p>Recently I have been reading more on Fully Homomorphic Encryption (FHE) and looking at implementations of these algorithms. I read the papers below, and downloaded a simple RPN function using HElib to  better understand FHE.</p>
<p>FHE supports both addition and multiplication, as opposed to just one operation. Gentry proposed a FHE algorithm using ideal lattices that was then modified and implemented. Some background is necessary to understand the algorithm.</p>
<p>We begin by defining a lattice to be the integral linear combinations of a basis of Rn. Every lattice has an infinite number of bases, but also a unique basis called the Hermite Normal Form (HNF), which can be computed given any basis of the lattice L. The HNF often serves as the public key representation of the lattice.</p>
<p>An <em>ideal </em>lattice is a subset of a certain type of ring* that is closed under addition and multiplication.</p>
<p><strong>Gentry's FHE Cryptosystem:</strong></p>
<p>Gentry begins with making a somewhat-homomorphic cryptosystem. In this system the public key is a "bad" basis of an ideal lattice with a basis of a "small" ideal. We note that a "bad" basis <em>B </em>will have long and/or spatially close vectors. A ciphertext <em>c</em> in this system would be a vector close to a point in the lattice L, and the message would be embedded in the distance to the closest lattice point. We set a vector <em>e </em>= 2<em>r</em> + <em>m</em> where m is the message and r is a small random vector. Then we get the ciphertext <em>c </em>by <em>e </em>mod <em>B</em>. <strong></strong>The secret key is a short vector <em>w</em> in L^-1. To decrypt the message we compute the difference between (<em>w</em> x <em>c</em>) and the integer closest to it (fractional part). This equals the fractional part of <em>w </em>x <em>e,</em> which exactly equals <em>w</em> x <em>e</em> for short <em>w </em>and <em>e</em>. If this is the case, the decryptor can recover <em>e</em> and then <em>m. </em>This system can only evaluate low degree polynomials since <em>e</em> will grow beyond the decryption capabilities otherwise. In order to solve this, Gentry used bootstrapping. A somewhat-homomorphic scheme is called bootstrappable if it can homomorphically evaluate (1) and (2) below for any two ciphertexts.</p>
<p>(1) DAdd[c1,c2](sk) = Dec[sk](<em>c1</em>) + Dec[sk](<em>c2</em>)</p>
<p>(2) DMul[c1,c2](sk) = Dec[sk](<em>c1</em>) x Dec[sk](<em>c2</em>)</p>
<p>If this is the case, the somewhat-homomorphic scheme can be turned into a fully-homomorphic one by adding an encryption of the secret key <em>s</em> to the public key. Then the addition and multiplication of two ciphertexts can be computed by evaluating DAdd and DMul on <em>s. </em>Gentry's scheme was not bootstrappable, so he found a way to "squash" his decryption circuit such that it became bootstrappable. The idea behind this was to include a set of vectors that have a subset that add up to <em>w</em> in the public key.</p>
<p>&nbsp;</p>
<p>Gentry proposed one of the first FHE cryptosystems, though HElib uses the <a title="BGV12" href="http://eprint.iacr.org/2011/277">Brakerski-Gentry-Vaikuntanathan</a> scheme. I plan to go over this scheme soon.</p>
<p>&nbsp;</p>
<p>References:</p>
<p>https://www.cs.cmu.edu/~odonnell/hits09/gentry-homomorphic-encryption.pdf</p>
<p>http://eprint.iacr.org/2010/520.pdf</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
