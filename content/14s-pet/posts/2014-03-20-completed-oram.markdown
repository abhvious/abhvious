---
layout: post
status: publish
published: true
title: Completed ORAM
author: saba
author_login: saba
author_email: se8dq@virginia.edu
wordpress_id: 247
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=247
date: '2014-03-20 15:32:03 -0400'
date_gmt: '2014-03-20 15:32:03 -0400'
tags: []
categories: 14s-pet
comments: []
---
<p>Over the week before spring break, I completed the basic ORAM construction I've been working on and tested it with the whole multilayered fuse system I had previously set up.  There are a couple small but persistent bugs which sometimes pop up, but they are in the reading/writing of files and not in the actual oram construction.  The oram which I have set up holds 32kb in 4kb blocks, with 3 buckets per node in the oram tree.  Unsurprisingly, due to the small size of the oram, local read/write operations complete instantaneously.  However, when connected to my google drive through google-drive-ocamlfuse, encfs, and my fuse_oram, read and write speeds are incredibly slow.  In fact, the time it took to complete a simple read or write was about 3 minutes!  The problem, however, does not actually lie directly in my oram but rather in the google-drive-ocamlfuse, which takes surprisingly long to read/write from google drive.  I'm not sure if this problem is intrinsic to the google drive api or if it's just a shortcoming of google-drive-ocamlfuse (probably the latter), but I should probably look into finding either a faster way to connect to google drive or another cloud storage provider to which I can connect faster if I want this to be practically usable.</p>
<p>My implementation of oram follows closely the basic construction of the oram we studied in class as described <a href="http://eprint.iacr.org/2013/243.pdf">here</a>, with a minor change so that overflow can only happen during the 'put back' stage and not during the flush.  This is easily accomplished by keeping an extra block in memory and always reading from the bucket at the next level down before writing back to the bucket at the current level of the tree in the flush.  Thus instead of having a read 1, write 1, read 2, write 2, ... pattern, there is a read 1, read 2, write 1, read 3, write 2, ... pattern. This doesn't change the way the oram works and therefore the correctness should be intact, but it avoids or at least delays overflows in some cases where the original scheme would not.</p>
<p>It should be noted that although my oram provides a means of reading/writing to blocks in the storage and supports a few basic filesystem operations, it is not a full filesystem and yet another fuse layer on top of it would be needed to implement a full filesystem that does things like supporting directory structures, spreading files over many blocks, etc.  Also, I use only the basic oram construction (with linear number of local registers) as opposed to the full-fledged recursive construction (with polylog registers) because, at least for smaller orams and my current setup, I think the larger local space needed can be sacrificed for not having to access multiple layers of oram and make even more costly reads from the drive.  Either the filesystem or the fully-fledged construction can be done later, but they're not really critical to the functioning of the oram (although they are important for practical usability for large amounts of data).</p>
<p>For next week (in addition to spending some time on minor improvements and fixes), I will (at prof Shelat's suggestion) run simulations of use on my oram and variations with different numbers of blocks or buckets to see how tight the bounds given in the paper are for the performance of the oram in real life.</p>
