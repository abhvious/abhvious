---
layout: post
status: publish
published: true
title: "(semi-project) Initial experiences with AVX"
author: jack
author_login: jack
author_email: jhd3pa@virginia.edu
wordpress_id: 83
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=83
date: '2014-02-01 05:47:12 -0500'
date_gmt: '2014-02-01 05:47:12 -0500'
tags: []
categories: 14s-pet
comments:
- id: 12
  author: abbas
  author_email: an2wm@virginia.edu
  author_url: https://abiusx.com
  date: '2014-02-02 02:11:35 -0500'
  date_gmt: '2014-02-02 02:11:35 -0500'
  content: "You're taking Defense Against the Dark Arts? :D Then technically I'm your
    TA right?\r\n\r\nKidding. These extensions are usually for graphic processing,
    or cryptography usages, not just improving random mathematical stuff."
- id: 18
  author: abhi
  author_email: abhi@virginia.edu
  author_url: ''
  date: '2014-02-06 18:01:00 -0500'
  date_gmt: '2014-02-06 18:01:00 -0500'
  content: I am very surprised; the AVX operations should also improve on simple dot-product
    calculations.  If this is indeed the case, then we should try and do more systematic
    testing to find concrete cases when AVX is much slower than the same program written
    with SSE.  It would be an interesting outcome.  However,  my intuition is that
    with a little rescheduling and loop-unrolling, etc., it would be possible to get
    better performance.
---
<p>As part of another course I was required over the last few days to write a program in raw x86 assembly making use of SSE. Since I was already 90% there, I decided to write a second version making use of AVX (since abhi's been interested in it), and learned a few things in the process. The first is that AVX isn't terribly hard to use (at least given an understanding of SSE - the differences are minimal). The second is that the improvement/regression over SSE is much more complex than I expected. Before I report any specifics, it's worth noting that my tests were very informal and not particularly well researched, and that they were run on a sandy bridge processor (the first chip with AVX, and lacking AVX2) in 32-bit mode (as required by the class, unfortunately).</p>
<p>The program in question was a very simple dot product calculation (time was measured over 10k runs, the inputs being two vectors of 2048 single precision floating point elements each, the first containing only 2s, and the second only 3s). I was quite surprised to find that the AVX version took almost twice as long to run as the normal SSE version, in spite of the fact that it required half as many iterations of the dot product loop. It turns out that this is because AVX instructions incur a performance pentalty, both for calculations, and *especially* for loads from cache into registers. Of course it's possible to load twice as many AVX registers as SSE registers in order to fill in the scheduling holes (this has no performance benefit for the SSE version, on my machine), but it turns out that the CPU has an easier time scheduling SSE optimally (since the delays are smaller), and so, at least for this problem and under these constraints, AVX is at best *almost* as fast as regular SSE (while presumably consuming more power, since it requires the use of more registers and vector units to attain the same speed).</p>
<p>That said, I don't think this result is necessarily indicative of how AVX2 might perform when used to implement a cryptosystem - after all, my tests were done for a single unrelated problem, using an irrelevant part of a different (but related) instruction set on an outdated processor. Certainly intel would never have released AVX if it had no benefit at all over classic SSE. However, this might be relevant to those systems with a high memory-access-to-computation ratio. For instance, assuming AVX2 has a similar performance penalty to AVX (I haven't checked), I suspect the speed increase when performing a xor between a key and a pain/cyphertext may be minimal or nonexistant. On the other hand, there is likely plenty of room for improvement in systems with a low memory-access-to-computation ratios, assuming care is taken in the implementation to allow the processor to schedule optimally.</p>
<p>Note, this isn't the project I've been intending to pursue, but I still thought this was interesting enough to be worth posting. I originally titled this post "initial experiences with AVX, and other thoughts on other matters", but it's late and I've already written a lot, so perhaps if you're all very unlucky I'll write another extra-verbose post soon about the other things that have been floating through my mind.</p>
<p>Good night,<br />
Jack</p>
