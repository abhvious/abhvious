---
status: publish
published: true
title: L1 - a review of some basic crypto
author: abhi
author_login: abhi
author_email: abhi@virginia.edu
wordpress_id: 28
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=28
date: '2014-01-14T12:25:35'
date_gmt: '2014-01-14 12:25:35 -0500'
tags: []
categories: 14s-pet
comments: []
---
<p>This lecture is flipped.  Please spend 90 minutes reviewing the following online videos:</p>
<ul>
<li><a href="https://class.coursera.org/crypto-preview/lecture/5">Stream Ciphers</a> (20m)
<li><a href="https://class.coursera.org/crypto-preview/lecture/7">Real-world stream ciphers</a> (20m)
<li><a href="https://class.coursera.org/crypto-preview/lecture/9">Semantic security, what it means for a cipher to be secure</a> (15m)
<li><a href="https://class.coursera.org/crypto-preview/lecture/12">Block ciphers</a> (17m)
<li><a href="https://class.coursera.org/crypto-preview/lecture/16">AES</a> (14m)
<li><a href="https://class.coursera.org/crypto-preview/lecture/21">CTR Mode of operation, how to actually use a block cipher</a> (10m)
</ul>
<p>Review the questions in <a href="https://class.coursera.org/crypto-preview/quiz/attempt?quiz_id=57">the first problem set</a> and <a href="https://class.coursera.org/crypto-preview/quiz/attempt?quiz_id=58">the second problem set</a>, we will spend the first minutes of L2 going over a subset of these questions.</p>
