---
layout: post
status: publish
published: true
title: User credentials in a Distributed Hash Table
author: nick
author_login: nick
author_email: nskelsey@gmail.com
wordpress_id: 195
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=195
date: '2014-02-25 05:12:31 -0500'
date_gmt: '2014-02-25 05:12:31 -0500'
tags: []
categories: 14s-pet
comments: []
---
<p>This post is going to be an explanation of <a href="https://github.com/maidsafe/MaidSafe/wiki/unpublished_papers/SelfAuthentication.pdf?raw=true">this paper</a>, which is a small portion of the much larger project called <a href="http://maidsafe.net/">MaidSafe</a> (Massive Array of Internet Disks). As a quick overview, the developers behind MaidSafe claim they have created a scalable, consistent, secure and completely distributed data-storage network. For the average user, this means you offer up some space on your machine for the network to use and then you can store anything you want in the network (as long as you don't try to store more than you offered.)</p>
<p>Its pretty straight forward, but as always the devil is in the details. Every chunk of data must have an address and every node is a member of a bucket where a subset of addresses map to that bucket. (this is for replication) Right now I am taking their solutions to the many potential problems this causes on faith. (They use Kademelia hashing + some sort of public key infrastructure). I am going to instead talk about how a single user can join this system and then use it to store arbitrary amounts of data without revealing who they are or how much information they are storing.</p>
<p>A user account in the MaidSafe network is an encrypted chunk of data that stores the addresses and private keys for other pieces of data that the user 'owns'. The developers call these chunks passports since being able to decrypt that one chunk gives you access to potentially many more chunks of data. </p>
<p>So, lets say I am a new user that wants to privately store 6 new files in this cloud thing. Lets assume the user is running a node that is already on the network and can publish into the network (more on that in other posts). Lets also assume that no malicious party can take over a single bucket. Then the steps are as follows:</p>
<p>(1) The user picks a username U and password W, a salt S is deterministically derived from U + W<br />
(2) The user computes H(U+S) and checks if the network is already storing something at the address H(U+S).<br />
(3) If not, the user stores a random string (RndStr) encrypted using AES and the key from PBKDF2(U, S) at address H(U+S)<br />
(4) The user can now publish encrypted files into the network recording the keys used.<br />
(5) The client bundles the encryption keys and network addresses used into one single data chunk.<br />
(6) Now the user can save his 'account' on the network at address H(U+S+RndStr) by encrypting his account data using PDKF2(W, S) and AES. </p>
<p>Since we have a method of finding RndStr in the network and decrypting the account packet we can store references to encrypted data using only a username and password. If that didn't make sense here is a picture!</p>
<p><img src="http://example.dev.nskelsey.com/self-authentication.gif" alt="self-auth" /></p>
<p>If you accept the assumption that the buckets holding Enc(RndStr) and Enc(Account) have an honest route to the user and are composed of at least one honest node then the user will always be able to recover their account. Which is a cool result.</p>
