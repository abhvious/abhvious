---
layout: post
status: publish
published: true
title: 'Program Obfuscation: Progress Update 2'
author: Amir
author_login: ameer
author_email: am8zv@virginia.edu
wordpress_id: 151
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=151
date: '2014-02-16 05:53:59 -0500'
date_gmt: '2014-02-16 05:53:59 -0500'
tags: []
categories: 14s-pet
comments: []
---
<p>I started implementing the framework for building circuits and branching programs. I should be done with that by next week and start with the Multi-linear Jigsaw Puzzles.</p>
<p>In this post, I'll go over Barrington's theorem, which is central to the idea of creating a representation for the inputs to Kilian's protocol. This corresponds to step 1 of the procedure described <a title="Program Obfuscation: Progress Update 1" href="http://crypto.cs.virginia.edu/courses/14s-pet/2014/02/07/program-obfuscation-progress-update-1/" target="_blank">here</a>. The theorem states that for any Boolean circuit <em>C </em>: {0,1}<sup>k</sup> -&gt; {0,1} with fan-in-2 gates and depth <em>d</em>, there exists and oblivious linear branching program <em>BP</em> of width 5 and length at most 4<sup><em>d</em></sup> that computes the same function as <em>C</em>.</p>
<p>First, what are branching programs? There exists various representation of branching programs. In our case, we can think of a branching program as a sequence of <em>n</em> tuples where tuple <em>i</em> is of the form (inp(<em>i</em>), <em>A<sub>i,0</sub></em>, <em>A<sub>i,1</sub></em>). The function inp is a mapping from [<em>n</em>] to [<em>k</em>] and <em>A<sub>i,0</sub></em> and <em>A<sub>i,1</sub></em> are <a href="http://en.wikipedia.org/wiki/Permutation_matrix" target="_blank">permutation matrices</a>. There is a pair of distinct permutation matrices that represent the values 1 and 0 (let's call these <em>D<sub>1</sub></em> and <em>D<sub>0</sub></em>, respectively). To evaluate the branching program over some <em>k</em>-bit input <em>x</em> we take matrices <em>A<sub>i,u</sub></em> (where <em>u</em> is the value of <em>x</em>'s <em>i</em>'th bit) for <em>i</em> = 1...<em>k </em>and multiply the chosen matrices in order. The resulting product should be either <em>D<sub>1</sub></em> or <em>D<sub>0</sub></em>.</p>
<p>Now, there is a mechanical way of applying Barrington's theorem on a circuit. One way to do that is to transform each AND and NOT gate in the circuit into a equivalent mini-branching programs. The mini-programs are then composed to create the final representation such that, for any input <em>x</em>, <em>C</em>(<em>x</em>) = 1 iff <em>BP</em>(<em>x</em>) = <em>D<sub>1</sub></em> (and likewise for 0). Since the resulting <em>BP</em> has length O(4<sup><em>d</em></sup>), the conversion promises polynomial length programs only for log-depth circuits, which is the complexity class <strong>NC</strong><sup>1</sup>. To extend it to polynomial depth circuits we use fully homomorphic encryption.</p>
<p>The decision to use branching programs lies in their ability to preserve their structure even after inserting randomness into the procedure, as we shall see when we go over the modified version of Kilian's protocol.</p>
