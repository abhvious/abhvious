---
layout: post
status: publish
published: true
title: Notes on SSL
author: Amir
author_login: ameer
author_email: am8zv@virginia.edu
wordpress_id: 138
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=138
date: '2014-02-13 19:33:20 -0500'
date_gmt: '2014-02-13 19:33:20 -0500'
tags: []
categories: 14s-pet
comments: []
---
<p><a title="SSL" href="http://www.cs.virginia.edu/~am8zv/resources/SSL.pdf" target="_blank">Here is a link</a> to an overview of the SSL handshake I compiled around a year ago for both RSA and the Diffie-Hellman key exchange protocols (and for various versions). There might be some slight differences for TLS, but the general idea remains the same. I hope this helps with anyone who is working on a related project or just interested in better understanding SSL/TLS.</p>
<p>If you find any errors or segments that need more clarification, please inform me so that I can update it.</p>
