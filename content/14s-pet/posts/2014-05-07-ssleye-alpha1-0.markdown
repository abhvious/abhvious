---
layout: post
status: publish
published: true
title: SSLeye alpha1.0
author: AbiusX
author_login: abbas
author_email: an2wm@virginia.edu
author_url: https://abiusx.com
wordpress_id: 295
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=295
date: '2014-05-07 20:56:05 -0400'
date_gmt: '2014-05-07 20:56:05 -0400'
tags: []
categories: 14s-pet
comments: []
---
<p>Hello!</p>
<p>Today I'm going to release SSLeye alpha1.0 to you guys, along with the source code (cheers!). It works nicely, and you'll be amazed to see what's going on in your network :D There are some downsides though:</p>
<ol>
<li> Alpha1.0 is only released for Mac OS X (in binary at least)</li>
<li>The daemon tries to find your active network interface, and latch to it. If it fails, it tells you (doesn't let you change it yet).</li>
<li>There is a bug in the daemon (caused because of thread-safety in OpenSSL RSA functions) that causes it to output some data out of order. This will result in some inaccurate data in the application, as well as halting of the application (if the daemon messes the end of packet token).</li>
</ol>
<p>Everything else is good! The alpha version doesn't send any usage information back to the server, that will be available in the beta version. So please use this, provide feedback and if you feel like it, send me your inspected certificate database!</p>
<p>I will port this to Linux, and possibly to Windows and fix the daemon bug for alpha2 version, and add anonymous feedback plus interface selection for beta1, but source codes of beta versions won't be released to the public.</p>
<p>Have fun using it!</p>
<p><a href="https://abiusx.com/archive/ssleye/ssleye.zip">https://abiusx.com/archive/ssleye/ssleye.zip</a> (GUI code)</p>
<p><a href="https://abiusx.com/archive/ssleye/ssleye-daemon.zip">https://abiusx.com/archive/ssleye/ssleye-daemon.zip</a> (daemon code)</p>
<p><a href="https://abiusx.com/archive/ssleye/ssleye.app.zip">https://abiusx.com/archive/ssleye/ssleye.app.zip</a> (Mac OS X application)</p>
<p>&nbsp;</p>
