---
layout: post
status: publish
published: true
title: Initial Experimental Results
author: Amir
author_login: ameer
author_email: am8zv@virginia.edu
wordpress_id: 287
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=287
date: '2014-04-01 06:26:13 -0400'
date_gmt: '2014-04-01 06:26:13 -0400'
tags: []
categories: 14s-pet
comments: []
---
<p>I ran some experiments using the completed GGH encoding scheme to compare the performance of normal "vanilla" Killian protocol evaluation of branching programs against randomized and encoded (i.e. obfuscated) branching programs. Due to the overhead of the added safeguards and the encoding process, it would not be surprising to see that obfuscated programs would be slower to generate and evaluate. However, the purpose of this initial experiment is to determine the extent of performance degradation.</p>
<p>Using a relatively small dimension parameter (~2<sup>4</sup>) and some large prime of size O(2<sup>22</sup>), the experiment was run over several random branching programs of length in the range [4,16] (averaged over multiple runs), which corresponds to very low-depth (at most 2-input) circuits. Based on this small sample size, the generation of the obfuscated program was slower than normal program generation by a factor of almost 90. The verification of the obfuscated program was slower than normal program verification (evaluation) by a factor of almost 280. Nevertheless, the actual time that obfuscation took to generate the encoded randomized branching program is significantly higher than verifying the encoded program (it is about 10 times as much slower).</p>
<p>In all cases, the time for generation/verification exhibited a linear relationship with the length of the branching program. Further testing would include investigating how the dimension parameter affects the time it takes to encode and running the encoding process on larger programs to see how time is related to the <em>size</em> of the circuit (not the length of the branching program). Also, the strength of the added safeguards (e.g. length of the padding for the permutation matrices) can be tweaked to determine how much of an effect it has on the performance. Though, it would seem at first glance that increasing the padding results in an enormous slowdown that may not justify the miniscule increase in security.</p>
