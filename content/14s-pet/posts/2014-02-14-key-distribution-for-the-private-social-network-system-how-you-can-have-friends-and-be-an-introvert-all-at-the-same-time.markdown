---
layout: post
status: publish
published: true
title: Key Distribution for the Private Social Network System - How you can have friends
  and be an introvert, all at the same time.
author: tommy
author_login: tommy
author_email: tjt7a@virginia.edu
wordpress_id: 155
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=155
date: '2014-02-14 20:34:32 -0500'
date_gmt: '2014-02-14 20:34:32 -0500'
tags: []
categories: 14s-pet
comments: []
---
<p>By: Tom and Nathan</p>
<p>In this post, Nate and I will be discussing the Key Distribution Problem with the Private Social Network System. One of the most difficult and error-prone components of any cryptographic system is the Key Distribution method and avoiding any possible MITM attacks</p>
<p>By definition, Alice is friends with Bob iff Alice and Bob both have decryption keys for each other.</p>
<p>How do Alice and Bob become friends in the first place?<br />
If Alice is friends with Bob, and Bob is friends with Carole, how do Alice and Carole become friends?</p>
<p>We propose two methods for establishing friendships (sharing keys between parties).<br />
1. Direct Friendship - This method requires some previously-established secure communication. This includes in-person contact, or PGP high-trust communication. This is meant to be the most secure and highly-trusted method of establishing friendship.</p>
<p>2. Indirect Friendship utilizing the Web of Trust - This method leverages previously-established friendships for forming new relations. Friendships may only be established by this mechanism if users have a friend in common (i.e. Alice may only become friends with Carole if both Alice and Carole share some friend Bob).</p>
<p>In the following discussion, Alice will be seeking friendship with Carole.</p>
<p>Utilizing the Direct Friendship protocol: Alice sends Carole a friend request through the pre-established secure channel. Should Carole accept, she will assign Alice a list of attributes (as per attribute-based encryption), generate Alice's decryption key accordingly, and then communicate this key to Alice via the same channel. Alice will do likewise for Carole. This can be done in person or over PGP.</p>
<p>Utilizing the Indirect Friendship protocol: Here, we say Bob is a mutual friend of Alice and Carole. The general idea of this protocol is to establish a secure connection between Alice and Carole for the Direct protocol, with the identity of Carole being verified by Bob.</p>
<p>(a) To begin, Alice will broadcast a message to all of her friends saying: "Does anyone know Carole?". Alice will also include in this message some [temporary] public key for all subsequent replies.<br />
(b) Bob, being Alice's friend, will read this message and, knowing Carole, will act as follows. Bob first generates a random number to act as a shared secret.<br />
(c) Bob securely sends a message to Carole informing her that Alice wishes to be her friend. He includes the public key provided by Alice and this shared secret. Because Carole and Bob are already friends, there exists a pair of decryption keys between the two for secure communication.<br />
(d) Bob sends a message to Alice containing the shared secret using her public key she provided. (Public Key Encryption)<br />
(e) Carole confirms the friendship request by also sending the secret to Alice encrypted using Alice's public key (provided by Bob), and a symmetric key for subsequent communication.<br />
(f) With this symmetric key, Alice and Carole now have a secure channel, verified by Bob, so that they may use the Direct Friendship protocol to establish a decryption key pair.</p>
<p>Possible extensions:<br />
1. Can we adapt this protocol to work for friendship Chains? In other words, can Alice friend Doug, if Alice is Friends with Bob, Bob is friends with Carole, and Carole is friends with Doug, in a way which prevents a malicious Carole from passing off NSA as Doug. (Web of Trust?)<br />
2. What if Alice does not completely trust Bob? How can Alice set a threshold number of friends who verify Carole's identity?<br />
3. Can we combine both questions in such a way that the system is still not vulnerable to Sybil attacks?</p>
