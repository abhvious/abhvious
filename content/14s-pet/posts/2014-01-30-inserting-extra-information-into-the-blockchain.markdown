---
layout: post
status: publish
published: true
title: Inserting extra information into the BlockChain
author: nick
author_login: nick
author_email: nskelsey@gmail.com
wordpress_id: 74
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=74
date: '2014-01-30 23:19:30 -0500'
date_gmt: '2014-01-30 23:19:30 -0500'
tags: []
categories: 14s-pet
comments:
- id: 10
  author: jack
  author_email: jhd3pa@virginia.edu
  author_url: ''
  date: '2014-02-01 06:02:12 -0500'
  date_gmt: '2014-02-01 06:02:12 -0500'
  content: "Perhaps a dual system, separating verification from data storage, would
    be more practical than the singular 'bitcoin chain + data' concept? For instance,
    you could build the system to operate something like bittorrent, but use a bitcoin-chain-style
    system for verification. The chain exists in its entirety on all systems, but
    any given data may or may not. The algorithm enforces distribution and redundancy
    of data across all member nodes, but doesn't require that any particular member
    node host any particular piece of data. This would go a long way toward alleviating
    the storage/bandwidth concerns, and should remain reasonably safe since the chain
    could be used to validate every piece of data as it changes hands, in order to
    avoid any sort of poisoning.\r\n\r\nOf course, it seems to me that it would be
    necessary in such a system to make it impossible to control (or perhaps even discover)
    exactly what data resides on one's own machine. Or, to put it a different way,
    I think the data distribution ought to happen at a very low level, such that the
    larger system must be consulted before any particular piece of data can be retrieved.
    It would almost be similar to a sort of massively-distributed, redundant, cryptographically-verifiable
    log file system. If that makes any sense.\r\n\r\nAnyway, just a thought."
- id: 15
  author: jack
  author_email: jhd3pa@virginia.edu
  author_url: ''
  date: '2014-02-04 16:06:00 -0500'
  date_gmt: '2014-02-04 16:06:00 -0500'
  content: 'This morning I read an interesting article on the implementation details
    of bitcoin, from a fellow who executed a bitcoin transaction manually. Perhaps
    it isn''t relevant (especially since you seem to know a lot more than me about
    how this all works), but just in case it comes in handy, here it is: http://www.righto.com/2014/02/bitcoins-hard-way-using-raw-bitcoin.html'
- id: 16
  author: nick
  author_email: nskelsey@gmail.com
  author_url: ''
  date: '2014-02-05 04:25:39 -0500'
  date_gmt: '2014-02-05 04:25:39 -0500'
  content: "So the link you posted below was extremely helpful in my understanding
    of bitcoin. The author  described the block chain as \"entries in a distributed
    database that keeps track of ownership of bitcoins.\"\r\n\r\nMy new cursory understanding
    of bitcoin leads me to believe that the point you made in this reply is the correct
    approach. The protocol does not seem to support erroneous data insertion at the
    transaction level, but I think you could still use some of the principles the
    protocol relies on to create a distributed database of other things."
- id: 31
  author: jack
  author_email: jhd3pa@virginia.edu
  author_url: ''
  date: '2014-02-11 19:59:02 -0500'
  date_gmt: '2014-02-11 19:59:02 -0500'
  content: It still may be that you run into a bit of a storage issue. While separating
    storage from verification will certainly ameliorate the problem significantly,
    it seems to me that if it becomes a mature system, you will end up with either
    the problem that old data will drop off the network once it is no longer wanted
    by anyone, or, if your design prevents that, the problem that the global database
    size increases with use while the user pool (and therefore the available storage
    space) remains relatively constant, or at least becomes bounded by the growth
    rate of storage technology.
---
<p>I am interested in creating a public document notary service ontop of bitcoin. Such services already exist [1], they however keep the document itself secret. The current protocol depends on the guarantee that transactions created earlier in the block chain occurred early in time. This is a fundamental notion for a public currency but it could also be used for other things. The concept that a document or piece of information could be verifiably unchanged since its creation is compelling to me. Since there is no need for a trusted third party in a distributed network, anybody could anonymously leak information or documents into the public domain with confidence that the information will be unaltered and available to the world.</p>
<p>In essence, if you could bootstrap a service like this ontop of a functioning crypto currency you could create an online public library whose contents undeniably existed as long as the network lives.</p>
<p>Obviously the bitcoin protocol may not be the best peer-to-peer network to use for this service. I intend to research other existing currencies, understand there methods of block creation and look into where extra information could be added into these chains. The problem as I see it now has to do with the amount of information that you may want to push into this public record. The entire bitcoin block chain is roughly ~10Gb, whereas an incriminating power-point may be 20Mb in size. Since every participating node must have a access to entire chain, the size of the chain would increase dramatically.</p>
<p>I highly doubt that I will be able to modify an existing crypto currency to support the functionality I am looking for, but it is not out of the question. Any obvious comments?</p>
<p>[1] <a href="http://www.proofofexistence.com/about">http://www.proofofexistence.com/about</a></p>
