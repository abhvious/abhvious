---
layout: post
status: publish
published: true
title: Time lock crypto references
author: abhi
author_login: abhi
author_email: abhi@virginia.edu
wordpress_id: 65
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=65
date: '2014-01-30 14:59:48 -0500'
date_gmt: '2014-01-30 14:59:48 -0500'
tags: []
categories: 14s-pet
comments: []
---
<p>In last class, we briefly discussed time-lock crypto as an aside.  Here is reference to the original paper: <a href="http://www.hashcash.org/papers/time-lock.pdf">time locked puzzles and time-release crypto</a>.</p>
<p>As we discussed in class, to time-lock a mesasge M for T seconds, the basic scheme is to pick an RSA modulus, n=pq, pick a random symmetric encryption key K, encrypt M with K, and then produce a "time-lock puzzle" for the key K.  The puzzle is simply C = K + a^{2^t} where t is the number of modular square operations that one can perform in T seconds.  See the paper for the details, and for another scheme in that paper that describes another way to time-lock using threshold secret sharing.</p>
