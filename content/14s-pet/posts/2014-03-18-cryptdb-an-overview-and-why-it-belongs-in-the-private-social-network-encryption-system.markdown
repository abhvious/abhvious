---
layout: post
status: publish
published: true
title: 'CryptDB: An Overview and Why it Belongs in the Private Social Network Encryption
  System'
author: tommy
author_login: tommy
author_email: tjt7a@virginia.edu
wordpress_id: 237
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=237
date: '2014-03-18 04:21:44 -0400'
date_gmt: '2014-03-18 04:21:44 -0400'
tags: []
categories: 14s-pet
comments: []
---
<p>By: Tommy Tracy II and Nathan</p>
<p>Source: <a title="http://people.csail.mit.edu/nickolai/papers/raluca-cryptdb.pdf" href="http://people.csail.mit.edu/nickolai/papers/raluca-cryptdb.pdf" target="_blank">http://people.csail.mit.edu/nickolai/papers/raluca-cryptdb.pdf</a></p>
<p>One of the most important components of the Private Social Network System is the key storage component. There are attribute-based encryption keys, attribute-based decryption keys, and management symmetric keys. The applications that will decrypt messages will only need access to decryption keys, the applications that will encrypt messages will only need access to the encryption keys, and finally, the management application is the only entity that may access the symmetric keys. In the case of an intrusion, it is critical that the symmetric keys remain secured. Once these keys are lost, the user completely loses control of his/her social network. It is, therefore, critical to secure these keys to the highest level possible.</p>
<p>We will be using CryptDB as the key storage mechanism. It provides a high level of confidentiality in the case of an attack and sections the database's data into regions that can only be accessed by a 'Principal' with the correct permissions. This will allow us to partition the decryption, encryption, and symmetric keys into cryptographically isolated regions.</p>
<p>CryptDB maintains its high level of confidentiality by using three different techniques.</p>
<ol>
<li>It uses SQL-Aware Encryption: All of the data, in this case the keys, are encrypted such that the  DataBase Management System (DBMS) can still execute searches on it using the CryptDB proxy, but in such a way that the DBMS does not have any knowledge of the data, itself.</li>
<li>The encryption system uses adjustable query-based encryption: It uses 'onions of encryption' to store multiple cipher texts within each other. This prevents leaking extra information, and avoids expensive re-encryptions.</li>
<li>It chains encryption keys to user passwords: This means that only Principals with the correct password can decrypt data items associated with that Principal, or groups of principals.</li>
</ol>
<p><strong>On to some of the programmer details. </strong></p>
<p>CryptDB is implemented for MySQL and Postgres and plugs into an existing unmodified DataBase Management System (DBMS). The CryptDB proxy consists of a C++ Library and a LUA module that passes queries/results to and from the C++ module.</p>
<p>And of course, one of the biggest questions any good programmer should ask: Has anyone already bitten the bullet and tried this? Yep; they have. Google, Lincoln Labs, and sql.mit.edu are currently using CryptDB to varying degrees. (<a title="Source" href="http://css.csail.mit.edu/cryptdb/" target="_blank">http://css.csail.mit.edu/cryptdb/</a>)</p>
<p>The code is accessible from the following link: <a title="http://css.csail.mit.edu/cryptdb/" href="http://css.csail.mit.edu/cryptdb/" target="_blank">http://css.csail.mit.edu/cryptdb/</a></p>
<p>&nbsp;</p>
