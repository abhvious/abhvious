---
layout: post
status: publish
published: true
title: Using HElib to extend homomorphic encryption
author: divy
author_login: divy
author_email: ds5nu@virginia.edu
wordpress_id: 99
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=99
date: '2014-02-06 20:07:34 -0500'
date_gmt: '2014-02-06 20:07:34 -0500'
tags: []
categories: 14s-pet
comments:
- id: 25
  author: abhi
  author_email: abhi@virginia.edu
  author_url: ''
  date: '2014-02-11 18:13:42 -0500'
  date_gmt: '2014-02-11 18:13:42 -0500'
  content: Divy, this is a great project and I have a lot of follow-up links for you.
---
<p>I recently came across HElib, a library released by IBM that implements homomorphic encryption (HE). The idea behind homomorphic encryption is to find ways to perform calculations on encrypted data (as of right now things like addition, multiplication, etc.) such that you get the same result you would get from decrypting the data and performing the operation. This could provide significantly better security for many transactions if it becomes applicable to more operations. HElib only implements a few simple operations as of right now, but perhaps it could be extended using existing HE theories? I am still reading and researching on the topic, but any suggestions or thoughts are welcome!</p>
<p>HElib: https://github.com/shaih/HElib</p>
<p>Also, a thank you to Amir for sharing his knowledge and sending me papers on the subject!</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
