---
layout: post
status: publish
published: true
title: Program Obfuscation in Practice
author: Amir
author_login: ameer
author_email: am8zv@virginia.edu
wordpress_id: 44
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=44
date: '2014-01-28 15:24:57 -0500'
date_gmt: '2014-01-28 15:24:57 -0500'
tags: []
categories: 14s-pet
comments: []
---
<h4>Introduction</h4>
<p>This project focuses on constructing a practical implementation of program obfuscation. Simply put, the goal of obfuscation is to transform a given program <em>P</em> that implements some function <em>f</em> into another program <em>P'</em> that implements the same function <em>f</em>  with the condition that the code of <em>P'</em> should be "incoherent". So, a malicious user with access to the code of <em>P'</em> would have difficulty understanding what the code is or how it works. There are various applications of obfuscation in cryptography and other areas of security (see Applications section below).</p>
<p>The strongest notion known used to describe the above goal is called "virtual black box" (VBB) obfuscation first defined in [1], which states that an adversary having access to the code of <em>P' </em> can learn nothing more than what can be deduced from having only black-box access to <em>P</em><em></em>. Of course, if <em>P</em> leaks some or all information about the code in its function output, then no obfuscator exists for <em>P</em> since an adversary can just look at the function output. For example, no obfuscator exists for a program that prints out its own code because the code will be revealed anyway in the function output. These programs are known as learnable functions and are of little interest to us in this case. What we like to obfuscate are un-learnable functions. Unfortunately, it was shown in [1] that VBB obfuscation is impossible in general: there exists a family of un-learnable functions such that there is no way to obfuscate any function in this class.</p>
<p>To counter the previous negative result, a weaker, perhaps more realizable, notion of obfuscation was suggested as an alternative on which to base obfuscators: <em>indistinguishability obfuscation</em> (iO). This basically means that given obfuscations of two programs <em>P'</em><sub>1</sub>, <em>P'</em><sub>2</sub> from the same class of functions, a computationally bounded adversary cannot distinguish between the two obfuscations with high probability. Unlike VBB obfuscation, iO has been shown to be useful in building obfuscators for all polynomial-size circuits. In fact, [2] shows one such candidate construction of an indistinguishability obfuscator that uses multilinear jigsaw maps as the underlying hardness assumption. The aim of this project is to implement this system as well as perhaps improving the practical efficiency for some applications.</p>
<h4>Some Useful Applications of Obfuscation:</h4>
<ol>
<li>Functional Encryption: Given some message <em>x</em> and an encryption of that message Enc(<em>x</em>), one can "decrypt" Enc(<em>x</em>) using some secret key <em>SK</em><sub>f</sub> to get <em>f</em>(<em>x</em>). No other information about <em>x</em> must be deduced by getting <em>f</em>(<em>x</em>).</li>
<li>Fully Homomorphic Encryption: A fully homomorphic encryption scheme is a public-key encryption scheme with the added ability of computing encryptions of functions of messages. So, in addition to the (Gen, Enc, Dec) algorithms, we also have an evaluation algorithm Eval that takes as input a public evaluation key and encryptions Enc(<em>x</em><sub>1</sub>),...,Enc(<em>x</em><sub>n</sub>) and outputs Enc(<em>f</em><em></em>(<em>x</em><sub>1</sub>,...,<em>x</em><sub>n</sub>)) for some function <em>f.</em></li>
<li>Software Protection: Developers that would like to publish their software but protect their intellectual property can obfuscate their program to impede any attempts at reverse-engineering the software.</li>
<li>Restricted-Use Software: Developers may wish to distribute their software with varying access controls (e.g. demo, commercial license). However, if they publish the software as is, access controls may be overridden by a malicious user that knows how to modify the code to get around the restrictions. To prevent such workarounds, developers can obfuscate their code after applying the access control mechanisms, and distribute that instead.</li>
</ol>
<h4>Project Goals (Tentative):</h4>
<ol>
<li>Implement the candidate construction described in [2] to achieve indistinguishability obfuscation for specific classes of functions</li>
<li>Attempt to enhance the construction for practical applications</li>
</ol>
<p><em>If you are interested in knowing more about this subject or like to work on this project, please don't hesitate to contact me.</em></p>
<h4>References:</h4>
<p>[1]   B. Barak, O. Goldreich, R. Impagliazzo, S. Rudich, A. Sahai, S. Vadhan, K. Yang,"On the (Im)possibility of Obfuscating Programs", Crypto 2001<br />
[2]  S. Garg, C. Gentry, S. Halevi, M. Raykova, A. Sahai, B. Waters, "Candidate Indistinguishability Obfuscation and Functional Encryption for all Circuits", FOCS 2013</p>
