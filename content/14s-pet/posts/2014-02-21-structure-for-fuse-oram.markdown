---
layout: post
status: publish
published: true
title: Structure for Fuse ORAM
author: saba
author_login: saba
author_email: se8dq@virginia.edu
wordpress_id: 179
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=179
date: '2014-02-21 06:25:21 -0500'
date_gmt: '2014-02-21 06:25:21 -0500'
tags: []
categories: 14s-pet
comments: []
---
<p>I spent this week setting up the structure on which I will write (or at least write much of) my ORAM implementation next week.  We want to be able to connect to our encrypted and ORAM'd remote cloud storage.  In short, I set up a short (few lines) script that glues together some existing fuse filesystems and a skeleton filesystem that I will soon expand into an ORAM interface.  I had a picture that I drew out on a whiteboard that explains it visually, but I seem to be unable to add it.</p>
<p>I have set up three separate fuse filesystems intentionally so they can be swapped out as desired/necessary to allow for different cloud providers to be used as desired.  Currently, the first layer filesystem is just for connecting to the cloud provider (this is the part that I imagine could be changed by a user).  I went back to the google-drive-ocamlfuse filesystem since copy-fuse started having inexplicable problems in the middle of the week whereas google-drive-ocamlfuse appears to be more robust.  The second layer uses EncFS to encrypt/decrypt the data stored on the cloud. The third layer will be my ORAM that shuffles/unshuffles the files held in the storage to prevent any information being gleaned from data access patterns. One goal that I'm trying to adhere to is to make sure that this can be used by others easily when it's done, and I think this approach facilitates that well.</p>
<p>For next week, I'll begin (and hopefully finish) implementing the ORAM that we discussed in class on Tuesday.</p>
<p>PS.  Since we mentioned the possibility of using ORAM on Flickr's impressive 1TB of free storage, I thought I'd share this fuse filesystem that someone's set up to transfer your files to flickr as .png images: https://github.com/Rotten194/flickr-fuse.  It comes with a bit of a disclaimer ("Flickr-fs is a fuse filesystem that allows you to upload your files to flickr as encoded <code>.png</code>s. It's also really slow and liable to explode at any moment, so don't seriously use it."), but may be promising.</p>
