---
layout: post
status: publish
published: true
title: 'AWSOMnet: crypto-currency incentivized mesh network'
author: tpd5bf
author_login: tpd5bf
author_email: tpd5bf@virginia.edu
wordpress_id: 68
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=68
date: '2014-01-30 19:28:22 -0500'
date_gmt: '2014-01-30 19:28:22 -0500'
tags: []
categories: 14s-pet
comments:
- id: 4
  author: abbas
  author_email: an2wm@virginia.edu
  author_url: https://abiusx.com
  date: '2014-01-31 15:23:04 -0500'
  date_gmt: '2014-01-31 15:23:04 -0500'
  content: "That is an awsom idea :D I'd love to collaborate with you on implementing
    that, as I bet there are countless low level network and systems stuff that this
    will be needing.\r\n\r\nThere are a bunch of high-level issues though, that I'm
    going to mention here:\r\n\r\nAnyone providing Internet access to the public,
    has an obligation to log all accesses and usages (at least in US, in my country
    nobody cared), for law enforcement reasons.\r\nThats why Airports and Hotels require
    your information (room number, CC) and charge you a lot, and thats exactly why
    others don't share network for free (or any hacker would go to a coffee shop and
    hack the world without worry).\r\n\r\nSecond issue is that you need to define
    a UNIT OF DATA, which obviously can not be a bit, to charge for. Just like you
    charge for a pound of rice, and not a grain of rice, because counting those grains
    will cost more than the entire thing itself. Then we need some sort of scale (we
    might have to actually go physical for this) that triggers on each pound of traffic
    (with an interrupt or something), and then there would be the issue of fractions
    of a pound usage.\r\nThere are possible solutions for both issues, but they need
    to be well studied. Please get back to me at me@abiusx.com to further discuss
    this."
- id: 8
  author: jack
  author_email: jhd3pa@virginia.edu
  author_url: ''
  date: '2014-02-01 03:17:21 -0500'
  date_gmt: '2014-02-01 03:17:21 -0500'
  content: As far as units of data go, what if you sold bandwidth instead of bits?
    I'm not sure about you guys, but I pay monthy for a certain amount of bandwidth,
    with the total data transmission theoretically uncapped. Not only that, but I'd
    be much more likely to pay for bandwidth (or maybe bandwidth*hours) than bits.
- id: 11
  author: tpd5bf
  author_email: tpd5bf@virginia.edu
  author_url: ''
  date: '2014-02-02 01:47:38 -0500'
  date_gmt: '2014-02-02 01:47:38 -0500'
  content: "Hi Abbas &amp; Jack, thanks for the feedback &amp; replies.\r\n\r\nThe
    issue of not wanting to be responsible for outgoing internet traffic is a real
    one; the goal of this system would be that it's global and free / open source,
    so people would be using it under various legal environments.  I imagine even
    the ToS from most ISPs would be against this kind of thing.  On the other hand
    I share internet with the people who live upstairs and don't worry too much about
    it.  I think having exit nodes could mostly solve the problem.  Lets say I want
    to gain awsom-points but nobody is around me, so I set up a high bandwidth exit
    node, where routers set up to share their internet to phones don't give phones
    direct internet access through their NAT, but transparently tunnel everything
    to my exit node.  The router owner pays the exit node a fee smaller than that
    which he charges the phone users keeps the difference.  ISPs would just see a
    high usage SSH tunnel to somewhere.  Exit nodes take some risk, but TOR exit nodes
    already do this and seem to survive, and they do it for free.  Hackers might use
    it, but they do in fact use coffee shops and such today -- the Silk Road's DPR
    was apparently captured while in a public library, administering the site from
    his laptop.\r\n\r\nI agree that it goes against our experience but I think the
    unit of data measured would just be the byte.  Duplex issues become tricky --
    should inbound and outbound traffic both contribute?  But probably yes.  The idea
    is that the protocol itself says nothing about pricing, and can span many orders
    of magnitude.  Right now I've got a 2-byte \"price\" field.  ten bits are the
    fraction, 5 bits for exponent, and one for ... not sure the word, but I've called
    it reciprocal.  If that bit is 0, it means bytes per awsom-point, and if it's
    1, it means points per byte.  fraction gives you 0-1023, exponent (base 10) is
    10^0 to 10^32. That what with a 16 bit field you can go from ~ 10^-35 points per
    byte to ~10^35 points per byte, with better than 1% granularity through the 70
    order of magnitude range.  Billing would occur at the quantum defined in this
    price field.  Thus a price field of 0, 0x09, 0x08, would translate to 8 awsom
    points per gigabyte, and billing transactions would only occur once every 1GB
    transferred.  There's overlap so if you want better granularity at the cost of
    overhead, you can charge 800 points per 10MB.\r\n\r\nThe idea is that it would
    be really, really cheap. It would have to be cheaper than current internet, because
    otherwise people wouldn't use it.  \r\n\r\nCompetition would arise if, for example,
    you move into an apartment, and two people on your floor are awsomnet hosts.  One
    charges 15 points per megabyte, the other 10 points per meg.  Obviously you go
    with the 10 point host.  The person running the 15 point host though can see the
    10 point one (nothing sneaky, he can just use his phone to see-- or hosts could
    auto-scan for competition) and may choose to lower his prices to get your usage.
    \ In competitive environments, the costs could become very low as most people's
    cable modem is idle 99% of the time.  Other methods like guaranteed bandwidth
    for periods of time are, I agree, desirable, but involve trust between parties
    that I'm trying to avoid in the design of the system.  The buyer pays first, but
    there's no way (I think) to know that you'll actually get internet when you pay.
    \ An attacking host router could easily be set up to offer internet at an attractive
    price, receive payment, then be a jerk and not give internet access.  Since this
    is unavoidable, the goal would be granularity on a scale such that phones can
    optimistically / automatically connect, pay, attempt to get to the net, and if
    they fail, suspend payment, locally blacklist the host node, and try somewhere
    else.\r\n\r\nLonger term things like assured bandwidth / web of trust type things
    could be built on top, but I think starting with just bytes and points is simplest
    and would work in many situations.  (Oh also the units of bandwidth * hours is
    bytes.)\r\n\r\nThere's a bunch of other issues / problems that get messy but I
    think it could work with some non-zero probability.  Can't post tables here but
    I have lots of notes / sketches / snippets of code that don't connect / etc that
    I can bring in."
- id: 14
  author: abbas
  author_email: an2wm@virginia.edu
  author_url: https://abiusx.com
  date: '2014-02-02 02:16:52 -0500'
  date_gmt: '2014-02-02 02:16:52 -0500'
  content: "I also believe that bandwidth is not suitable, though if it was, it'd
    be an awesome solution to the stated problem, because bandwidth can be fake, and
    is not guaranteed to be provided.\r\nplease enlist your problems and lets have
    a discussion on this topic, not on next class session though cuz I wont be there."
---
<p>Most people don't have access to the internet, but they should.  Internet infrastructure, while very capable, relies on centralized systems and tends towards natural monopolies and stagnation. (Ever been to the Comcast office up on rt 29?)  While mesh networks have seen use in research and academic settings, there is little to no consumer use of wifi mesh networks.  In practice, people secure their personal wifi routers with a pre-shared key, and through the key limit usage of their network to only themselves and friends.  There is no motivation, and many reasons against having an open wifi access point.</p>
<p>If people had an incentive to share their wifi, however, this could open up possibilities for mesh networking and much more effective use of bandwidth.  While there are systems like wifi captive portals to charge for wifi in airports, for example, those only work with credit cards and are very slow / awkward / expensive.  A captive portal using something like bitcoin would be an improvement.  A fully automated protocol where your phone silently pays cryptographic points in exchange for internet access on a per-byte basis would be ideal.</p>
<p>The initial proof of concept for AWSOMnet (awsom wireless stateful open mesh) would have a distributed, cryptographically signed point transfer system, as well as virtual ethernet interface IP tunneling.  Initially it could be limited to one phone or laptop connecting to one router and setting up a payment channel [<a href="https://en.bitcoin.it/wiki/Contracts#Example_7:_Rapidly-adjusted_.28micro.29payments_to_a_pre-determined_party">link</a>].  Further fun could be had with multi-hop networks.  For example, your phone connects to a router selling internet at 5 points per kilobyte.  It then re-broadcasts to other phones in range (but not within range of the router) and re-sells for 10 points per kilobyte.  At the costs of battery / cpu use, it can gain points for later usage.  Router owners, where points accumulate, then transfer their points to their phones, or phones of friends, or sell the points on markets.  Eventually hardware and range may be cheap enough that it could be profitable to purchase multi-hop nodes for the express purpose of building out the mesh, placing them on telephone poles or rooftops, and collecting the point fees.  This would be somewhat analogous to bitcoin mining today.</p>
<p>Issues include implementation of the transfer protocol, including maintaining an embedded device friendly signing system, limiting bandwidth overhead for they payment channels, incentivizing network sharing, and network security.  Paid for "exit nodes" could be used, similar to TOR, if router-owners did not want to be associated with outgoing traffic from users they are selling internet to.  An exit node would be any internet host which takes payment, along with an IP tunnel from the AWSOMnet, and allows I/O to the normal internet.  With only one link from router to exit node the source of traffic is not unknown, but since nodes do not have knowledge of network topology, sources of traffic are plausibly deniable. (I didn't connect to blackhat.com, someone using my AWSOMnet must have been!)</p>
<p>This is maybe a bit too large a project for this class, but I'd like to do at least part of it, and continue and complete other aspects later.</p>
