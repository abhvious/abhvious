---
layout: post
status: publish
published: true
title: 'Program Obfuscation: Progress Update 1'
author: Amir
author_login: ameer
author_email: am8zv@virginia.edu
wordpress_id: 106
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=106
date: '2014-02-07 04:40:45 -0500'
date_gmt: '2014-02-07 04:40:45 -0500'
tags: []
categories: 14s-pet
comments:
- id: 27
  author: abhi
  author_email: abhi@virginia.edu
  author_url: ''
  date: '2014-02-11 18:16:01 -0500'
  date_gmt: '2014-02-11 18:16:01 -0500'
  content: I am going to count on you to present the scheme at a later part of the
    course.  Can you dig into all of the details and get ready to present it in ~2
    weeks?
- id: 33
  author: Amir
  author_email: am8zv@virginia.edu
  author_url: ''
  date: '2014-02-11 23:52:14 -0500'
  date_gmt: '2014-02-11 23:52:14 -0500'
  content: Sure. The scheme borrows from and fuses different concepts, which at first
    may seem incomprehensible, but the end result is a somewhat conceivable approach
    to obfuscation.
- id: 36
  author: 'Program Obfuscation: Progress Update 2'
  author_email: ''
  author_url: https://crypto.cs.virginia.edu/courses/14s-pet/2014/02/16/program-obfuscation-progress-update-2/
  date: '2014-02-16 10:07:20 -0500'
  date_gmt: '2014-02-16 10:07:20 -0500'
  content: "[&#8230;] for the inputs to Kilian&#8217;s protocol. This corresponds
    to step 1 of the procedure described here. The theorem states that for any Boolean
    circuit C : {0,1}k -&gt; {0,1} with fan-in-2 gates and [&#8230;]"
- id: 39
  author: 'Program Obfuscation: Progress Update 3 (+Kilian&#8217;s Protocol)'
  author_email: ''
  author_url: https://crypto.cs.virginia.edu/courses/14s-pet/2014/02/26/program-obfuscation-progress-update-3/
  date: '2014-02-26 22:46:04 -0500'
  date_gmt: '2014-02-26 22:46:04 -0500'
  content: "[&#8230;] In this post, I&#8217;ll go over Kilian&#8217;s protocol in
    the context of secure computation and explain how to use it to create the inputs
    for the multi-linear encoding scheme. This corresponds to step 2 in the process
    described here. [&#8230;]"
---
<p>I started going through <a href="http://eprint.iacr.org/2013/451.pdf" target="_blank">this paper</a> more rigorously to understand how the construction works (and what assumptions are being used). Recall that we're trying to implement a system that is based on indistinguishability obfuscation: given two <em>functionally equivalent</em> circuits, their obfuscations should be indistinguishable to any probabilistic polynomial-time adversary (with high probability).<em></em></p>
<p>Let Alice be the obfuscator and Bob be the evaluator. The high-level idea of the construction starts with a depth-<em>d</em> circuit <em>C</em> that implements some function <em>f</em> : {0,1}<sup>s</sup> -&gt;{0,1} and goes through the following steps:</p>
<ol>
<li>Using <a href="http://people.cs.umass.edu/~barring/publications/bwbp.pdf" target="_blank">Barrington's theorem</a>, Alice transforms circuit <em>C</em> into a <em>branching program</em> <em>BP</em> of width 5 and a length at most 4<sup>d</sup> represented as a sequence of 5x5 permutation matrices.</li>
<li>Using an augmented version of <a href="http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.92.9265&amp;rep=rep1&amp;type=pdf" target="_blank">Kilian's protocol</a> (this is a secure multi-party computation protocol based on oblivious transfer),  Alice encapsulates <em>BP</em> with random matrices to create a randomized branching program <em>RBP.</em></li>
<li>Alice generates a <em>Multi-linear Jigsaw Puzzle</em>: She encodes each element of <em>RBP </em>such that each element represents a jigsaw puzzle piece that can be combined in restricted ways to create valid <a href="http://en.wikipedia.org/wiki/Multilinear_form">multi-linear forms</a>.</li>
<li>Once Bob receives this puzzle (i.e. obfuscated circuit) from Alice, it builds a multi-linear form out of the "puzzle pieces" using its input <em>x</em>. The evaluation of the multi-linear form reveals the evaluation of <em>C</em> on <em>x</em>. Security is based on the hardness of distinguishing between two puzzles.</li>
</ol>
<p>Each step is quite involved, so in the upcoming weeks, I'll give more detailed explanations of each step (including the intuitions behind each decision) as I implement it. It's also worth noting that while the current construction does run in polynomial-time, the authors express concern about its efficiency when used to solve more practical problems. I'll try to see if I can make some modifications to the system (without breaking security) to enhance its performance for specific classes of circuits.</p>
