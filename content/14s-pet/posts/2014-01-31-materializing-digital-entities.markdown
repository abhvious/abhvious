---
layout: post
status: publish
published: true
title: Materializing Digital Entities
author: AbiusX
author_login: abbas
author_email: an2wm@virginia.edu
author_url: https://abiusx.com
wordpress_id: 79
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=79
date: '2014-01-31 16:13:05 -0500'
date_gmt: '2014-01-31 16:13:05 -0500'
tags: []
categories: 14s-pet
comments:
- id: 6
  author: Amir
  author_email: am8zv@virginia.edu
  author_url: ''
  date: '2014-01-31 17:22:09 -0500'
  date_gmt: '2014-01-31 17:22:09 -0500'
  content: That's an interesting problem! By originality of an entity, do you mean
    exclusive entitlement of this object (no one can use it but me) or uniqueness
    (no one can have exactly that item except me)? If it's the former, shouldn't a
    digitally signed certificate of ownership/authenticity suffice, or doe this violate
    the third condition?
- id: 7
  author: jack
  author_email: jhd3pa@virginia.edu
  author_url: ''
  date: '2014-02-01 03:12:57 -0500'
  date_gmt: '2014-02-01 03:12:57 -0500'
  content: "Following up on Amir's two cases, the first seems to already be covered
    to a limited extent by encryption (that is, if you have the key, you can use the
    object), though, of course, without the key it is also impossible to determine
    what the object *is* (perhaps this is an application of Abhi's format-preserving-encryption
    idea?)\r\n\r\nThe second case seems more interesting, but also more challenging
    (and also slightly terrifying, given how interested the entertainment industries
    would likely be in such a technology). I have a difficult time conceiving of a
    way of preserving originality during low level (bit-for-bit) copies without involving
    a notary server of some kind. On the other hand, finding a way to ease the notary
    server requirement certainly would be revolutionary."
- id: 13
  author: abbas
  author_email: an2wm@virginia.edu
  author_url: https://abiusx.com
  date: '2014-02-02 02:13:24 -0500'
  date_gmt: '2014-02-02 02:13:24 -0500'
  content: "Well you state valid points, but simply put, I want to be able to treat
    data like materials, not like digital entities. Simply put, if you duplicate a
    hard disk having this \"entity\" on it, you would not be having the same entity,
    or a copy of it, or etc.\r\nIt's not clear in my head how to and what limitations
    should be there, but it is clear that it will have countless usages."
- id: 17
  author: nick
  author_email: nskelsey@gmail.com
  author_url: ''
  date: '2014-02-05 04:37:08 -0500'
  date_gmt: '2014-02-05 04:37:08 -0500'
  content: "How can you vouch for the uniqueness of readable bits on a hard drive?
    I think if your notion of entity stops at the level of just readable + writeable
    data then you are stuck.\r\n\r\nHeres an idea. what if this entity, say a hard
    disk with a PRNG whose initialization values were unknown to everyone, reported
    slightly different answers every time you read from it.\r\n\r\nThis would create
    an entity that was unique, tied to some piece of data, and also not that useful.
    If it signed everything with a secret key then you could have some confidence
    that you were interacting with the thing and not some imposter."
- id: 19
  author: abhi
  author_email: abhi@virginia.edu
  author_url: ''
  date: '2014-02-06 18:05:10 -0500'
  date_gmt: '2014-02-06 18:05:10 -0500'
  content: "Even if we semantically call one particular copy of the entity at a given
    time the \"true\" copy, wouldn't it be the case that other digital copies have
    the same *functionality* as the \"true copy\" ?\r\n\r\nIn your list of conditions,
    is it the case that the true copy needs to have some functional differentiation
    from all other copies?  \r\n\r\nIf not, then a trivial solution exists.  All you
    need is a signature chain from the person who created the entity.  Each signature
    is \"Owner X now gives entity E to its new owner Y on Feb 6, 2014 at 12p.\"  New
    owners just propogate this signature chain.  The \"real copy\" is the one that
    has a signature chain until the present."
---
<p>This is more of a long shot dream than a real applicable scenario, but has been boggling my mind forever;  thus I'd really appreciate any feedback and/or comments on it.</p>
<p>The idea is to materialize digital entities, i.e having only <strong>"one copy"</strong> of some digital stuff (like a movie, or a key or etc.). To get there, the following conditions need to be met:</p>
<ul>
<li>Any copy of the data should be a copy, and different from the original</li>
<li>When passed along, the originality should be passed along (in ideal case, the previous owner should no longer have it)</li>
<li>Should not rely on notary servers (applicable between two isolated parties)</li>
<li>Should be applicable to everything digital, not a specific type of it</li>
</ul>
<p>These are the ideal conditions though, and we can compromise on one or more. Making this happen would be the biggest revolution of software systems though, because it can transform software to physical entities, with all their features.</p>
<p>That's all.</p>
