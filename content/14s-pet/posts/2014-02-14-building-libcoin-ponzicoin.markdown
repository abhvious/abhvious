---
layout: post
status: publish
published: true
title: Building libcoin + ponzicoin
author: nick
author_login: nick
author_email: nskelsey@gmail.com
wordpress_id: 153
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=153
date: '2014-02-14 17:16:45 -0500'
date_gmt: '2014-02-14 17:16:45 -0500'
tags: []
categories: 14s-pet
comments: []
---
<p>So I figured I would try to see if I could get the examples provided in libcoin to work as a logical next step. This required building the entire project, which as I found out was no small task. The TL; DR is: I got libcoin to build, ponzicoin has some bug in it, but their daemon libcoind seems to work fine with the regular block chain.</p>
<p>To do this, I setup an ubuntu vm and started installing dependencies listed in their readme. Since there were no versions specified, I just went and used the versions that bitcoin was using for all of the packages they listed. This ended up biting me.</p>
<p>Before that though, I had to actually start building the project. While nominally still under development the main contributors to libcoin have all but walked away, resulting in a broken build in master. Most of the problems had to do with non-existant examples and an entire library (lol) called coin-reg.</p>
<p>Anywho after paving over all the obvious problems I ran into some real problems in the actual build. One of them was an overloading ambiguity on this <a href="https://github.com/libcoin/libcoin/blob/master/src/coinChain/Verifier.cpp#L51">line</a>:</p>
<p><code>_pending_verifications.push_back(boost::move(fut));</code></p>
<p>The problem had to do with C++ templates and me using boost 1.4.8 instead of 1.4.2. My solution:</p>
<p><code>//_pending_verifications.push_back(boost::move(fut));</code></p>
<p>Once the thing actually built, it created a whole bunch of fun binaries. Some of them don't work quite right but some of them seem to, which is exciting.</p>
<p><code>vagrant@precise64:~/libcoin/bin$ ls <br><br />
blocktree  client        extended_key  jsonance  merkletrie  ponzicoin   sqliterate<br />
claims     coinexplorer  extrawallet   libcoind  namecoin    simplecoin<br />
</code></p>
<p>The one binary I really wanted to work was ponzicoin, since it is its own custom currency. Unfortunately it segfaults when I run it. Which means I am going to have to really go in and debug.</p>
<p><code><br />
vagrant@precise64:~/libcoin/bin$ ./ponzicoin <br></p>
<p>Logger started<br />
Block with hash: 8e4628fc0532a2b1f6e601e3a39a537e264d722ec1fa43e018aa88cf66ed124f mined<br />
terminate called without an active exception<br />
Aborted<br />
</code></p>
