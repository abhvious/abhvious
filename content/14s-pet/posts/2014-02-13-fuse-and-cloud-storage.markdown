---
layout: post
status: publish
published: true
title: Fuse and cloud storage
author: saba
author_login: saba
author_email: se8dq@virginia.edu
wordpress_id: 144
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=144
date: '2014-02-13 23:10:07 -0500'
date_gmt: '2014-02-13 23:10:07 -0500'
tags: []
categories: 14s-pet
comments: []
---
<p>I spent my time this week looking into Fuse as a means of accessing cloud storage services.  For a couple days I read background information and tutorials about what Fuse does, how it works, how to set it up, etc.  After this, I looked for already existing implementations for accessing various cloud storage providers, in hopes that I could implement ORAM on top of an existing Fuse filesystem instead of having to write one from scratch.  I found that many, many people have made fuse filesystems for connecting to cloud providers, but most such filesystems were broken or otherwise problematic.  Here are a few that I looked at.  First the bad ones:</p>
<ul>
<li>gdocsfs: Written in Java for accessing google docs.  Didn't work because it hasn't been updated for a long time and was also trying to send your password unencrypted.  The functionality described from back when it did work was pretty limited since it was designed for the old google docs that only accepted  a couple file types</li>
<li>google-docs-fs: Python fuse filesystem for accessing google docs.  This one almost worked but didn't allow opening files and only listed some of the files on my drive</li>
<li>dropfuse: This is the fuse filesystem for DropBox that Professor Shelat originally told me about when we talked about this project and it seems to work alright.  Unfortunately, it doesn't fit well with this project because it only accesses public links from DropBox (we're trying to improve privacy, so there may be a philosophical difference here), and it caches all the data on the DropBox file accessed, which conflicts with the notion of cloud storage that we're trying to achieve</li>
<li>Cloud.com's tool: cloud.com provides a nice command-line interface for mounting their storage locally.  Unfortunately, this tool also makes local copies of everything, so it's not exactly what it advertises to be.</li>
</ul>
<p>I had success with two Fuse filesystems: google-drive-ocamlfuse and copy-fuse.</p>
<ul>
<li>google-drive-ocamlfuse <a href="https://github.com/astrada/google-drive-ocamlfuse">(https://github.com/astrada/google-drive-ocamlfuse)</a> works well and provides access to google drive without any hiccups.  The only drawback is that it is very slow, even slower than transferring data over the network would warrant.  Some comments about this online blame it on the google drive api itself, so this doesn't seem like something that can be easily avoided.  Seeing as that ORAM's computation overhead further slows things down, this is a very undesirable characteristic.</li>
<li>copy-fuse <a href="https://github.com/copy-app/copy-fuse">(https://github.com/copy-app/copy-fuse)</a> also works fine for copy.com accounts.  Although it's not blazingly fast, it is significantly faster than google-drive-ocamlfuse.  Although copy.com is not as popular as google drive, I think it would be wiser to add ORAM on top of this filesystem since time savings will be crucial.</li>
</ul>
<p>The next steps are to add encryption on top of copy-fuse and then actually implement the Path ORAM.  For next week, I'd like to figure out whether I want to modify the copy-fuse code or add a layer on top of that.  My goal is to have it encrypting files by the end of next week.</p>
