---
layout: post
status: publish
published: true
title: 'Now You See Me, Now You Don''t: the Read O(nly)RAM'
author: saba
author_login: saba
author_email: se8dq@virginia.edu
wordpress_id: 215
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=215
date: '2014-03-01 01:31:07 -0500'
date_gmt: '2014-03-01 01:31:07 -0500'
tags: []
categories: 14s-pet
comments: []
---
<p>My progress this week is more or less as mentioned in class. My ORAM fuse filesystem is set up such that it can be mounted and the user can look at the listing of files in the directory and do any other things that don't require actually connecting to the remote storage.  Additionally, the user can at this point read files from the oram, but the putting back part is not complete.  This means that a pre-prepared ORAM can have each item read from it once and then destroyed.  Clearly, this is not of any practical use, but it means I'm a little over halfway done with the read operation, and the write operation should be very similar.  My current goal is to have a basic, working ORAM before leaving for spring break.</p>
<p>The minimal list of operations to write before being done that I'm keeping currently consists of just five functions (read, write, mknod, unlink, rename), but there is a lot of behavior that my ORAM currently can't accomplish that I may be interested in adding next.  For example, I haven't bothered allowing for any directory structure in the ORAM, it currently has to be just one big folder.  Furthermore, there is no mechanism for determining or changing file permissions on the files in the ORAM.  These are important but not critical to having a working model.  One thing that I think really is critical is figuring out how to store a file that is bigger than one page. Currently, I don't have a way of storing files that take up more than the 4k page size in the oram I'm working with.  That's something important I'd like to figure out in the next week.</p>
<p>Since my approach to using fuse has been modular and separately employs the google drive fuse, encfs, and my own oram, I've been testing the oram independently of the other two fuse filesystems.  This is helpful for testing because it means I don't have to wait for data to be transferred over the network.  I look forward to finding out how long operations will take once the data transfers start having to happen over a network connection instead of just on my hard drive.  Another goal for next week, or perhaps spring break/the week after would be to write a setup script that allows a user to specify the parameters of their oram and have it setup without having to worry about changing constants in various files as I have it now.</p>
