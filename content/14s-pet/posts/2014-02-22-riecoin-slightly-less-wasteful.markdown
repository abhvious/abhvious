---
layout: post
status: publish
published: true
title: "(non-project) Riecoin: slightly less wasteful?"
author: jack
author_login: jack
author_email: jhd3pa@virginia.edu
wordpress_id: 190
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=190
date: '2014-02-22 21:44:37 -0500'
date_gmt: '2014-02-22 21:44:37 -0500'
tags: []
categories: 14s-pet
comments: []
---
<p><a href="http://riecoin.org/">http://riecoin.org/</a><br />
Apparently uses the finding of prime number constellations as Proof of Work.<br />
One of the criticisms Abhi brought up about the use of SHA256 in bitcoin is that bitoin depends on properties of the hash about which it was not designed to make guarantees. I'm curious about the implications of this particular proof of work scheme, since it's based on actual mathematics (as I understand it).</p>
