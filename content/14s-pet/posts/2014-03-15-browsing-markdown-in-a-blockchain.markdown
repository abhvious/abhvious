---
layout: post
status: publish
published: true
title: Browsing Markdown in a blockchain
author: nick
author_login: nick
author_email: nskelsey@gmail.com
wordpress_id: 226
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=226
date: '2014-03-15 21:35:14 -0400'
date_gmt: '2014-03-15 21:35:14 -0400'
tags: []
categories: 14s-pet
comments: []
---
<p>After reading <a href="http://www.righto.com/2014/02/ascii-bernanke-wikileaks-photographs.html">this</a> blog post. I decided to go ahead and make a small service that lets you create and view markdown posts that are stored in the blockchain. It works and it lives <a href="http://jeffcoin.nskelsey.com">here</a> (the code is <a href="http://github.com/NSkelsey/jeffcoin">here</a>). I learned a whole bunch about bitcoin building this thing and it didn't turn out to be that hard. I did blatantly copy <a href="https://gist.github.com/shirriff/bfc4df70a02732493a28#file-bitcoin-insertion-tool-py">code</a> attributed to satoshi....</p>
<p>Anyway, there are several limitations to the service right now that I would like to go into. The main one is that it is running on bitcoin's testnet. This is because the transactions that store the posts are not relayed by nodes in the real network. This is done to prevent the accumulation of transactions precisely like the ones I am creating. The size of the block chain has become a real problem for the portability and accessiblilty of bitcoin and anything that needlessly increases that rate of storage is being excised by the bitcoin developers.</p>
<p>If you try to join the bitcoin network today it takes many hours for your machine to download the entire blockchain and build out the local database verifies blocks and transactions as they are passed around the network.</p>
<p>This scaleability problem is so bad that the developers placed a <a href="https://github.com/bitcoin/bitcoin/pull/2577">minimum value</a> on every output for a transaction so clients do not have to track these small values. They have similarly reduced the number of allowed transactions to 2 types. Either you can do the typical thing and send coins to an address that someone else owns or you can create a multisignature transaction. The multisig transactions are pretty cool, but they have been limited significantly in their functionality and extensibility due to the blockchain bloat problem. Currently the max size of a multisig transaction is <a href="https://github.com/petertodd/bitcoin/blob/5afaceebc20660d9daeba6a544385ba6b138d71d/src/script.h#L25">520 bytes</a>. Meaning that 520 bytes is the absolute maximum amount of data you could stuff into the scriptPubKey of a transaction. However, there is far more verification that goes into a multisig transaction which prevents someone (like me) from using those full 520 bytes. This means that if I want to run this service on the real blockchain it is going to much more expensive and more complicated.</p>
<p>A second smaller problem is the services reliance on a local database that stores pointers to the transactions that contain the blog posts. I would like to devise some scheme where a server running this service could easily find an index file in the blockchain. This would let anyone essentially bootstrap the same service relying only on information in the blockchain.</p>
<p>A third problem is the lack of encryption between nodes relaying transactions through the network. Meaning an attacker could easily discover that your machine is the one publishing this information into the blockchain. Even with encrypted traffic between nodes I think a sophisticated attacker could source the transactions back to a particular client if they wanted to.</p>
<p>Also, even in the testnet some nodes refuse to relay extremely large transactions I want to upload. It took several hours to upload transactions, which were in 100s of kilobytes. But, if you could mine your own blocks while creating these transactions you could potentially get around this problem. You would just publishing a block periodically that had all of these non-standard transactions already baked into them.</p>
<p>Finally, this service is only marginally useful as is. I think a mechanism to upload files, a generic specification about how things should be stored and a method of distributed indexing would go a long way to make this a real product.</p>
