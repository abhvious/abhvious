---
layout: post
status: publish
published: true
title: Be careful what you hash
author: jack
author_login: jack
author_email: jhd3pa@virginia.edu
excerpt: Over the last few days I've been researching the merkle tree bitcoin asset
  proof system, and I have found a flaw in the existing implementations that might
  allow the prover to hide liabilities undetectably. Fortunately, it should be easy
  to fix (in fact, it has been fixed already in some of the documentation).
wordpress_id: 217
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=217
date: '2014-03-04 18:58:14 -0500'
date_gmt: '2014-03-04 18:58:14 -0500'
tags: []
categories: 14s-pet
comments:
- id: 41
  author: nick
  author_email: nskelsey@gmail.com
  author_url: ''
  date: '2014-03-05 21:36:43 -0500'
  date_gmt: '2014-03-05 21:36:43 -0500'
  content: What is the point of demonstrating that they can generate a hash tree if
    not to show that the sum of the user's inputs adds up to some grand total. I guess
    I am missing the point of proveit here. Please enlighten
- id: 42
  author: jack
  author_email: jhd3pa@virginia.edu
  author_url: ''
  date: '2014-03-06 00:04:03 -0500'
  date_gmt: '2014-03-06 00:04:03 -0500'
  content: The point is to show the total liability of the users in such a way that
    the exchange can't fraudulently reduce the sum. The exchange then proves through
    a different mechanism (either some sort of zero knowledge proof, or by revealing
    the numbers) that it has a volume of bitcoin large enough to cover the combined
    liability of its users. By using a mechanism to report the liability of the users
    in such a way that it can't lie, the exchange hopes to convince them that it's
    solvent.
---
<p>Over the last few days I've been researching the merkle tree bitcoin asset proof system, and I have found a flaw in the existing implementations that might allow the prover to hide liabilities undetectably. Fortunately, it should be easy to fix (in fact, it has been fixed already in some of the documentation).<a id="more"></a><a id="more-217"></a></p>
<p>Rather than explaining how the scheme works, I'll direct you to <a href="https://iwilcox.me.uk/2014/proving-bitcoin-reserves" target="_blank">this page</a>, which does it better than I could, and for now I'll discuss the flaw under the assumption that the reader already understands.</p>
<p>As of right now, there are two implementations of this system, <a href="https://github.com/olalonde/blind-solvency-proof" target="_blank">one written in javascript</a> and <a href="https://github.com/ConceptPending/proveit" target="_blank">the other in python</a>. Both are about a week old. One of them has fairly decent documentation, which is what allowed me to figure this out. The other has very little documentation, but it's the one I tested this on to make sure it actually works.</p>
<p>The flaw is this: leaf node hashes are calculated based on only the user's identifier and a salt. The balance isn't hashed, which isn't really a problem so long as everyone in the system has a unique balance. However, if two users have the same balance, it is possible to hide the value owned by one of them. The prover is allowed to chose how the users are arranged in the tree, and users cannot discover the arrangement. If the prover arranges the tree such that users with identical values are neighbors, it can generate different hash trees for each of them. In the tree visible to each of the two users, his/her own balance is correct, but the balance of the other user is reduced. The math will work out the same for both of them, and they'll both validate against a root hash calculated with a reduced sum for their pair. Nothing will seem amiss to either user, as each will see the correct value for themself, and neither knows what the neighbor's value <em>should</em> be (except that it shouldn't be negative). Fortunately, this only works for leaf nodes, since interior node <em>do</em> include their value in the hash.</p>
<p>See the below an example using the proveit implementation, using the test data from proveit's own documentation.</p>
<pre>&gt;&gt;&gt; from proveit import *
&gt;&gt;&gt; customers = [ Node(123, 'unique identifier, preferably hashed'), Node(456, 'unique identifier2, preferably hashed as well')]
&gt;&gt;&gt; customers2 = [ Node(456, 'unique identifier, preferably hashed'), Node(123, 'unique identifier2, preferably hashed as well')]
&gt;&gt;&gt; hashtree = HashTree(customers)
&gt;&gt;&gt; hashtree2 = HashTree(customers2)
&gt;&gt;&gt; hashtree.tree[1][0].hashdigest                                             
'20b7c098df834da7cb1a910d5bcf65de962efb863b8327e6189acfb558ddefa6'
&gt;&gt;&gt; hashtree2.tree[1][0].hashdigest
'20b7c098df834da7cb1a910d5bcf65de962efb863b8327e6189acfb558ddefa6'</pre>
<p>The two root hash values are on the last lines, and they are the same, as you can see.<br />
Of course, this is only possible when two accounts have the same balance, but in a large enough system, this might not be uncommon, especially among low-balance accounts and in an exchange, where I expect trades often happen in evenly-sized chunks, and where the prover might be able to manipulate fees in order to increase the likelihood of identical balances. As you might guess, the fix is simple: if the value is included in the hash of the leaf nodes, then the value swap cannot happen without changing the users' hashes, preventing the modified trees from validating.</p>
<p>Upon finishing this post, I reloaded <a href="https://iwilcox.me.uk/2014/proving-bitcoin-reserves" target="_blank">this page</a>, and discovered that it has been updated since last night to address this problem (though not by name). Presumably the implementers will soon fix it as well.</p>
