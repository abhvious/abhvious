---
layout: post
status: publish
published: true
title: "(seed-of-a-project) Testing security of crypto schemes on commercial apps "
author: peter
author_login: peter
author_email: pts2td@virginia.edu
wordpress_id: 90
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=90
date: '2014-02-02 21:05:45 -0500'
date_gmt: '2014-02-02 21:05:45 -0500'
tags: []
categories: 14s-pet
comments:
- id: 26
  author: abhi
  author_email: abhi@virginia.edu
  author_url: ''
  date: '2014-02-11 18:15:09 -0500'
  date_gmt: '2014-02-11 18:15:09 -0500'
  content: Interesting.  The idea would be to setup a framework for evaluating the
    apps...it might take a lot of people to actually do the work, perhaps a crowdsourcing
    method?
---
<p>As of this writing, there are several different crypto apps available to the mass market on the android platform. Silent Circle may be the most famous after debuting last year [1].  Silent Circle is available for $240 a year, and uses ZRTP for the two phones to agree on a key without the need of a third party. Part of the way this is done by presenting the users with a 'short authentication string', and then it establishes a key over the encrypted connection for future communications.  The most vulnerable part of it is that the phone app allows the user to call Plain Old Telephones, the problem here is that the correspondence won't be encrypted until it reaches the Silent Circle Servers.</p>
<p>Another initialive with a host of apps is the Guardian Project [2]. It was focused on applying GnuPG to android phones, and seems to have decent success.</p>
<p>While it may be outside the scope of feasibility for this class, one project would be examining the different vulnerabilities that would be needed to compromise a communication over these different apps, and what level of access you would need to do so. Part of the Guardian Project involves a TOR web browser on android, and while TOR may not be crackable to the laymen, tracking where people enter and exit has proven a viable strategy for finding people.</p>
<p>I haven't had any serious experience with implementing crypto in the past (though like jack I'm working on our Defense Against the Dark Arts Assignment) but I'm certainly sniffing out projects, possibly with a group. I look forward to more practical suggestions.</p>
<p>1. https://silentcircle.com/web/how-it-works/</p>
<p>2. https://guardianproject.info/</p>
