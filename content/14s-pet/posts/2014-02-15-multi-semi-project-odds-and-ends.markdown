---
layout: post
status: publish
published: true
title: "(multi-semi-project) Odds and Ends"
author: jack
author_login: jack
author_email: jhd3pa@virginia.edu
wordpress_id: 160
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=160
date: '2014-02-15 18:02:28 -0500'
date_gmt: '2014-02-15 18:02:28 -0500'
tags: []
categories: 14s-pet
comments:
- id: 38
  author: tommy
  author_email: tjt7a@virginia.edu
  author_url: ''
  date: '2014-02-26 03:26:00 -0500'
  date_gmt: '2014-02-26 03:26:00 -0500'
  content: "Jack, below we have answered your questions about our system:\r\n\r\nWhat
    happens if Alice wants to change the attributes she has assigned to Bob? Do they
    need to renegotiate keys?\r\n-----\r\nIf Alice is removing an attribute from Bob,
    no. What Alice needs to do is determine which attributes she wants to 'remove'
    from Bob, and re-negotiate with everyone else that shares those attributes. In
    effect, Bob won't know that he's been removed from the attribute, but everyone
    else will have the necessary updated decryption keys to still receive those attribute
    messages. Everyone else will know that something has changed, but not necessarily
    what the change was.\r\n\r\nOtherwise, if adding attributes, yes. Alice will need
    to use the shared symmetric key to contact Bob and re-negotiate with encryption/decryption
    keys.\r\n\r\n-----"
---
<p>So I've done some additional research on the AVX and SNI Certificate Census Projects, and also there are a few other things at the end.</p>
<p>AVX comes first. I've done a (slightly) more scientific version of my test from the last post. The problem is the same - dot product of two vectors, one containing only 3s and the other only 4s - but with different parameters. The software is now 64bit (it turns out this is <em>mostly</em> irrelevant), and uses only aligned memory, which eliminates any performance penalty due to unaligned memory, and allows me to keep twice as much data in flight (since SSE/AVX can read operands directly from memory if they are aligned). Additionally, vector length and run count are now configurable. The entire project (for Mac OS X; probably doesn't compile on linux) can be found <a href="https://www.mediafire.com/?5nx98laixtinaad" target="_blank">here</a>. Also note that, like last time, this is a test of a problem unrelated to and not characteristic of most of the algorithms we'd be hoping to accelerate, using a feature (floating point) which is mostly irrelevant to them, performed on an outdated processor. All times quoted here will be for 10M runs each.</p>
<p>For vector lengths of 1024 elements:</p>
<ul>
<li>SSE - 8 simultaneous elements - 1.402s</li>
<li>SSE - 16 simultaneous elements - 1.041s   (the maximum number of elements for 32 bits)</li>
<li>SSE - 32 simultaneous elements - 0.961s   (the maximum number of elements for 64 bits)</li>
<li>AVX - 32 simultaneous elements - 1.456s  (the maximum number of elements for 32 bits)</li>
<li>AVX - 64 simultaneous elements - 1.470s  (the maximum number of elements for 64 bits)</li>
</ul>
<p>For vector lengths of 10240 elements:</p>
<ul>
<li>SSE - 8 simultaneous elements - 17.631s</li>
<li>SSE - 16 simultaneous elements - 18.990s</li>
<li>SSE - 32 simultaneous elements - 16.763s</li>
<li>AVX - 32 simultaneous elements - 20.076s</li>
<li>AVX - 64 simultaneous elements - 17.236s</li>
</ul>
<p>For vector lengths of 102400 elements:</p>
<ul>
<li>SSE - 8 simultaneous elements - 4m15.203s</li>
<li>SSE - 16 simultaneous elements - 4m15.761s</li>
<li>SSE - 32 simultaneous elements - 4m5.031s</li>
<li>AVX - 32 simultaneous elements - 4m20.516s</li>
<li>AVX - 64 simultaneous elements - 4m13.378s</li>
</ul>
<p>The results here are interesting, and I think they may reflect on both scheduling and what seems to be a memory bandwidth wall. The exceptionally poor performance of AVX for small vector lengths may be due to the horizontal sum command, which is not very well optimized, so I have read. Not only is an additional horizontal sum operation required, but each may take longer than the equivalent SSE instruction. However, I have no firm proof that this is true, and I suspect there is some sort of complex interplay involving the on-cpu scheduling and instruction reordering, especially given that SSE/16 is anomalously (but consistently) slow for vector lenth 10240, even though it's faster in the other two tests. I have ordered my instructions in a straightforward way which I believe to be efficient, though perhaps not 100% optimal, and which I think will allow the cpu to reorder them effectively (there was an article which suggested that the particular format I've used is good for this, but I've lost the link).  I also suspect that the fact that they all run in approximately the same amount of time is an indication that in this case the factor determining performance is memory bandwidth, not actual execution speed. In other words, the results may improve for algorithms with a lower memory-access-to-computation ratio.</p>
<p>&nbsp;</p>
<p>Now, time for the Certificate Census</p>
<p>The first hurdle in this project is finding a set of DNS records which can be polled for certificates. Getting these for the three major TLDs isn't particularly difficult, but it's not so easy for many other TLDs, and even then, only second level domains would be covered. I have a sneaking suspicion that third level domains (i.e. subdomains) will be important to this census, since servers hosting multiple subdomains (or domains, for that matter) almost always uses SNI to distinguish between them these days. There are a number of groups who have such databases (google, data mining corps, isps, &amp;c), but none of them are publicly available as far as I can tell. Fortunately, there was a project which generated a <a href="http://dnscensus2013.neocities.org/index.html" target="_blank">public database of DNS records</a> just last year. The dataset comprises 2.5B records, which leads me into the next hurdle: hardware requirements. Obviously, we'll want a pretty hefty database system if we plan to run queries against that many records, but this isn't too pressing an issue.  The larger problem is that, assuming 1/5 of the servers on the list have working SSL (I have no basis for this assumption) and that their certificates average around 1.5K (another unscientific number; half way between the size of my own self-signed cert, and gmail's cert), then the certificates alone come in slightly under 1tb, which will fit onto a single hard disk, as long as you don't plan on doing anything with it very quickly. Even a simple sequential read of that much data could take a few hours on a slowish disk, so we may want to look into more complex storage solutions. I suspect the optimal arrangement would involve a single high-throughput database server (SPARC T2s are pretty cheap on ebay right now, incidentally), with multiple satellite machines responsible for data gathering and analysis. NoSQL is also an option, but it removes a certain amount of flexibility and adds some complication.</p>
<p>&nbsp;</p>
<p>Finally, other bits and peices:</p>
<p>Abbas, if you read this, would a cert database be useful for your SSL cert monitoring project?</p>
<p>Tom and Nathan, WP won't let me leave a comment on your last post, but what happens if Alice wants to change the attributes she has assigned to Bob? Do they need to renegotiate keys? If so, wouldn't this be nonideal, since it informs Bob that Alice has changed his attributes (there's a reason why facebook doesn't tell users when they've been blocked). While this sort of enforced honesty is actually a very interesting concept, I suspect it might not sit well with a lot of people.</p>
<p>If we're going to go to the trouble of acquiring a cert database, what else could we do with it aside from trying Abhi's extended fraction attack?</p>
<p>What does everyone think of implementing in-camera steganography on canon DSLRs? I believe it should be possible, but I'm struggling to find a real use-case.</p>
