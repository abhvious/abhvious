---
layout: post
status: publish
published: true
title: ORAM Simulation Results
author: saba
author_login: saba
author_email: se8dq@virginia.edu
wordpress_id: 268
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=268
date: '2014-03-28 16:58:24 -0400'
date_gmt: '2014-03-28 16:58:24 -0400'
tags: []
categories: 14s-pet
comments: []
---
<p>This week, I rewrote my oram filesystem in the form of a simulation where all the contents of the oram are kept in memory and all the actual data storing parts are removed.  I then ran the simulation with various block sizes and bucket sizes in order to get a sense for the oram's behavior.</p>
<p>There were 3 main iterations of the simulations.  In the first iteration, I set a max operations cap at 50,000 operations to overflow and ran orams ranging in size from 15 to 262143 blocks (going up in powers of two) with increasing numbers of buckets until the mean number of operations from 300 trials was equal to or over 50,000.  Every oram size tested got to over 50,000 operations by bucket size 5.</p>
<p>The next round of simulations had the operation cap at 1,000,000 and only took the mean of 30 trials for each size/buckets combo to save time.  Surprisingly, almost every instance still reached the cap by bucket size 5, with the exception of 2 instances where the mean was just under 1,000,000, suggesting that most trials would have gone on well over 1,000,000 but a few that had the misfortune of dying early pulled the average down (since even if the oram could have gone on forever, 1,000,000 was the biggest contribution it could make to the mean calculation).  Even more surprisingly, the orams which went to bucket size  6 were not the largest ones tested (although they were both in the larger half).</p>
<p>The last round of simulations involved removing the cap completely to see how far orams would go before having an overflow.  In this round, I started averaging only 10 trials and also printed out the individual results (something I probably should have done before too) since I expected very long running times.  Although the results for 2 and 3 buckets were about the same as in the trials with capped numbers of operations, the means for 4 buckets were significantly higher than they were for any of the capped trials.  Since I had numbers for individual orams instead of averages this time, I was also surprised that the range of numbers of operations to overflow seemed to vary a great deal for four buckets, being just over 25,000 in one instance and almost 3.5 million in another.  When I got to 5 blocks, I ran into a problem, which made it such that I could only test two or three oram sizes: not a single oram overflowed when given hours of simulation time.  A 127-block and 4095-block oram didn't overflow enough times (I was still doing 30 trials at that point) over a 6 hour period, and even when I reduced the number of trials to just 1, None of the 127, 4095, or 262143 block orams had a single overflow in 1.5 hours.</p>
<p>It would appear that a bucket size of 5 or 6 (to be safe), is enough to allow these orams to run almost indefinitely or at least for a very large number of operations.  Unfortunately, due to some shortcomings of my oram simulator (that can be fixed probably without too much effort), I wasn't able to test orams larger than 262143 blocks.  It may be good to test orams of significantly larger block sizes to make sure this property holds even there, but the lack of any real decrease in the number of operations as the oram sizes I did test grew seem to suggest that this property does hold as the oram size grows.  Also useful would be to run the simulations again and also record variance in the number of operations until failure.  These may be things I work on for next week.</p>
