---
layout: post
status: publish
published: true
title: "(not a project) Optimization of SHA2-256 for partial collisions"
author: tpd5bf
author_login: tpd5bf
author_email: tpd5bf@virginia.edu
wordpress_id: 57
wordpress_url: http://crypto.cs.virginia.edu/courses/14s-pet/?p=57
date: '2014-01-29 02:05:15 -0500'
date_gmt: '2014-01-29 02:05:15 -0500'
tags: []
categories: 14s-pet
comments: []
---
<p>Brought up in class today was the feasibility of modifying the SHA256 function for bitcoin mining, because you only want hash outputs that start with lots of 0's.</p>
<p>There are two approaches to this.  The first is to try to compute bit[0] without computing bit[255] to save on computation. Some discussion here</p>
<p><a href="https://bitcointalk.org/index.php?topic=55888.0">https://bitcointalk.org/index.php?topic=55888.0</a></p>
<p>and technical results showing that it could work here:</p>
<p><a href="http://jheusser.github.io/2013/02/03/satcoin.html">http://jheusser.github.io/2013/02/03/satcoin.html</a></p>
<p>Another optimization which I was not aware of, but in fact may be widely practiced, is that by the time round 61 has passed, the MSBs have been computed and rounds 62, 63 and 64 only shift those bits.  Checking the state after round 61 lets you skip the final 3 rounds of the hash function.  This is implemented here:</p>
<p><a href="https://github.com/progranism/Open-Source-FPGA-Bitcoin-Miner">https://github.com/progranism/Open-Source-FPGA-Bitcoin-Miner</a></p>
<p>and probably in most ASICs as well.</p>
<p>-Tadge</p>
