+++
date = "2016-01-30T20:53:21-05:00"
title = "Teaching"

+++

<h3>Fall 2016</h3>

<p>I am teaching CS4800 Algorithms in the Fall semester at Northeastern.</p>

<ul>
<li><a href="/16f-4800">16F Algorithms</a></p></li>
</ul>

<p>In 2016, I was selected as the U of Virginia Professor of the year by the student ACM chapter.
	In 2013, I was among 5 professors selected from across the university by the U. of Virginia student council to present a general topic to the student body in the "Look Hoos Talking" event.  I choose to introduce cryptography, and you can see the attempt in this youtube video, <a href="https://www.youtube.com/watch?v=Rv0YAyGq8tM">Gur zlfgvdhr bs pelcgb</a>.</p>.

<h3>Prior semesters</h3>
<ul>
<li><a href="/16s-4102">16S Algorithms</a></p></li>
<li> 14f,15s,15f (Sabbatical)
<li><a href="/14s-pet">14S Privacy Enhancing Technology</a></p>
<p>I also supervise CS1501: <a href="https://github.com/mlp5ab/CS1501">Quick introduction to Nodejs</a>, run by student Michael Paris and run from github!  </p>
<li><a href="/13f-4102">13f-4102 Algorithms</a>
<li>13s-6161 Algorithms (graduate)
<li>12f-4501 Computer security
<li>12s-8501 Seminar on CCA2 encryption
<li>11f-4501 Advanced Algorithms and Implementation
<li>11f-4102 Algorithms
<li>11s (leave)
<li>10f-3102 Theory of Computation
<li>10s-6501 Introduction to Cryptography
<li>09f-4102 Algorithms
<li>09s-661 Algorithms (grad)
<li>08f-432 Algorithms
<li>08s Seminar
<li>07f-651 Intro to Cryptography
</ul>