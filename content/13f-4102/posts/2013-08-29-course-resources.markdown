---
layout: post
status: publish
published: true
title: Course Resources
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 21
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=21
date: '2013-08-29 10:22:39 -0400'
date_gmt: '2013-08-29 10:22:39 -0400'
categories:
- Uncategorized
tags: []
comments: []
---
<p>You do not need a textbook for this course, but if you would like to purchase one for review, I recommend the following two books:</p>
<ul>
<li>CLRS: "<a href="http://www.amazon.com/Introduction-Algorithms-Thomas-H-Cormen/dp/0262033844">Introduction to Algorithms</a>",</li>
<li>Kleinberg+Tardos, "<a href="http://www.amazon.com/Algorithm-Design-Jon-Kleinberg/dp/0321295358">Algorithm Design</a>"</li>
</ul>
<p>In addition, I recommend the following Coursera links for you to review between and after my lectures:</p>
<ul>
<li>Tim Roughgarden's  open course <a href="https://class.coursera.org/algo-004/lecture/preview">Alg Part I</a></li>
<li>Wayne+Sedgewick's course <a href="https://class.coursera.org/algs4partI-003/class/index">Alg Design Part I</a></li>
</ul>
