---
layout: post
status: publish
published: true
title: 'L13: Gerrymandering + Intro to Greedy Alg'
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 148
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=148
date: '2013-10-08T14:36:02'
date_gmt: '2013-10-08 18:36:02 -0400'
categories: 13f4102 lecture
tags: []
comments: []
---
<p><a href="/13f4102/wp-content/uploads/L13-4102-F13-delivered.pdf">Annotated L13 notes</a></p>
<p>Topic covered: We finished the gerrymandering problem, and we started a unit on greedy algorithms.  We covered a course scheduling problem, and the cache-management problem.</p>
<p>A video of L13 <a href="/13f4102/wp-content/uploads/L13/index.html">will be here</a> when available.</p>
