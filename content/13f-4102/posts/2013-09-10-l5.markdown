---
layout: post
status: publish
published: true
title: L5
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 66
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=66
date: '2013-09-10T11:46:30'
date_gmt: '2013-09-10 15:46:30 -0400'
categories: 13f4102
tags: []
comments: []
---
<p><a href="/13f4102/wp-content/uploads/L5-4102-F13.pdf">L5 Raw notes</a> are here in case you want to follow along in lecture.</p>
<p>Here are the <a href="/13f4102/wp-content/uploads/L5-4102-F13-delivered.pdf">annontated L5 notes</a>.</p>
<p>A <a href="/13f4102/wp-content/uploads/L5/index.html">video of class</a> is now available.</p>
