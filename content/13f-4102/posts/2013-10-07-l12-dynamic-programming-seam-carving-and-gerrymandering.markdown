---
layout: post
status: publish
published: true
title: 'L12: Dynamic Programming, seam-carving and gerrymandering'
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 146
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=146
date: '2013-10-07T10:31:59'
date_gmt: '2013-10-07 14:31:59 -0400'
categories: 13f4102 lecture
tags: []
comments: []
---
<p><a href="/13f4102/wp-content/uploads/L11-4102-F13.pdf">Raw L12 notes</a> are a continuation of the L11 notes.  The <a href="/13f4102/wp-content/uploads/L12-4102-F13-delivered.pdf">annotated L12 notes</a>  are here as well.</p>
<p>Topic covered: Dynamic programming: seam carving, gerrymandering</p>
<p>A video of L12 <a href="/13f4102/wp-content/uploads/L12/index.html">will be here</a> when available.</p>
