---
layout: post
status: publish
published: true
title: 'L17: MST, Prim and shortest paths'
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 179
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=179
date: '2013-10-24T12:04:20'
date_gmt: '2013-10-24 16:04:20 -0400'
categories: 13f4102 lecture
tags: []
comments: []
---
<p><a href="/13f4102/wp-content/uploads/L17-4102-F13.pdf">Raw L17 notes</a> and <a href="/13f4102/wp-content/uploads/L17-4102-F13-delivered.pdf">Annotated L17 notes</a> (when they are available)</p>
<p>Topic covered: We discussed Prim's MST algorithm and then introduced shortest path algorithms, starting with Dijkstra.</p>
<p>A video of L17 <a href="/13f4102/wp-content/uploads/L17/index.html">will be here</a> when available.</p>
