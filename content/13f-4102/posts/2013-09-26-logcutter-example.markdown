---
layout: post
status: publish
published: true
title: Logcutter example
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 120
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=120
date: '2013-09-26 10:24:53 -0400'
date_gmt: '2013-09-26 14:24:53 -0400'
categories:
- Uncategorized
tags: []
comments: []
---
<p>I am posting (a) a short explanation of why the greedy approach suggested in class on Tuesday does not work for this problem, and (b) a working example of the algorithm for an instance of the problem.  <a href="/13f4102/wp-content/uploads/logcutter.pdf">Logcutter examples worked out</a></p>
