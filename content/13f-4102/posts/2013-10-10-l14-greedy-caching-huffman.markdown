---
layout: post
status: publish
published: true
title: 'L14: Greedy, Caching + Huffman'
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 150
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=150
date: '2013-10-10T12:08:58'
date_gmt: '2013-10-10 16:08:58 -0400'
categories: 13f4102 lecture
tags: []
comments: []
---
<p><a href="/13f4102/wp-content/uploads/L14-4102-F13.pdf">Raw L14 notes</a> and<br />
<a href="/13f4102/wp-content/uploads/L14-4102-F13-delivered.pdf">Annotated L14 notes</a></p>
<p>Topic covered: We finished the gerrymandering problem, and we started a unit on greedy algorithms.  We covered a course scheduling problem, and the cache-management problem.</p>
<p>Unfortunately, the video for this class was not recorded.  Collab has an <a href="/13f4102/wp-content/uploads/L14-audio.mp4">audio recording</a> available.</p>
