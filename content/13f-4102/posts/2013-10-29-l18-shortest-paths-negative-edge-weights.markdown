---
layout: post
status: publish
published: true
title: 'L18: Shortest paths, negative edge weights,'
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 182
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=182
date: '2013-10-29T12:20:05'
date_gmt: '2013-10-29 16:20:05 -0400'
categories: 13f4102 lecture
tags: []
comments: []
---
<p><a href="/13f4102/wp-content/uploads/L18-4102-F13.pdf">Raw L18 notes</a> and <a href="/13f4102/wp-content/uploads/L18-4102-F13-delivered.pdf">Annotated L18 notes</a> (when they are available)</p>
<p>Topic covered: We discussed shortest path algorithms, both when edge weights are positive and when they might be negative:<br />
 Dijkstra and Bellman-Ford.</p>
<p>A video of L18 <a href="/13f4102/wp-content/uploads/L18/index.html">will be here</a> when available.</p>
