---
layout: post
status: publish
published: true
title: L2
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
excerpt: "<a href=\"/13f4102/wp-content/uploads/L2-4102-F13-delivered.pdf\">Annotated
  L2 slides in PDF</a>.\r\n\r\nVideo is included in the full version of the post (click
  on the \"Read More\" button below).\r\n\r\n<strong>Resources for this Lecture</strong>\r\n\r\n<ul>\r\n\t<li>Tim's
  lecture on <a href=\"https://class.coursera.org/algo-004/lecture/165\">Simple multiplication</a>,
  and <a href=\"https://class.coursera.org/algo-004/lecture/167\">Karatsuba</a></li>\r\n\r\n\t<li>Kevin
  Wayne+Robert Sedgewick's video on <a href=\"https://class.coursera.org/algs4partI-003/lecture/14\">Asymptotic
  notation</a></li>\r\n\t<li>Chapter 3 of CLRS covers asymptotic notation</li>\r\n\t<li>Chapter
  4 of CLRS covers the Divide & Conquer strategy that we will discuss for the next
  3-4 lectures</li>\r\n</ul>\r\n\r\n"
wordpress_id: 19
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=19
date: '2013-08-29 02:31:37 -0400'
date_gmt: '2013-08-29 02:31:37 -0400'
categories: 13f4102
tags: []
comments: []
---
<p><a href="/13f4102/wp-content/uploads/L2-4102-F13-delivered.pdf">Annotated L2 slides in PDF</a>.</p>
<p>Video is included in the full version of the post (click on the "Read More" button below).</p>
<p><strong>Resources for this Lecture</strong></p>
<ul>
<li>Tim's lecture on <a href="https://class.coursera.org/algo-004/lecture/165">Simple multiplication</a>, and <a href="https://class.coursera.org/algo-004/lecture/167">Karatsuba</a></li>
<li>Kevin Wayne+Robert Sedgewick's video on <a href="https://class.coursera.org/algs4partI-003/lecture/14">Asymptotic notation</a></li>
<li>Chapter 3 of CLRS covers asymptotic notation</li>
<li>Chapter 4 of CLRS covers the Divide & Conquer strategy that we will discuss for the next 3-4 lectures</li>
</ul>
<p><a id="more"></a><a id="more-19"></a></p>
<p><video id="example_video_1" class="video-js vjs-default-skin" controls preload="auto" width="540" height="315"<br />
  poster="/13f4102/wp-content/uploads/L2-4102-F13.001.png"<br />
  data-setup='{"example_option":true}'><br />
 <source src="/13f4102/wp-content/uploads/L2a.mp4" type='video/mp4' /><br />
</video></p>
