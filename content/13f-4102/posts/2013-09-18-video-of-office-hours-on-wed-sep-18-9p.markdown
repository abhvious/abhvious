---
layout: post
status: publish
published: true
title: Video of Office hours on Wed Sep 18, 9p
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 96
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=96
date: '2013-09-18 22:29:39 -0400'
date_gmt: '2013-09-19 02:29:39 -0400'
categories: 13f4102
tags: []
comments: []
---
<p>I have foolishly let myself be recorded on Youtube.  So if you missed it, you can review the most bizarre video of our google hangouts office hour right <a href="https://plus.google.com/115334887111697323553/posts/8GX46nRZjE2">here</a>.</p>
<p>Spoiler alert: answers/hints are revealed for many of the problems.</p>
