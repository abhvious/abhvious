---
layout: post
status: publish
published: true
title: H7
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 202
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=202
date: '2013-11-14 15:44:01 -0500'
date_gmt: '2013-11-14 19:44:01 -0500'
categories: 13f4102
tags: []
comments: []
---
<p>Homework 7 is due at 11.59p on Mon, Nov 25.</p>
<p>
<a href="https://crypto.cs.virginia.edu/13f4102/wp-content/uploads/hw7.pdf">H7 pdf</a> <a href="https://crypto.cs.virginia.edu/13f4102/wp-content/uploads/hw7.tex">H7 tex template</a><br />
<a href="https://crypto.cs.virginia.edu/13f4102/wp-content/uploads/bob.png">Bob png</a><br />
<a href="https://crypto.cs.virginia.edu/13f4102/wp-content/uploads/hw7-irrational.pdf">hw7-irrational</a><br />
<a href="https://crypto.cs.virginia.edu/13f4102/wp-content/uploads/gold2.jpeg">gold bar</a></p>
