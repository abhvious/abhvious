---
layout: post
status: publish
published: true
title: Sample midterm problems
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 159
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=159
date: '2013-10-16T14:08:38'
date_gmt: '2013-10-16 18:08:38 -0400'
categories: 13f4102
tags: []
comments: []
---
<p><a href="/13f4102/wp-content/uploads/sample-midterm.pdf">Practice problems for the midterm</a></p>
