---
layout: post
status: publish
published: true
title: Video of Oct 1 online office hours
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 137
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=137
date: '2013-10-01 21:35:28 -0400'
date_gmt: '2013-10-02 01:35:28 -0400'
categories:
- Uncategorized
tags: []
comments: []
---
<p>You can watch <a href="https://plus.google.com/115334887111697323553/posts/BaJFvZC2DQy">our short office hours on hangout</a> this evening.  Spoiler alert: no answers were given outright.</p>
