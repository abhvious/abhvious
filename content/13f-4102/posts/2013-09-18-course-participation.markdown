---
layout: post
status: publish
published: true
title: Course Participation
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 98
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=98
date: '2013-09-18T23:03:33'
date_gmt: '2013-09-19 03:03:33 -0400'
categories: 13f4102
tags: []
comments: []
---
<p>Your grade depends on hw, midterm, and class participation.</p>
<p>Your participation score depends on: individual participation and anonymous course-wide participation.  I can monitor both, either via lecture itself, online presence on the blog and piazza, and finally, with polls like the one I am about to send.</p>
<p>I would like to get 95% participation in these polls, but I want to keep them anonymous.  So your course-wide participation component depends only on the final participation rate.  If there is >95% participation *overall*, it means everyone gets 100% for this component; below 95, and the component rapidly degrades via an exponential function.  Thus, your incentive for these random polls should be: answer quickly and encourage others so that everyone benefits.</p>
