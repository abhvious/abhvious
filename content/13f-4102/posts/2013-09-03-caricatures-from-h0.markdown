---
layout: post
status: publish
published: true
title: Caricatures from H0
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 30
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=30
date: '2013-09-03 05:47:36 -0400'
date_gmt: '2013-09-03 09:47:36 -0400'
categories: 13f4102
tags: []
comments: []
---
<p>Bravo class! Also kudos to the high quality selection of literary passages.<br/></p>
<p><a href="https://crypto.cs.virginia.edu/13f4102/wp-content/uploads/2013/09/caricatures.001.jpg"><img src="https://crypto.cs.virginia.edu/13f4102/wp-content/uploads/2013/09/caricatures.001.jpg" alt="caricatures.001" />Click for full size</a></p>
