---
layout: post
status: publish
published: true
title: LaTeX Links
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 5
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=5
date: '2013-08-28 02:09:39 -0400'
date_gmt: '2013-08-28 02:09:39 -0400'
categories:
- Uncategorized
tags: []
comments:
- id: 5
  author: asdf asdf
  author_email: asdf@asdf.asd
  author_url: ''
  date: '2013-09-01 21:56:36 -0400'
  date_gmt: '2013-09-02 01:56:36 -0400'
  content: I just wanted to say that after trying out several LaTeX editors on linux,
    I found that Gummi was the easiest to use (in case people were wondering what
    to use on linux).
---
<p><a href="http://pages.uoregon.edu/koch/texshop/" title="Texshop">TexShop</a> is a great tex editor for the Mac platform.  </p>
<p>For windows, I have heard good feedback about <a href="http://www.texniccenter.org/">TexNiCenter</a>.</p>
<p>The <a href="http://tobi.oetiker.ch/lshort/lshort.pdf">Not so short intro</a> to latex is a good reference.  <a href="http://www.tug.org/twg/mactex/tutorials/ltxprimer-1.0.pdf">This</a> is another tutorial for latex from a user group.</p>
