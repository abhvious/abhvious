---
layout: post
status: publish
published: true
title: Office Hours + Course Calendar
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 50
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=50
date: '2013-09-03 14:23:29 -0400'
date_gmt: '2013-09-03 18:23:29 -0400'
categories: 13f4102
tags: []
comments:
- id: 6
  author: Mark
  author_email: superha1@hotmail.com
  author_url: ''
  date: '2013-09-03 21:34:14 -0400'
  date_gmt: '2013-09-04 01:34:14 -0400'
  content: I cannot find the link to join call. Is there another way to join the hangout?
- id: 7
  author: abhi4102
  author_email: abhi@virginia.edu
  author_url: ''
  date: '2013-09-05 16:05:28 -0400'
  date_gmt: '2013-09-05 20:05:28 -0400'
  content: |-
    From the course calendar, click the "Join video call" link.
    For Thu evening (tonight), you can use <a href="https://plus.google.com/hangouts/_/calendar/dmtyYWRnNWRtMWYzdTJpb2EwOG9rc3Nycm9AZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ.beln6l1nign947f09stgi2q5o8" rel="nofollow">this link</a>.
- id: 9
  author: Anonymous
  author_email: anondld8@gmail.com
  author_url: ''
  date: '2013-09-08 22:06:24 -0400'
  date_gmt: '2013-09-09 02:06:24 -0400'
  content: "Dear Professor Shelat,\r\n\r\nI was wondering if there will be anymore
    office hours other than the ones that appear in the Calendar.  If it is possible,
    I believe that office hours on Monday afternoon or even Sunday would be really
    helpful given that the homeworks are due Wednesday afternoon."
---
<p>I have a course calendar on google<br />
<a href="https://www.google.com/calendar/embed?src=vkradg5dm1f3u2ioa08okssrro%40group.calendar.google.com&ctz=America/New_York">html</a> and <a href="https://www.google.com/calendar/ical/vkradg5dm1f3u2ioa08okssrro%40group.calendar.google.com/public/basic.ics">ical</a>.</p>
<p>I will hold the first Google Hangouts office hours today, Tue at ~9ish.  You can join by going to the calendar, finding the 9pm event, and clicking the "Join call" link.</p>
