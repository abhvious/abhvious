---
layout: post
status: publish
published: true
title: 'L22: Maxflow-mincut'
author: abhi4102
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 205
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=205
date: '2013-11-19 01:02:59 -0500'
date_gmt: '2013-11-19 05:02:59 -0500'
categories: 13f4102 lecture
tags: []
comments: []
---
<p><a href="/13f4102/wp-content/uploads/L22-4102-F13.pdf">Raw L22 notes</a> and <a href="/13f4102/wp-content/uploads/L22-4102-F13-delivered.pdf">Annotated L22 notes</a> (when they are available)</p>
<p>Topic covered: Maxflow-mincut, Ford-Fulkerson, Edmonds-Karp</p>
<p>A video of L22 <a href="/13f4102/wp-content/uploads/L22/index.html">will be here</a> when available.</p>
