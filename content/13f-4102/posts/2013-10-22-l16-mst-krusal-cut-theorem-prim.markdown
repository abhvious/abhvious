---
layout: post
status: publish
published: true
title: 'L16: MST, Krusal, cut theorem, Prim'
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 173
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=173
date: '2013-10-22T15:55:59'
date_gmt: '2013-10-22 19:55:59 -0400'
categories: 13f4102 lecture
tags: []
comments: []
---
<p><a href="/13f4102/wp-content/uploads/L16-4102-F13.pdf">Raw L16 notes</a> and <a href="/13f4102/wp-content/uploads/L16-4102-F13-delivered.pdf">Annotated L16 notes</a></p>
<p>Topic covered: We discussed the Kruskal algorithm, we proved the cut theorem and used it to explain the Kruskal algorithm, we introduced the Prim algorithm.</p>
<p>A video of L16 <a href="/13f4102/wp-content/uploads/L16/index.html">will be here</a> when available.</p>
