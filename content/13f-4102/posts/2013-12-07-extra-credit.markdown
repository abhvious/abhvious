---
layout: post
status: publish
published: true
title: Extra credit
author: abhi4102
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 224
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=224
date: '2013-12-07 15:28:33 -0500'
date_gmt: '2013-12-07 19:28:33 -0500'
categories: 13f4102
tags: []
comments: []
---
<p>Extra credit is due at 10p next Saturday, Dec 14.</p>
<p>The first problem is about arrays is an "interview question" problem; the remaining three problems are programming problems from the ICPC contest that require applications of graph algorithms, or other concepts from the course.  Each of those three problems were solved by undergraduate programmers (who competed in the ICPC) in a few hours.  Nonetheless, the are challenging problems.</p>
<p>Here are <a href="http://icpc.baylor.edu/icpcnews/data/icpc2013-data.tar.gz">sample inputs and outputs</a> that I will use to test your program.</p>
<p>
<a href="https://crypto.cs.virginia.edu/13f4102/wp-content/uploads/extra.pdf">Extra pdf</a> <a href="https://crypto.cs.virginia.edu/13f4102/wp-content/uploads/extra.tex">Extra tex template</a></p>
