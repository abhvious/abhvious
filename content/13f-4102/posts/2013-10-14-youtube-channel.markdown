---
layout: post
status: publish
published: true
title: Youtube channel
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 156
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=156
date: '2013-10-14 07:13:16 -0400'
date_gmt: '2013-10-14 11:13:16 -0400'
categories:
- Uncategorized
tags: []
comments: []
---
<p>Videos of lectures will also be posted to this youtube channel: <a href="http://www.youtube.com/channel/UCxXYk53cSZof2bR_Ax0uJYQ">http://www.youtube.com/channel/UCxXYk53cSZof2bR_Ax0uJYQ</a></p>
<p>A preliminary version of the lecture should be posted almost immediately after lecture, and an edited one will appear later.</p>
