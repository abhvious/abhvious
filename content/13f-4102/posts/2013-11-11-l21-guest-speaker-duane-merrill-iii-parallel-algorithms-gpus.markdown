---
layout: post
status: publish
published: true
title: 'L21: Guest speaker Duane Merrill III, Parallel algorithms & GPUs'
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 198
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=198
date: '2013-11-11 21:18:11 -0500'
date_gmt: '2013-11-12 01:18:11 -0500'
categories: 13f4102 lecture
tags: []
comments: []
---
<p>Duane Merrill III gave a guest lecture today.</p>
<p>He talked about parallel computing using tens of thousands of threads, specifically the efficiency of synchronous cooperation (with barriers) versus the inefficiency of mutual-exclusion (with locks/atomics).  He started with an example of parallel pair-wise reduction versus atomic-add.  He spent the rest of the time talking about algorithms for (a) taking stuff apart (parallel partitioning using prefix sum) and (b) putting it back together (parallel merge).</p>
<p>His lecture was mostly blackboard, but here are some <a href="/13f4102/wp-content/uploads/L21-parallelmerging.pdf">supporting slides</a>.</p>
