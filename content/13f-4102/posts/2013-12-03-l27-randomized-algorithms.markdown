---
layout: post
status: publish
published: true
title: 'L27: Randomized algorithms'
author: abhi4102
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 214
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=214
date: '2013-12-03 15:29:23 -0500'
date_gmt: '2013-12-03 19:29:23 -0500'
categories: 13f4102 lecture
tags: []
comments: []
---
<p><a href="/13f4102/wp-content/uploads/L27-4102-F13.pdf">Raw L27 notes</a> and <a href="/13f4102/wp-content/uploads/L27-4102-F13-delivered.pdf">Annotated L27 notes</a> (when they are available)</p>
<p>Topic covered: Randomized algorithms: the prince and his matches, fingerprinting, string matching, message passing</p>
<p>A video of L27 <a href="/13f4102/wp-content/uploads/L27/index.html">will be here</a> when available.</p>
