---
layout: post
status: publish
published: true
title: Intro
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 1
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=1
date: '2013-08-24 20:24:04 -0400'
date_gmt: '2013-08-24 20:24:04 -0400'
categories:
- Uncategorized
tags: []
comments:
- id: 3
  author: Sam Prestwood
  author_email: swp2sf@virginia.edu
  author_url: ''
  date: '2013-08-30 14:49:36 -0400'
  date_gmt: '2013-08-30 18:49:36 -0400'
  content: Test comment.
---
<p>This is the course site for CS4102, Algorithms, at U. Virginia.  Class is held Tue + Thu in Rice Hall Auditorium.</p>
