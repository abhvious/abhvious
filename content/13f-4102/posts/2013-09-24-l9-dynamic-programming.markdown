---
layout: post
status: publish
published: true
title: L9 - Dynamic Programming
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 112
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=112
date: '2013-09-24T11:48:32'
date_gmt: '2013-09-24 15:48:32 -0400'
categories: 13f4102 lecture
tags: []
comments: []
---
<p><a href="/13f4102/wp-content/uploads/L9-4102-F13.pdf">Raw L9 notes</a> and the <a href="/13f4102/wp-content/uploads/L9-4102-F13-delivered.pdf">annotated L9 notes</a> </p>
<p>Topic covered: Dynamic programming, stairs, the log cutter problem, matrix chain multiplication, typesetting</p>
<p>A <a href="/13f4102/wp-content/uploads/L9/index.html">video of L9</a> is now available.</p>
