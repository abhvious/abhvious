---
layout: post
status: publish
published: true
title: Office hours, Oct 11 1130p, on Hangouts
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 152
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=152
date: '2013-10-11T00:48:45'
date_gmt: '2013-10-11 04:48:45 -0400'
categories: 13f4102 
tags: []
comments: []
---
<p>Here is a link to the archived <a href="https://www.youtube.com/watch?v=X1p6Cp0KLoM">CS4102 office hour session, Oct 11, 11.30p</a>.  First 6 minutes can be safely skipped...</p>
