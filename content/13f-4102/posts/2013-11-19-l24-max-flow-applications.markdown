---
layout: post
status: publish
published: true
title: 'L24: Max flow applications'
author: abhi4102
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 209
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=209
date: '2013-11-19 01:04:08 -0500'
date_gmt: '2013-11-19 05:04:08 -0500'
categories: 13f4102 lecture
tags: []
comments: []
---
<p><a href="/13f4102/wp-content/uploads/L24-4102-F13.pdf">Raw L24 notes</a> and <a href="/13f4102/wp-content/uploads/L24-4102-F13-delivered.pdf">Annotated L24 notes</a> (when they are available)</p>
<p>Topic covered: Maxflow-mincut Edmonds-Karp, applications of maxflow to bipartite matching, edge-disjoint paths, baseball, homework 7</p>
<p>A video of L24 <a href="/13f4102/wp-content/uploads/L24/index.html">will be here</a> when available.</p>
