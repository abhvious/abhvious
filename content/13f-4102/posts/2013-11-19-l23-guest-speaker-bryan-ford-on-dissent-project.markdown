---
layout: post
status: publish
published: true
title: 'L23: Guest speaker, Bryan Ford on Dissent project'
author: abhi4102
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 207
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=207
date: '2013-11-19 01:03:18 -0500'
date_gmt: '2013-11-19 05:03:18 -0500'
categories: 13f4102 lecture
tags: []
comments: []
---
<p>The slides are available on the Dissent home page:  http://dedis.cs.yale.edu/dissent/<br />
See the first bullet under the "Lectures" section in the lower-right; the slides are available in native OpenOffice form (with working animations) or in PDF form (with animations collapsed).</p>
