---
layout: post
status: publish
published: true
title: Office hours
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 7
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=7
date: '2013-08-28 02:11:33 -0400'
date_gmt: '2013-08-28 02:11:33 -0400'
categories:
- Uncategorized
tags: []
comments: []
---
<p>My office hours are Tue,Thu 1.45-3p in my office which is the 5th floor of Rice, #512.  You can also write me to make a different appt if you cannot make that time.  Monday afternoons are generally good for that.</p>
<p>As I learn the schedules from all of the TAs, I will add info about their office hours in this post.</p>
