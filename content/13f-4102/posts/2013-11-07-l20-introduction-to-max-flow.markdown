---
layout: post
status: publish
published: true
title: 'L20: Introduction to Max flow'
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
date: 2013-11-07T08:45:56
categories: 13f4102 lecture
tags: []
comments: []
---
<p>Nathan Brunelle gave the lecture today; here are his slides: <a href="http://www.cs.virginia.edu/~njb2b/F4102-L20.pdf">L20 Slides</a></p>
