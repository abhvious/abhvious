---
layout: post
status: publish
published: true
title: L10 - Dynamic Programming, Matrix chain multiplication and typesetting
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 122
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=122
date: '2013-09-26T11:25:11'
date_gmt: '2013-09-26 15:25:11 -0400'
categories: 13f4102 lecture
tags: []
comments: []
---
<p><a href="/13f4102/wp-content/uploads/L10-4102-F13.pdf">Raw L10 notes</a> and the <a href="/13f4102/wp-content/uploads/L10-4102-F13-delivered.pdf">annotated L10 notes</a> </p>
<p>Topic covered: Dynamic programming: matrix chain multiplication, typesetting</p>
<p>A <a href="/13f4102/wp-content/uploads/L10/index.html">video of L10</a> is now available.</p>
<p>Here is a sample program in java that implements the typesetting algorithm that we discussed in class: <a href="/13f4102/wp-content/uploads/typeset.java">typeset.java</a> and its sample input file <a href="/13f4102/wp-content/uploads/charly">charly</a> (you can use any text input that you like).  You can run it like this:</p>
<p><code>javac typeset.java<br />
java typeset charly 30<br />
0 best: 0 ch 0<br />
1 best: 2116 ch 0<br />
2 best: 1764 ch 0<br />
3 best: 1444 ch 0<br />
...<br />
118 best: 156 ch 109<br />
119 best: 95 ch 112<br />
120 best: 392 ch 114<br />
It was the best of times, it was the worst<br />
of times, it was the age of wisdom, it was<br />
the age of foolishness, it was the epoch of<br />
belief, it was the epoch of incredulity, it<br />
was the season of Light, it was the season<br />
of Darkness, it was the spring of hope, it<br />
was the winter of despair, we had everything<br />
before us, we had nothing before us, we were<br />
all going direct to heaven, we were all going<br />
direct the other way - in short, the period<br />
was so far like the present period, that<br />
some of its noisiest authorities insisted on<br />
its being received, for good or for evil, in<br />
the superlative degree of comparison only.<br />
</code></p>
<p>A good way for you to double check whether you understand this algorithm is to look at the slides again and implement the same typeset program yourself.  If you get stuck, you can get a hint from my version.</p>
