---
layout: post
status: publish
published: true
title: 'L26: Stable Matching'
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 217
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=217
date: '2013-12-03 15:28:05 -0500'
date_gmt: '2013-12-03 19:28:05 -0500'
categories: 13f4102 lecture
tags: []
comments: []
---
<p><a href="/13f4102/wp-content/uploads/L26-4102-F13.pdf">Raw L26 notes</a> and <a href="/13f4102/wp-content/uploads/L26-4102-F13-delivered.pdf">Annotated L26 notes</a> (when they are available)</p>
<p>Topic covered: Stable matching</p>
<p>A video of L26 <a href="/13f4102/wp-content/uploads/L26/index.html">will be here</a> when available.</p>
