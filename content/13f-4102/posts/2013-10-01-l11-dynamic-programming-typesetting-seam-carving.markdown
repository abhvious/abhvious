---
layout: post
status: publish
published: true
title: 'L11: Dynamic Programming, typesetting + seam carving'
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 132
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=132
date: '2013-10-01T20:27:02'
date_gmt: '2013-10-02 00:27:02 -0400'
categories: 13f4102 lecture
tags: []
comments: []
---
<p><a href="/13f4102/wp-content/uploads/L11-4102-F13.pdf">Raw L11 notes</a> and the <a href="/13f4102/wp-content/uploads/L11-4102-F13-delivered.pdf">annotated L11 notes</a> </p>
<p>Topic covered: Dynamic programming: typesetting, intro to seam carving, quickfire challenge was path counting</p>
<p>You can watch the <a href="http://www.youtube.com/watch?v=vIFCV2spKtg">seam carving video</a> that I showed in class.</p>
<p>A <a href="/13f4102/wp-content/uploads/L11/index.html">video of L11</a> is now available.</p>
