---
layout: post
status: publish
published: true
title: L3 - Analyzing Recurrences
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 34
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=34
date: '2013-09-03 14:24:08 -0400'
date_gmt: '2013-09-03 18:24:08 -0400'
categories: 13f4102
tags: []
comments:
- id: 10
  author: Anonymous
  author_email: anondld8@gmail.com
  author_url: ''
  date: '2013-09-08 22:09:00 -0400'
  date_gmt: '2013-09-09 02:09:00 -0400'
  content: "Dear Professor Shelat,\r\n\r\nI do not seem to find any information about
    the other 2 cases from the Master Theorem. Could you please post them whenever
    is convenient for you?"
- id: 11
  author: Anonymous
  author_email: anondld8@gmail.com
  author_url: ''
  date: '2013-09-08 23:33:47 -0400'
  date_gmt: '2013-09-09 03:33:47 -0400'
  content: "Dear Professor Shelat,\r\nCould you please check the lecture slides for
    L3 whenever is convenient for you? It seems like it is the same one as L2."
- id: 12
  author: Anonymous
  author_email: Anonymous@anonymous.com
  author_url: ''
  date: '2013-09-09 19:02:17 -0400'
  date_gmt: '2013-09-09 23:02:17 -0400'
  content: Could you please put up lecture 3 and 4? Thanks for going to the trouble
    of doing this for us!
---
<p><a href="/13f4102/wp-content/uploads/L3-4102-F13.pdf">Raw L3 slides in PDF</a>&nbsp;|&nbsp;<a href="/13f4102/wp-content/uploads/L3-4102-F13-delivered.pdf">Annotated L3 slides in PDF</a></p>
<p>The <a href="https://crypto.cs.virginia.edu/13f4102/wp-content/uploads/l3/index.html">video cast for L3</a> is now available.  You can skip thru the parts of this lecture using the "Chapters" button on the right.  Please send me feedback if this was helpful.</p>
