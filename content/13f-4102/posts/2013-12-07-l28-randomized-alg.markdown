---
layout: post
status: publish
published: true
title: 'L28: Randomized alg'
author: abhi4102
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 222
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=222
date: '2013-12-07 15:26:04 -0500'
date_gmt: '2013-12-07 19:26:04 -0500'
categories: 13f4102 lecture
tags: []
comments: []
---
<p><a href="/13f4102/wp-content/uploads/L28-4102-F13.pdf">Raw L28 notes</a> and <a href="/13f4102/wp-content/uploads/L28-4102-F13-delivered.pdf">Annotated L28 notes</a> (when they are available)</p>
<p>Topic covered: Randomized algorithms: message passing, median, RSA encryption</p>
<p>A video of L28 <a href="/13f4102/wp-content/uploads/L27/index.html">will be here</a> when available.</p>
