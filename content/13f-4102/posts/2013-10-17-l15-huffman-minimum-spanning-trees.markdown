---
layout: post
status: publish
published: true
title: 'L15: Huffman, Minimum spanning trees, midterm'
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 165
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=165
date: "2013-10-17T11:52:51"
categories: 13f4102 lecture
tags: []
---
<p><a href="/13f4102/wp-content/uploads/L15-4102-F13.pdf">Raw L15 notes</a></p>
<p>Topic covered: We finished the Huffman coding problem, and we started the minimum spanning tree problem.  The <a href="/13f4102/wp-content/uploads/midterm-4102-F13.pdf">midterm</a> was handed out in class.</p>
<p>A video of L15 <a href="/13f4102/wp-content/uploads/L15/index.html">will be here</a> when available.</p>
<p><a href="/13f4102/wp-content/uploads/midterm13.tex">tex template for the midterm</a> </p>
<h3>Midterm corrections</h3>
<ol>
<li>In problem 4, "one mashup of the loops z=drmdrmdr and w=fsfslfs could be dfsrmdrmdfrmsldfsr" the string z should be "z=drmdrmdrmdr" in order to produce the final string properly.</li>
<li>In that same example, it happens that z and w have disjoint notes, but your solution cannot rely on this property.  You should assume that the snippets x and y given as input may contain common notes.
<li>Deadline is Wednesday noon.
</ol>
