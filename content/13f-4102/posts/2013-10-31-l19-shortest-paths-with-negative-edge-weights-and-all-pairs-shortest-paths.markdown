---
layout: post
status: publish
published: true
title: 'L19: Shortest paths with negative edge weights, and All-pairs shortest paths'
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 188
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=188
date: '2013-10-31T12:22:35'
date_gmt: '2013-10-31 16:22:35 -0400'
categories: 13f4102 lecture
tags: []
comments: []
---
<p><a href="/13f4102/wp-content/uploads/L19-4102-F13.pdf">Raw L19 notes</a> and <a href="/13f4102/wp-content/uploads/L19-4102-F13-delivered.pdf">Annotated L19 notes</a> (when they are available)</p>
<p>Topic covered: We discussed shortest path algorithms, both when edge weights are negative:<br />
 Bellman-Ford, all pairs shortest paths, and the introduction to max flow.</p>
<p>A video of L19 <a href="/13f4102/wp-content/uploads/L19/index.html">will be here</a> when available.</p>
