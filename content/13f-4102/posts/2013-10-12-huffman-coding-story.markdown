---
layout: post
status: publish
published: true
title: Huffman coding story
author:
  display_name: abhi4102
  login: abhi4102
  email: abhi@virginia.edu
  url: ''
author_login: abhi4102
author_email: abhi@virginia.edu
wordpress_id: 154
wordpress_url: https://crypto.cs.virginia.edu/13f4102/?p=154
date: '2013-10-12T04:31:00'
date_gmt: '2013-10-12 08:31:00 -0400'
categories: 13f4102
tags: []
---
<p>According to Wiki, my story about Huffman's homework solution is wrong, and I don't remember exactly where I picked up the that anecdote.  Anyway, Wiki claims that </p>
<blockquote><p>"In 1951, David A. Huffman and his MIT information theory classmates were given the choice of a term paper or a final exam. The professor, Robert M. Fano, assigned a term paper on the problem of finding the most efficient binary code. Huffman, unable to prove any codes were the most efficient, was about to give up and start studying for the final when he hit upon the idea of using a frequency-sorted binary tree and quickly proved this method the most efficient.[2]<br />
In doing so, the student outdid his professor, who had worked with information theory inventor Claude Shannon to develop a similar code. By building the tree from the bottom up instead of the top down, Huffman avoided the major flaw of the suboptimal Shannon-Fano coding.</p></blockquote>
<p>So the coding problem was indeed an open problem that was assigned by the professor, but DH clearly knew it was an open problem and not just homework at the time...</p>
