---
title:  "Lecture 20: All-pairs Shortest paths, Maxflow"
date:   "2016-03-02T15:30:01-05:00"
categories: teaching
draft: false
---


[L20 Slides](/~shelat/dl/16s4102/L20-4102-S16-delivered.pdf)

We discussed the all-pairs shortest path algorithm, and then introduced the max-flow problem.


<div itemprop="video" itemscope itemtype="http://schema.org/VideoObject">
<meta itemprop="duration" content="T27M55S" />
  <meta itemprop="thumbnailUrl" content="/~shelat/dl/16s4102/L20-4102-S16.001.jpeg" />
  <meta itemprop="contentURL" content="/~shelat/16s4102/L20" />
  <meta itemprop="uploadDate" content="2016-02-20T08:00:00+08:00" />
  <meta itemprop="height" content="720" />
  <meta itemprop="width" content="1280" />
  <meta itemprop="description" content="All pairs shortest paths and Maxflow introduction" />
  <meta itemprop="name" content="Lecture 20 CS4102 Sprint 2016 UVA" />

<video id="player" class="video-js"
  controls preload="auto" width="640" height="360"
  poster="/~shelat/dl/16s4102/L20-4102-S16.001.jpeg"
  data-setup='{ "playbackRates": [1, 1.25, 1.5, 1.718] }'>>
  <source src="/~shelat/dl/16s4102/L20-4102-16s.mp4" type='video/mp4' />
  <source src="/~shelat/dl/16s4102/L20-4102-16s.webm" type='video/webm' />
 <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
</video>
</div>
