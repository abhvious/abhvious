---
type: post
title:  "Lecture 1: Intro"
date:   "2016-01-21T01:01:01-05:00"
categories: teaching
---

{% highlight tex %}
{% endhighlight %} 

[L1 Slides PDF](/~shelat/dl/16s4102/L1-4102-S16.pdf)

The video for the lecture does not have sound.

The lecture discussed the goals, format, and expectations of the class.  Towards the end, we ran a parallel algorithm to count the number of students in the lecture (and also find the youngest student).  In L2, we will focus on analyzing this algorithm and that will lead us to studying recurrences.