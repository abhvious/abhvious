---
title:  "Lecture 25: Linear Programming, Reductions"
date:   "2016-03-02T15:30:01-05:00"
categories: teaching
draft: false
---


[L25 Slides](/~shelat/dl/16s4102/L25-4102-S16-delivered.pdf)

We finished our discussion of linear programming and introduced the idea of reductions between
problems.  This lecture, we reviewed what is expected for the reductions that you need to create to
solve the homework problems.  We then introduced the independent set problem and the vertex cover problem; in the next lecture we will show these problems to be equivalent.


<div itemprop="video" itemscope itemtype="http://schema.org/VideoObject">
<meta itemprop="duration" content="T20M09S" />
  <meta itemprop="thumbnailUrl" content="/~shelat/dl/16s4102/L25-4102-S16.001.jpeg" />
  <meta itemprop="contentURL" content="/~shelat/16s4102/L25" />
  <meta itemprop="uploadDate" content="2016-02-20T08:00:00+08:00" />
  <meta itemprop="height" content="720" />
  <meta itemprop="width" content="1280" />
  <meta itemprop="description" content="Reductions, independent set, vertex cover" />
  <meta itemprop="name" content="Lecture 25 CS4102 Sprint 2016 UVA" />

<video id="player" class="video-js"
  controls preload="auto" width="640" height="360"
  poster="/~shelat/dl/16s4102/L25-4102-S16.001.jpeg"
  data-setup='{ "playbackRates": [1, 1.25, 1.5, 1.718] }'>>
  <source src="/~shelat/dl/16s4102/L25-4102-16s.mp4" type='video/mp4' />
  <source src="/~shelat/dl/16s4102/L25-4102-16s.webm" type='video/webm' />
 <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
</video>
</div>
