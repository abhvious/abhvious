+++
date = "2016-01-30T20:53:21-05:00"
title = "Index"
type = "index"
+++

<div class="home">
  <p>I am an associate professor of computer science; I have recently (summer'16) moved from the University of Virginia to <a href="https://www.ccs.neu.edu">Northeastern University</a>.  I work in computer security and cryptography with the aim of developing novel systems that incorporate the state-of-the-art security techniques.   Some of my work focuses on building secure computation protocols, some of my work focuses on building systems that ensure both anonymity and accountability. Some of my work also studies the complexity of encryption schemes that are suitable for the internet.  My email is abhi @ neu.edu.
  </p>

  <h1 class="page-heading">Research</h1>
  <p>My research is supported by the National Science Foundation, DARPA, the Microsoft Faculty Fellowship Award (2010), the FEST fellowship award (2010), an Amazon Research award, an SAIC research award, a Jacobs Future of Money Workshop research prize (2014), and the Google Faculty Research Award (2015).</p>

  
  <p>See <a href="https://anonize.org">anonize.org</a> for our implementation of the ANONIZE scheme.  The implementation was used to run course reviews at CornellTech during the Spring semester.
  Here is a <a href="http://www.wired.com/2015/09/new-crypto-tool-makes-anonymous-surveys-truly-anonymous/">Wired story</a> about the scheme.
   Contact me if you are interested in using it.</p>


  <h1 class="page-heading">Students</h1>
  <p> My PhD students include Mona Sergi (2013, now at Google), Chih-hao Shen (2014, now at Google), Ben Kreuter (now at Google), Ben Terner (Masters 2015, going to UCSD to complete PhD).</p>

</div>
