---
layout: post
title:  "Approximating the Smallest Grammar: Kolmogorov Complexity in Natural Models"
authors: "Moses Charikar, Eric Lehman, Ding Liu, Rina Panigrahy, Manoj Prabhakaran, April Rasala, Amit Sahai, abhi shelat"
conf: "STOC 2002"
date:   2002-05-01T01:01:01
categories: research
---

{{< highlight html >}}
{{< /highlight >}} 

Approximating the Smallest Grammar: Kolmogorov Complexity in Natural Models
Moses Charikar, Eric Lehman, Ding Liu, Rina Panigrahy, Manoj Prabhakaran, April Rasala, Amit Sahai, abhi shelat
STOC 2002

