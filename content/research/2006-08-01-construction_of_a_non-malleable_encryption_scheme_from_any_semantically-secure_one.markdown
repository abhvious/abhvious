---
layout: post
title:  "Construction of a Non-malleable Encryption Scheme from Any Semantically-Secure One"
authors: "Rafael Pass, abhi shelat, and Vinod Vaikuntanathan"
conf: "CRYPTO’06, Santa Barbara, CA, Aug 2006, p.271-289."
date:   2006-08-01T01:01:01
categories: research
---

{{< highlight html >}}
{{< /highlight >}} 

Construction of a Non-malleable Encryption Scheme from Any Semantically-Secure One
Rafael Pass, abhi shelat, and Vinod Vaikuntanathan
CRYPTO’06, Santa Barbara, CA, Aug 2006, p.271-289.

