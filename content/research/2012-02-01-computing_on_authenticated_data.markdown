---
layout: post
title:  "Computing on Authenticated Data"
authors: "J.Ahn, D.Boneh, J.Camenisch, S.Hohenberger, a.shelat, and B.Waters"
conf: "Theory of Cryptography Conference 2012"
date:   2012-02-01T01:01:01
categories: research
---

{{< highlight html >}}
{{< /highlight >}} 

Computing on Authenticated Data
J.Ahn, D.Boneh, J.Camenisch, S.Hohenberger, a.shelat, and B.Waters
Theory of Cryptography Conference 2012

