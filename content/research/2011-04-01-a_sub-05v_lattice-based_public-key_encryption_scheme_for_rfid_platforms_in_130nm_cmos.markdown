---
layout: post
title:  "A Sub-0.5V Lattice-Based Public-Key Encryption Scheme for RFID Platforms in 130nm CMOS"
authors: "Yu Yao, Sudhanshu Khanna, Ben Calhoun, Dave Evans, John Lach, and a. shelat"
conf: "RFIDSEC’11 Asia"
date:   2011-04-01T01:01:01
categories: research
---

{{< highlight html >}}
{{< /highlight >}} 

A Sub-0.5V Lattice-Based Public-Key Encryption Scheme for RFID Platforms in 130nm CMOS
Yu Yao, Sudhanshu Khanna, Ben Calhoun, Dave Evans, John Lach, and a. shelat
RFIDSEC’11 Asia

