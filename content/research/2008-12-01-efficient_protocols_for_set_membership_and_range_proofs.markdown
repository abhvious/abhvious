---
layout: post
title:  "Efficient Protocols for Set Membership and Range Proofs"
authors: "Jan Camenisch, Raﬁk Chaabouni, and abhi shelat"
conf: "ASIACRYPT 2008"
date:   2008-12-01T01:01:01
categories: research
---

{{< highlight html >}}
{{< /highlight >}} 

Efficient Protocols for Set Membership and Range Proofs
Jan Camenisch, Raﬁk Chaabouni, and abhi shelat
ASIACRYPT 2008

