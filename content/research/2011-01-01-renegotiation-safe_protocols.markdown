---
layout: post
title:  "Renegotiation-safe Protocols"
authors: "Rafael Pass and a. shelat"
conf: "Innovations in Computer Science ’11 (ICS’11)"
date:   2011-01-01T01:01:01
categories: research
---

{{< highlight html >}}
@inproceedings{PS11,
	title="Renegotiation-safe Protocols",
	author="Rafael Pass and Abhi Shelat",
	year="2011",
	booktitle="Innovations in Computer Science ’11 (ICS’11)"
}
{{< /highlight >}} 



