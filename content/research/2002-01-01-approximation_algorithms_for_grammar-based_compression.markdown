---
title:  "Approximation Algorithms for Grammar-Based Compression"
authors: "Eric Lehman and abhi shelat"
conf: "SODA 2002"
date:   "2002-01-01T01:01:01"
categories: research
---


Approximation Algorithms for Grammar-Based Compression
Eric Lehman and abhi shelat
SODA 2002

[PDF](/~shelat/dl/LehmanShelat-SODA02.pdf)