---
layout: post
title:  "Cryptography from Sunspots: How to use an Imperfect Reference String"
authors: "Ran Canetti, Rafael Pass, and abhi shelat"
conf: "Foundations of Computer Science (FOCS’07), Providence, Rhode Island, October 2007."
date:   2007-10-01T01:01:01
categories: research
---

{{< highlight html >}}
Cryptography from Sunspots: How to use an Imperfect Reference String
Ran Canetti, Rafael Pass, and abhi shelat
Foundations of Computer Science (FOCS’07)
Providence, Rhode Island, October 2007.
{{< /highlight >}} 

[PDF](/~shelat/dl/focs07-proc.pdf)

