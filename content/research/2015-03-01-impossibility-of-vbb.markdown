---
layout: post
title:  "Impossibility of VBB Obfuscation with Ideal Constant-Degree Graded Encodings"
authors: "Rafael Pass and abhi shelat"
conf: "TCC 2016"
date:   2015-10-01T01:01:01
categories: research
---


{{< highlight html >}}
@misc{PS15a,
 author = {Rafael Pass and abhi shelat},
 title = {Impossibility of VBB Obfuscation with Ideal Constant-Degree Graded Encodings},
 booktitle = {To Appear, TCC 2016, and eprint.iacr.org/2015/383},
 year = {2015},
}
{{< /highlight >}} 

[PDF](https://eprint.iacr.org/2015/383)

A celebrated result by Barak et al (JACM'12) shows the imposibility of general-purpose
*virtual black-box (VBB)* obfuscation in the plain model.
A recent work by Canetti, Kalai, and Paneth (TCC'15) extends this result
also to the random oracle model (assuming trapdoor permutations).

In contrast, Brakerski-Rothblum (TCC'15) and Barak et
al (EuroCrypt'14) show that in *idealized* graded encoding models,
general-purpose VBB obfuscation indeed is possible; these construction
require graded encoding schemes that enable evaluating *high-degree*
(polynomial in size of the circuit to be obfuscated) polynomials on
encodings.

We show a complementary impossibility of general-purpose VBB obfuscation 
in idealized graded encoding models that enable only evaluation of 
*constant-degree* polynomials (assuming  trapdoor permutations).