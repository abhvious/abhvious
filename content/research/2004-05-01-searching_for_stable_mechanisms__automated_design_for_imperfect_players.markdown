---
layout: post
title:  "Searching for Stable Mechanisms : Automated Design for Imperfect Players"
authors: "Andrew Blumberg and abhi shelat"
conf: "AAAI 2004"
date:   "2004-05-01T01:01:01"
categories: research
---

{{< highlight html >}}
{{< /highlight >}} 

Searching for Stable Mechanisms : Automated Design for Imperfect Players
Andrew Blumberg and abhi shelat
AAAI 2004

