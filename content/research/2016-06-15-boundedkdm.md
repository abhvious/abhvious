---
title:  Bounded KDM Security from iO and OWF
authors: Antonio Marcedone and Rafael Pass and abhi shelat
conf:  SCN'16
date:   2016-06-15T01:01:01
categories: research
---


{{< highlight html >}}
@inproceedings{MPS16,
 title = {Bounded KDM Security from iO and OWF},
 author = {Antonio Marcedone and Rafael Pass and abhi shelat},
 booktitle = {Secure Communication Networks (SCN'16)},
 year = {2016},
}
{{< /highlight >}}


To date, all constructions in the standard model (i.e., without random oracles) of Bounded Key-Dependent Message (KDM) secure (or even just circularly-secure) encryption schemes rely on specific assumptions (LWE, DDH, QR or DCR); all of these assumptions are known to imply the existence of collision-resistant hash functions. In this work, we demonstrate the existence of bounded KDM secure encryption assuming indistinguishability obfsucation for $P/poly$ and just one-way functions. Relying on the recent result of Asharov and Segev (STOC'15), this yields the first construction of a Bounded KDM secure (or even circularly secure) encryption scheme from an assumption that provably does not imply collision-resistant hash functions w.r.t. black-box constructions.  Combining this with prior constructions, we show how to augment this Bounded KDM scheme into a Bounded CCA2-KDM scheme.










