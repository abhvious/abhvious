---
title:  "How to Use SNARKs in Universally Composable Protocols"
authors: "Ahmed Kosba, Zhichao Zhao,rew Miller, Hubert Chan, Charalampos Papamanthou, Rafael Pass, abhi shelat and Elaine Shi"
conf: "eprint/2015/1093"
date:   2015-11-02T01:01:01
categories: research
---


{{< highlight html >}}
@inproceedings{KZMCPPSS15,
 title = {How to Use SNARKs in Universally Composable Protocols},
 author = {Ahmed Kosba and Zhichao Zhao and Andrew Miller and Hubert Chan and Charalampos Papamanthou and Rafael Pass and abhi shelat and Elaine Shi},
 booktitle = {eprint/2015/1093},
 year = {2015},
}
{{< /highlight >}}


[PDF](https://eprint.iacr.org/2015/1093)

The past several years have seen tremendous advances in practical, general-purpose, non-interactive proof systems called SNARKs. These building blocks are efficient and convenient, with multiple publicly available implementations, including tools to compile high-level code (e.g., written in C) to arithmetic circuits, the native representation used by SNARK constructions. However, while we would like to use these primitives in UC-secure protocols --- which are provably-secure even when composed with other arbitrary concurrently-executing protocols --- the SNARK definition is not directly compatible with this framework, due to its use of non black-box knowledge extraction. We show several constructions to transform SNARKs into UC-secure NIZKs, along with benchmarks and an end-to-end application example showing that the added overhead is tolerable. Our constructions rely on embedding cryptographic algorithms into the SNARK proof system. Ordinarily, cryptographic constructions are chosen and tuned for implementation on CPUs or in hardware, not as arithmetic circuits. We therefore also initiate the study of SNARK-friendly cryptography, describing several protocol parameterizations, implementations, and performance comparisons for encryption, commitments, and other tasks. This is also of independent interest for use in other SNARK-based applications.







