---
layout: post
title:  "The Smallest Grammar Problem"
authors: "Moses Charikar, Eric Lehman, Ding Liu, Rina Panigrahy, Manoj Prabhakaran, Amit Sahai, abhi shelat"
conf: "IEEE Transactions on Information Theory, Vol. 51, Issue 7, Jul 2005, p2554-2576."
date:   2005-07-01T01:01:01
categories: research
---

{{< highlight html >}}
{{< /highlight >}} 

The Smallest Grammar Problem
Moses Charikar, Eric Lehman, Ding Liu, Rina Panigrahy, Manoj Prabhakaran, Amit Sahai, abhi shelat
IEEE Transactions on Information Theory, Vol. 51, Issue 7, Jul 2005, p2554-2576.

[PDF](/~shelat/dl/GrammarIEEE.pdf)