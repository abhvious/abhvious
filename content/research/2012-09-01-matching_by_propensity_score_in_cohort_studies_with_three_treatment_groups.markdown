---
layout: post
title:  "Matching by propensity score in cohort studies with three treatment groups"
authors: "J. A. Rassen, a. shelat, J. Myers, R.J. Glynn, and S. Schneeweiss"
conf: "Epidemiology, in press 2012"
date:   2012-09-01T01:01:01
categories: research
---

{{< highlight html >}}
{{< /highlight >}} 

Matching by propensity score in cohort studies with three treatment groups
J. A. Rassen, a. shelat, J. Myers, R.J. Glynn, and S. Schneeweiss
Epidemiology, in press 2012

