---
layout: post
title:  "Micropayments for Peer-to-peer currencies"
authors: "Rafael Pass and abhi shelat"
conf: "ACM Conf on Computer Security (CCS) 2015"
date:   2015-07-25T01:01:01
categories: research
---


{{< highlight html >}}
@inproceedings{RS15b,
 author = {Rafael Pass and abhi shelat},
 title = {Micropayments for peer-to-peer currencies},
 booktitle = {CCS'2015},
 year = {2015},
}
{{< /highlight >}} 

[PDF](/~shelat/dl/micropay2.pdf)

All electronic financial transactions in the US, even those enabled by Bitcoin, have relatively high transaction costs. As a result, it becomes infeasible to run \emph{micropayments}, i.e. payments that are on the order of pennies or even fractions of a penny.

In order to circumvent the cost of recording all transactions, Wheeler (1996) and Rivest (1997) suggested the notion of a probabilistic payment, that is, one implements payments that have \emph{expected} value on the order of micro pennies by running an appropriately biased lottery for a larger payment. While there have been quite a few proposed solutions to such lottery-based micropayment schemes, all these solutions rely on a trusted third party to coordinate the transactions; furthermore, to implement these systems in today's economy would require a a global change to how either banks or electronic payment companies (e.g., Visa and Mastercard) handle transactions. 

We put forth a new lottery-based micropayment scheme for Bitcoins, and more generally any ledger-based transaction system, that can be used today without any change to the current infrastructure.


<h3>Graphs from the paper</h3>
<p>A diagram of the messages involved in the 2nd version of our protocol.</p>
<img src="/~shelat/dl/m2-diagram.png" width="400px">

<p>92% of the transactions have a fee between 0.0001 and 0.0005 bitcoin</p>
<img src="/~shelat/dl/cumu.png" width="400px">



<p>The transaction fee is purportedly related to the size of a transaction and the age of the ``from" account.</p>
<img src="/~shelat/dl/fees.png" width="400px">

 
