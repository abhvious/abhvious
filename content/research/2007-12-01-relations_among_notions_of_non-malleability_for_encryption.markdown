---
layout: post
title:  "Relations Among Notions of Non-Malleability for Encryption"
authors: "Rafael Pass, abhi shelat, and Vinod Vaikuntanathan."
conf: "ASIACRYPT’07, December 2007, Kuching, Malaysia."
date:   2007-12-01T01:01:01
categories: research
---

{{< highlight html >}}
{{< /highlight >}} 

Relations Among Notions of Non-Malleability for Encryption
Rafael Pass, abhi shelat, and Vinod Vaikuntanathan.
ASIACRYPT’07, December 2007, Kuching, Malaysia.

