---
layout: post
title:  "An Overview of ANONIZE: A Large scale anonymous survey system"
authors: "S. Hohenberger, S. Myers, R. Pass, and a. shelat"
conf: "IEEE Security & Privacy Journal, Vol 13, No 2, Mar 2015."
date:   2015-03-01T01:01:01
categories: research
---


{{< highlight html >}}
@article{HMPS15,
	title="An Overview of ANONIZE: A Large-Scale Anonymous Survey System",
	authors="Susan Hohenberger and Steven Myers and Rafael Pass and abhi shelat",
	booktitle="IEEE Security & Privacy Magazine",
	number=2,
	month="Mar--Apr",
	volume=13,
	year=2015,
	pages="22--29"
}
{{< /highlight >}} 


[IEEE website](https://www.computer.org/csdl/mags/sp/2015/02/msp2015020022-abs.html)


This is a survey version of our ANONIZE paper for the IEEE Security and Privacy magazine that contains a non-technical overview.  We have also posted a full, technical version of the paper on [eprint](https://eprint.iacr.org/2015/681).  See [this](/~shelat/research/2014/03/31/anonize.html) entry for a description of the technical work.


 
