---
layout: post
title:  "Securely Obfuscating Re-encryption"
authors: "Susan Hohenberger, Guy Rothblum, abhi shelat, and Vinod Vaikuntanathan"
conf: "Theory of Cryptography Conference (TCC’07), Amsterdam, The Netherlands, Feb 2007, p. 233-252."
date:   2007-02-01T01:01:01
categories: research
---

{{< highlight html >}}
@inproceedings{HRSV07,
	Author = {S. Hohenberger and G. Rothblum and Abhi Shelat and V. Vaikuntanathan},
	Booktitle = {Theory of Cryptography Conference (TCC)},
	Title = {Securely Obfuscating Re-Encryption},
	Year = {2007},
	pages= {233--252}
}
{{< /highlight >}} 

[PDF](/dl/HRSV07.pdf)