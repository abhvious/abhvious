---
layout: post
title:  "Efficient Fork-Linearizable Access to Untrusted Shared Memory"
authors: "Christian Cachin, abhi shelat, and Alex Shraer"
conf: "Principles of Distributed Computing (PODC’07), Portland, Oregan, Aug 2007."
date:   2007-07-01T01:01:01
categories: research
---

{{< highlight html >}}
{{< /highlight >}} 

Efficient Fork-Linearizable Access to Untrusted Shared Memory
Christian Cachin, abhi shelat, and Alex Shraer
Principles of Distributed Computing (PODC’07), Portland, Oregan, Aug 2007.

