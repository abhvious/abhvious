---
layout: post
title:  "Unconditional Characterizations of Non-Interactive Zero-Knowledge"
authors: "Rafael pass and abhi shelat"
conf: "CRYPTO’05, Santa Barbara, CA, Aug 2005, p.118-134."
date:   2005-08-01T01:01:01
categories: research
---

{{< highlight html >}}
{{< /highlight >}} 

Unconditional Characterizations of Non-Interactive Zero-Knowledge
Rafael pass and abhi shelat
CRYPTO’05, Santa Barbara, CA, Aug 2005, p.118-134.

