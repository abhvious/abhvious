---
layout: post
title:  "Simulatable Adaptive Oblivious Transfer"
authors: "Jan Camenisch, Gregory Neven, and abhi shelat"
conf: "EUROCRYPT 2007, Barcelona, Spain, May 2007. p.573-590."
date:   2007-05-01T01:01:01
categories: research
---

{{< highlight html >}}
Simulatable Adaptive Oblivious Transfer
Jan Camenisch, Gregory Neven, and abhi shelat
EUROCRYPT 2007, Barcelona, Spain, May 2007. p.573-590.
{{< /highlight >}} 

[PDF](/~shelat/dl/CNS07.pdf)

