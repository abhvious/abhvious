---
layout: post
title:  "Lower Bounds for Collusion-Secure Fingerprinting"
authors: "Chris Peikert, abhi shelat, and Adam Smith"
conf: "SODA 2003"
date:   2003-01-01T01:01:01
categories: research
---

{{< highlight html >}}
{{< /highlight >}} 

Lower Bounds for Collusion-Secure Fingerprinting
Chris Peikert, abhi shelat, and Adam Smith
SODA 2003

