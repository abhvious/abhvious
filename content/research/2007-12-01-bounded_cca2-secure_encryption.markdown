---
layout: post
title:  "Bounded CCA2-Secure Encryption"
authors: "Ronald Cramer, Goichiro Hanaoka, Dennis Hofheinz, Hideki Imai, Eike Kiltz, Rafael Pass, abhi shelat, and Vinod Vaikuntanathan."
conf: "ASIACRYPT’07, December 2007, Kuching, Malaysia."
date:   2007-12-01T01:01:01
categories: research
---

{{< highlight html >}}
{{< /highlight >}} 

Bounded CCA2-Secure Encryption
Ronald Cramer, Goichiro Hanaoka, Dennis Hofheinz, Hideki Imai, Eike Kiltz, Rafael Pass, abhi shelat, and Vinod Vaikuntanathan.
ASIACRYPT’07, December 2007, Kuching, Malaysia.

