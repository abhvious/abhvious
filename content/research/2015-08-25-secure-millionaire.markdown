---
layout: post
title:  "Secure Computation from Millionaire"
authors: "abhi shelat and Muthu Venkitasubramaniam"
conf: "Asiacrypt 2015"
date:   2015-08-16T01:01:01
categories: research
---


{{< highlight html >}}
@inproceedings{SV15,
 title = {Secure Computation from Millionaire},
 author = {abhi shelat and Muthu Venkitasubramaniam},
 booktitle = {Asiacrypt'2015},
 year = {2015},
}
{{< /highlight >}}


The standard method for designing a secure computation protocol for function $f$ first transforms $f$ into either a circuit or a RAM program and then applies a generic secure computation protocol that either handles boolean gates or translates the RAM program into oblivious RAM instructions.

In this paper, we show a large class of functions for which a different *iterative* approach to secure computation results in more efficient protocols.  The first such examples of this technique was presented by Aggrawal, Mishra, and Pinkas (J. of Cryptology, 2010) for computing the median; later, Brickell and Shmatikov (Asiacrypt 2005) showed a similar technique for shortest path problems.

We generalize the technique in both of those works and show that it applies to a large class of problems including certain matroid optimizations, sub-modular optimization, convex hulls, and other scheduling problems.  The crux of our technique is to *securely reduce* these problems to secure comparison operations and to employ the idea of *gradually releasing* part of the output.  We then identify  conditions under which both of these techniques for protocol design are compatible with achieving simulation-based security  in the honest-but-curious and covert adversary models.  In special cases such as median, we also show how to achieve malicious security.
