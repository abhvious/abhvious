---
layout: post
title:  "Optimistic Concurrent Zero Knowledge"
authors: "Alon Rosen and a. shelat"
conf: "Asiacrypt ’10"
date:   2010-12-01T01:01:01
categories: research
---

{{< highlight html >}}
Optimistic Concurrent Zero Knowledge
Alon Rosen and a. shelat
Asiacrypt ’10
{{< /highlight >}} 


[PDF](/~shelat/dl/rs10-optcurr.pdf)
