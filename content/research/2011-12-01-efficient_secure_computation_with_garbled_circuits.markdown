---
layout: post
title:  "Efficient Secure Computation with Garbled Circuits"
authors: "Y. Huang, C.~Shen, D.~Evans, J.~Katz, and a.~shelat"
conf: "Information systems security 2011"
date:   2011-12-01T01:01:01
categories: research
---

{{< highlight html >}}
{{< /highlight >}} 

Efficient Secure Computation with Garbled Circuits
Y. Huang, C.~Shen, D.~Evans, J.~Katz, and a.~shelat
Information systems security 2011

