---
layout: post
title:  "Collusion-Free Protocols in the Mediated Model"
authors: "Joel Alwen, abhi shelat, and Ivan Visconti"
conf: "CRYPTO 2008"
date:   "2008-08-01T01:01:01"
categories: research
---

{{< highlight html >}}
{{< /highlight >}} 

Collusion-Free Protocols in the Mediated Model
Joel Alwen, abhi shelat, and Ivan Visconti
CRYPTO 2008

