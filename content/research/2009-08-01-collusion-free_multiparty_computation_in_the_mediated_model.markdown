---
layout: post
title:  "Collusion-Free Multiparty Computation in the Mediated Model"
authors: "J. Alwen, J. Katz, Y. Lindell, G. Persiano, a. shelat, and I. Visconti."
conf: "CRYPTO 2009. Santa Barbara, CA."
date:   2009-08-01T01:01:01
categories: research
---

{{< highlight html >}}
{{< /highlight >}} 

Collusion-Free Multiparty Computation in the Mediated Model
J. Alwen, J. Katz, Y. Lindell, G. Persiano, a. shelat, and I. Visconti.
CRYPTO 2009. Santa Barbara, CA.

