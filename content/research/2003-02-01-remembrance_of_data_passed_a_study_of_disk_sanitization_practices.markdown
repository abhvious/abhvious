---
layout: post
title:  "Remembrance of Data Passed: A Study of Disk Sanitization Practices"
authors: "Simson Garfinkel and abhi shelat"
conf: "IEEE Security and Privacy, January/February 2003"
date:   2003-02-01T01:01:01
categories: research
---

{{< highlight html >}}
{{< /highlight >}} 

Remembrance of Data Passed: A Study of Disk Sanitization Practices
Simson Garfinkel and abhi shelat
IEEE Security and Privacy, January/February 2003

