---
layout: post
title:  "Fair Zero Knowledge"
authors: "Matt Lepinski and Silvio Micali and abhi shelat"
conf: "Theory of Cryptography (TCC’05), Cambridge, MA, Feb 2005, p.245-263."
date:   2005-02-01T01:01:01
categories: research
---

{{< highlight html >}}
{{< /highlight >}} 

Fair Zero Knowledge
Matt Lepinski and Silvio Micali and abhi shelat
Theory of Cryptography (TCC’05), Cambridge, MA, Feb 2005, p.245-263.

