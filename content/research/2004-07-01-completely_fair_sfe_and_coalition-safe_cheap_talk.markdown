---
layout: post
title:  "Completely Fair SFE and Coalition-Safe Cheap Talk"
authors: "Matt Lepinski and Silvio Micali and Chris Peikert and abhi shelat"
conf: "Principles of Distributed Computing (PODC’04), St. John’s Newfoundland, Jul 2004, p.1-10."
date:   2004-07-01T01:01:01
categories: research
---

{{< highlight html >}}
{{< /highlight >}} 

Completely Fair SFE and Coalition-Safe Cheap Talk
Matt Lepinski and Silvio Micali and Chris Peikert and abhi shelat
Principles of Distributed Computing (PODC’04), St. John’s Newfoundland, Jul 2004, p.1-10.

