---
title:  Verifiable ASICs
authors: Riad S. Wahby and Max Howald and Siddharth Garg and abhi shelat and Michael Walfish
conf:  Oakland S&P 2016, and eprint/2015/1243 
date:   2016-02-10T01:01:01
categories: research
---


{{< highlight html >}}
@inproceedings{WHGSW16,
 title = {Verifiable ASICs},
 author = {Riad S. Wahby and Max Howald and Siddharth Garg and abhi shelat and Michael Walfish},
 booktitle = {IEEE Security and Privacy (Oakland) 2016, eprint/2016/1243},
 year = {2016},
}
{{< /highlight >}}


[PDF](https://eprint.iacr.org/2015/1243)


A manufacturer of custom hardware (ASICs) can undermine the intended execution of that hardware; high-assurance execution thus requires controlling the manufacturing chain. However, a trusted platform might be orders of magnitude worse in performance or price than an advanced, untrusted platform. This paper initiates exploration of an alternative: using verifiable computation (VC), an untrusted ASIC computes _proofs_ of correct execution, which are verified by a trusted processor or ASIC. In contrast to the usual VC setup, here the prover and verifier _together_ must impose less overhead than the alternative of executing directly on the trusted platform. We instantiate this approach by designing and implementing physically realizable, area-efficient, high throughput ASICs (for a prover and verifier), in fully synthesizable Verilog. The system, called Zebra, is based on the CMT and Allspice interactive proof protocols, and required new observations about CMT, careful hardware design, and attention to architectural challenges. For a class of real computations, Zebra meets or exceeds the performance of executing directly on the trusted platform.











