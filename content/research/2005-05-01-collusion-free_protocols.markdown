---
layout: post
title:  "Collusion-free Protocols"
authors: "Matt Lepinski and Silvio Micali and abhi shelat"
conf: "Symposium on the Theory of Computation (STOC’05), Baltimore, MD, May 2005, p.543-552."
date:   2005-05-01T01:01:01
categories: research
---

{{< highlight html >}}
Collusion-free Protocols
Matt Lepinski and Silvio Micali and abhi shelat
Symposium on the Theory of Computation (STOC’05)
Baltimore, MD, May 2005, p.543-552.
{{< /highlight >}} 

[PDF](/~shelat/dl/CollusionFreeSTOC.pdf)

