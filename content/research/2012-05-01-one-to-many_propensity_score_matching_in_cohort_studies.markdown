---
layout: post
title:  "One-to-many propensity score matching in cohort studies,"
authors: "J. A. Rassen, a. shelat, J. Myers, R. Glynn, K. Rothman, S. Schneeweiss"
conf: "J. of Pharmacoepidemiology and Drug Safety, v21.S2, 12 pages, May 2012"
date:   2012-05-01T01:01:01
categories: research
---

{{< highlight html >}}
{{< /highlight >}} 

One-to-many propensity score matching in cohort studies,
J. A. Rassen, a. shelat, J. Myers, R. Glynn, K. Rothman, S. Schneeweiss
J. of Pharmacoepidemiology and Drug Safety, v21.S2, 12 pages, May 2012

