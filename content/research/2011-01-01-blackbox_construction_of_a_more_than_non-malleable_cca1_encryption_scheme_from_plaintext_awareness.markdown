---
layout: post
title:  "Blackbox Construction of A More Than Non-Malleable CCA1 Encryption Scheme from Plaintext Awareness"
authors: "Steve Myers, Mona Sergi, abhi shelat"
conf: "Secure Computer Networks (SCN) 2011"
date:   2011-09-01T01:01:01
categories: research
---

{{< highlight html >}}
@inproceedings{MSS11a
	title={Blackbox Construction of A More Than Non-Malleable CCA1 Encryption Scheme from Plaintext Awareness},
	author="Steve Myers and Mona Sergi and Abhi Shelat",
	booktitle="Secure Computer Networks",
	year="2011"
}
{{< /highlight >}} 

Please see the updated journal version of this paper <a href="/~shelat/research/2013/04/30/pa1.html">here</a>.
