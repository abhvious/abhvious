+++
date = "2016-01-30T20:53:21-05:00"
title = "Bio"
+++


<center><img src="/dl/0101.jpg" width="400px" />
<br/>
<br/>
</center>

I graduated from Harvard in 1997 and then joined a startup company in the bay area.   I then left Mountain View to pursue graduate school at MIT and finished a masters with Madhu Sudan and then a PhD with Silvio Micali in 2005.  My PhD thesis studied three aspects of non-interactive zero-knowledge proofs.

After PhD, I worked at the Zurich IBM Research Lab as a Research Staff Member.  In Fall 2007, I joined the faculty in the computer science department at U. of Virginia in Charlottesville, VA.  I was promoted to Associate professor (tenure) in 2013.  During 2014--15, I was on sabbatical at Cornell Tech in NYC.  In 2016, I moved to Northeastern University.

In the last 5 years, I had three children with my partner and acclaimed architectural historian <a href="http://www.arch.virginia.edu/people/directory/cammy-brothers">Cammy Brothers</a> and 
I also started a software company, <a href="http://arqspin.com">Arqspin</a>, with a colleague Jason Lawrence.

